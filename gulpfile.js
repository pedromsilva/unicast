var plugins = require( 'gulp-load-plugins' )();
var runSequence = require( 'run-sequence' );
var lazypipe = require( 'lazypipe' );
var path = require( 'path' );
var gulp = require( 'gulp' );

var FOLDERS = {};
FOLDERS.root = __dirname;
FOLDERS.source = path.join( FOLDERS.root, 'src' );
FOLDERS.target = path.join( FOLDERS.root, 'bin' );

gulp.task( 'build', function () {
	var scripts = lazypipe()
		.pipe( plugins.cached, 'es6' )
		.pipe( plugins.sourcemaps.init.bind( plugins.sourcemaps ) )
        .pipe( plugins.plumber )
        .pipe( plugins.if, '**/*.js', plugins.babel() )
        .pipe( plugins.plumber )
		.pipe( plugins.sourcemaps.write.bind( plugins.sourcemaps ), '.', {
			sourceRoot: FOLDERS.source
		} );

	return gulp.src( path.join( FOLDERS.source, '**', '*' ), { base: FOLDERS.source } )
		.pipe( plugins.plumber() )
		.pipe( plugins.if( '**/*.js', scripts() ) )
        .pipe( plugins.newer( FOLDERS.target ) )
		.pipe( gulp.dest( FOLDERS.target ) );
} );

gulp.task( 'watch', function () {
	plugins.watch( path.join( FOLDERS.source, '**', '*.js' ), function ( vinyl ) {
		if ( vinyl.event === 'unlink' ) {
			delete plugins.cached.caches[ 'es6' ][ vinyl.path ]; // gulp-cached remove api

            fs.unlinkSync( vinyl.path );
		}

		runSequence( 'build' );
	} );
} );

gulp.task( 'server', function () {
	plugins.nodemon( {
		script: 'app.js',
		args: [ 'server' ],
		ext: 'js peg',
		watch: [ 'bin' ],
		delay: '2',
		execMap: {
			'js': 'node --harmony-proxies'
		},
		env: { 'NODE_ENV': 'development' }
	} );
} );

gulp.task( 'rel:server', function () {
	plugins.nodemon( {
		script: 'app.js',
		args: [ 'server' ],
		ext: 'js peg',
		watch: [ 'bin' ],
		delay: '2',
		execMap: {
			'js': 'node --harmony-proxies'
		},
		env: { 'NODE_ENV': 'production' }
	} );
} );

gulp.task( 'dev:server', function ( done ) {
	runSequence( 'default', 'server', done );
} );

gulp.task( 'default', function ( done ) {
	runSequence( 'build', 'watch', done );
} );