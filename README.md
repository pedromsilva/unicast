<p align="center">
    <a href="https://gitlab.com/pedromsilva/unicast/"><img src="banner.png" width="500px"></a>
</p>

Unicast is a Node.js server to stream your media to different devices, supporting transcoding, playlists, multiple content providers and receivers.

**Note:** This is simply an HTTP server, which means that all interaction it supports is done through REST requests. A web interface, integrated with Kodi, is currently in development, and will most likely be published as soon as it is usable.

## Installation
The application requires:
  - NodeJS
  - FFMpeg

If you have the dependencies installed, proceed to clone this repository and run `npm install` on the downloaded folder.
After the command finishes, run the last command `gulp build`.

In the future, a more user-friendly installation process can be created, such as a one-click installer.

## Running the Server
Execute `node --harmony app.js server`

## Can I use this?
Sure. Currently, without a graphical interface, this is aimed more at developers than users.

## What Receivers are supported?
 For now, only Chromecast. Altough the application allows the creation of custom receivers (sort of), so if you're interested, post an issue and I can guide you as best as I can.

## Supported Media
At the moment, the server supports local and youtube videos (movies, tv shows or just videos). Images and Music support is planned to be added later on.

# Extending the Application
The application has been engineered with a focus on extensibility, which means that most of it's components are decoupled enough so that adding new ones or replacing them is straightfoward. A official plugin API is planned to be added sometime in the near future, as soon as a dependency injection system is chosen. Meanwhile, if you want to get to know the inner workings of the server, check the [summary in the wiki](https://gitlab.com/pedromsilva/unicast/wikis/home).