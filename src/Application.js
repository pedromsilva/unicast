import Pluggable from './Components/Pluggable';
import FolderLoader from './Components/FolderLoader';

import commander from 'commander';

export default class Application extends Pluggable {
	name : string = 'application';

	commands : Object = {};

	displayCommandExceptions : boolean = true;

	constructor ( program = null, importer = null ) {
        super( importer );

		if ( !program ) {
			program = commander;
		}

		this.program = program;
	}

	async register ( command ) {
		this.commands[ command.name ] = command;

		if ( !command.loaded ) {
			await command.load();
		}

		command.register( this.program ).action( ( ...args ) => {
			this.execute( command.name, ...args );
		} );
	}

	async execute ( name, ...args ) {
		try {
			let command = this.commands[ name ];

			if ( !command ) {
				throw new Error( `Trying to call unixistent command "${ name }"` );
			}

			let value = await command.execute( ...args );

            return value;
		} catch ( error ) {
			if ( this.displayCommandExceptions ) {
				console.error( 'ERROR', error.message || error, error.stack || '' );
			} else {
				throw error;
			}
		}
	}

    async install () {
        this.loader( new FolderLoader( 'bin/Plugins' ) );

        await this.load();
    }

	async run ( argv = null ) {
		try {
			await this.install();

			return this.parse();
		} catch ( error ) {
			console.error( 'ERROR', error.message, error.stack );
		}
	}

	parse ( argv = null ) {
		if ( !argv ) {
			argv = process.argv;
		}

		this.program.parse( argv );
	}
}