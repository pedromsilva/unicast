import { RemoveSignatures, RemoveFormatting, RemoveEmptyLines, RemoveSquares, SubtitlesTiming, SplitLongLines } from 'subbox/rules';

export var Transformers = [ new RemoveFormatting, new RemoveSignatures, new RemoveEmptyLines ];

export var YoutubeTransformers = [ new RemoveSquares, new RemoveEmptyLines, new SubtitlesTiming, new SplitLongLines ];