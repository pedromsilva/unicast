import is from 'is';
import config from 'config';
import subtitler from 'subtitler';

export default class OpenSubtitles {
    async login ( credentials = null ) {
        if ( !credentials ) {
            //credentials = config.get( 'subtitles.openSubtitles' );
        }

        return subtitler.api.login();
    }

    async token () {
        if ( !this._token ) {
            this._token = await this.login();
        }

        return this._token;
    }

    getUserRankScore ( rank ) {
        switch ( ( rank || '' ).toLowerCase() ) {
            case 'administrator': return 10;
            case 'trusted': return 8;
            default: return 0;
        }
    }

    normalize ( subtitles ) {
        return {
            provider: 'openSubtitles',
            id: subtitles.IDSubtitleFile,
            idImdb: subtitles.IDMovieImdb,
            size: subtitles.SubSize,
            hash: subtitles.SubHash,
            lastTimestamp: subtitles.SubLastTS,
            format: subtitles.SubFormat,
            reports: +subtitles.SubBad,
            rating: subtitles.SubRating,
            addedAt: subtitles.SubAddDate,
            downloads: +subtitles.SubDownloadsCnt,
            releaseName: subtitles.MovieReleaseName || '',
            fps: subtitles.MovieFPS,
            idMovie: subtitles.IDMovie,
            idMovieImdb: subtitles.seriesIMDBParent || subtitles.IDMovieImdb,
            title: subtitles.MovieName,
            year: subtitles.MovieYear,
            imdbRating: subtitles.MovieImdbRating,
            language: subtitles.SubLanguageID,
            languageName: subtitles.LanguageName,
            languageISO639: subtitles.ISO639,
            encoding: subtitles.SubEncoding,
            type: subtitles.MovieKind,
            rank: this.getUserRankScore( subtitles.UserRank ),
            attributes: {
                hearingImpaired: !!( +subtitles.SubHearingImpaired ),
                highDefinition: !!( +subtitles.SubHD )
            },
            show: {
                season: +subtitles.SeriesSeason,
                episode: +subtitles.SeriesEpisode
            },
            download: {
                manual: subtitles.SubtitlesLink,
                zip: subtitles.ZipDownloadLink,
                direct: subtitles.SubDownloadLink
            }
        };
    }

    async searchForFile ( lang, file ) {
        let token = await this.token();

        let result = await subtitler.api.searchForFile( token, lang, file );

        return result.map( this.normalize.bind( this ) );
    }

    searchShow ( lang, show, season, episode ) {
        return this.search( lang, {
            query: show,
            season: season,
            episode: episode
        } );
    }

    searchMovie ( lang, name, year = null ) {
        return this.search( lang, name + ( year === null ? '' : ( ' ' + year ) ) );
    }

    async search ( lang, text ) {
        let token = await this.token();

        if ( is.object( text ) ) {
            if ( text.imdb ) {
                text.imdbid = text.imdb.slice( 2 );

                delete text.imdb;
            }
        }

        let result = await subtitler.api[ is.string( text ) ? 'searchForTitle' : 'search' ]( token, lang, text );

        return result.map( this.normalize.bind( this ) );
    }
}