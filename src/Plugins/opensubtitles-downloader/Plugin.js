import Plugin from '/Unicast/Components/Plugin';

export default class OpenSubtitlesDownloaderPlugin extends Plugin {
    dependsOn : Array<String> = [ 'subtitles-downloader' ];

    async install ( server, System ) {
        let { 'default' : OpenSubtitles } = await System.import( '/Unicast/Plugins/OpensubtitlesDownloader/OpenSubtitles' );

        server.get( 'subtitles' ).registerProvider( new OpenSubtitles );
    }
}