import Plugin from '/Unicast/Components/Plugin';

export default class YoutubeProviderPlugin extends Plugin {
    dependsOn : Array<String> = [ 'local-provider', 'youtube-downloader' ];

    async install ( server, System ) {
        let { 'default' : YoutubeProvider } = await System.import( '/Unicast/Plugins/YoutubeProvider/Provider' );

        server.get( 'providers' ).register( new YoutubeProvider );
    }
}