import SubtitlesStream from '/Unicast/Plugins/LocalProvider/SubtitlesStream';
import SubtitlesDownloader from '/Unicast/Plugins/YoutubeDownloader/SubtitlesDownloader';
import Subbox from 'subbox';
import { RemoveSquares, RemoveEmptyLines, SubtitlesTiming, SplitLongLines } from 'subbox/rules';

export default class YoutubeSubtitlesStream extends SubtitlesStream {
	constructor ( filepath ) {
		super( filepath );

		this.captions = new SubtitlesDownloader();
	}

	download ( filepath ) {
		return this.captions.download( filepath );
	}

    async get () {
		let subtitles = await this.download( this.filepath );

        return Subbox.string( subtitles, [ new RemoveSquares, new RemoveEmptyLines, new SubtitlesTiming, new SplitLongLines ], 'vtt', 'srt' );
    }
}