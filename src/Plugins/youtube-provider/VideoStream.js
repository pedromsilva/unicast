import DefaultVideoStream from '/Unicast/Plugins/DefaultProvider/VideoStream';
import { Live, Buffered } from '/Unicast/Plugins/Providers/StreamTypes';
import LiveVideo from '/Unicast/Utilities/LiveVideo';
import YoutubeVideo from '/Unicast/Plugins/YoutubeDownloader/YoutubeVideo';
import rangeParser from 'range-parser';
import fs from 'fs-promise';
import li from 'link-id';

export default class VideoStream extends DefaultVideoStream {
	static cache = new Map();

	static make ( video ) {
		if ( this.cache.has( video ) ) {
			return this.cache.get( video );
		}

		let stream = new this( video );

		this.cache.set( video, stream );

		return stream;
	}

	constructor ( video ) {
		super();

		this.video = video;
		this.info = li( video );
		this.live = true;
	}

	static get ( video ) {
		if ( !( 'cache' in this ) ) {
			this.cache = {};
		}

		if ( !( video in this.cache ) ) {
			this.cache[ video ] = new YoutubeVideo( video );
		}

		return this.cache[ video ];
	}

    get transcodable () : boolean {
        return false;
    }

    get streamType () {
        return Buffered;
    }

    get type () {
        return Promise.resolve( Live );
    }

	get total () {
		return VideoStream.get( this.video ).total;
	}

	async read ( offset = null ) {
		let stream = VideoStream.get( this.video );

		if ( offset ) {
			return stream.read( offset ).pipe( new LiveVideo() );
		}

		return stream.read().pipe( new LiveVideo() );
	}
}