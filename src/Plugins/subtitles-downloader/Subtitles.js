import SmartDownload from './SmartDownload';
import StreamCache from 'stream-cache';
import iconv from 'iconv-lite';
import fs from 'fs-promise';
import extend from 'extend';
import yauzl from 'yauzl';
import path from 'path';
import got from 'got';
import is from 'is';

export default class Subtitles {
    constructor () {
        this.providers = [];

        this.cache = new Map();
    }

    registerProvider ( provider ) {
        this.providers.push( provider );

        return this;
    }

    async searchInProviders ( action ) {
        let results = [];

        for ( let provider of this.providers ) {
            results.push( Promise.resolve( action( provider ) ).catch( err => console.log( err ) ) );
        }

        results = await Promise.all( results );

        return results.reduce( ( memo, results ) => memo.concat( results ), [] );
    }

    async searchForFile ( lang, file ) {
        return this.searchInProviders( provider => provider.searchForFile( lang, file ) );
    }

    async searchShow ( lang, show, season, episode ) {
        return this.searchInProviders( provider => provider.searchShow( lang, show, season, episode ) );
    }

    async searchMovie ( lang, name, year = null ) {
        return this.searchInProviders( provider => provider.searchMovie( lang, name, year ) );
    }

    async search ( lang, text ) {
        return this.searchInProviders( provider => provider.search( lang, text ) );
    }

    async smart ( lang, title, file, imdb ) {
        let downloader = new SmartDownload( lang, title, file, imdb );

        return downloader.download( this );
    }

    async mass ( lang, title, folder, options = {} ) {
        options = extend( {
            overwrite: false,
            autoDownload: true,
            writeToDisk: true
        }, options );

        if ( is.string( folder ) ) {
            folder = ( await fs.readdir( folder ) ).map( file => path.join( folder, file ) );
        }

        folder = folder.filter( file => MovieExtensions.indexOf( path.extname( file ).toLowerCase() ) != -1 );

        let results = [];
        for ( let video of folder ) {
            let subtitleFile = path.join( path.dirname( video ), path.basename( video, path.extname( video ) ) + '.srt' );

            let stream;
            let subtitles = await this.smart( lang, title, video );

            if ( options.autoDownload ) {
                if ( subtitles.length && options.writeToDisk && ( options.overwrite || !( await fs.exists( subtitleFile ) ) ) ) {
                    if ( !subtitles[ 0 ].encoding ) {
                        console.log( path.join( path.basename( path.dirname( video ) ), path.basename( video ) ), subtitles[ 0 ].releaseName, subtitles[ 0 ].encoding );
                    }

                    stream = await this.download( subtitles[ 0 ] );

                    stream = stream.pipe( fs.createWriteStream( subtitleFile ) );
                } else {
                    subtitles = null;
                }
            }

            if ( subtitles ) {
                results.push( {
                    files: {
                        video: video,
                        subtitles: subtitleFile
                    },
                    subtitles: subtitles,
                    stream: stream
                } );
            }
        }

        return results;
    }

    async download ( subtitle, options = {} ) {
        let url = is.string( subtitle ) ? subtitle : subtitle.download.zip;

        options = extend( { cache: true }, options );

        if ( options.cache ) {
            if ( this.cache.has( url ) ) {
                return this.cache.get( url );
            }
        }

        let content = await got( url, {
            encoding: null
        } );

        let result = new Promise( ( resolve, reject ) => {
            yauzl.fromBuffer( content.body, ( err, file ) => {
                if ( err ) {
                    return reject( err );
                }

                file.on( 'entry', entry => {
                    if ( path.extname( entry.fileName ).toLowerCase() == '.srt' ) {
                        file.openReadStream( entry, ( err, stream ) => {
                            if ( err ) {
                                return reject( err );
                            }

                            stream = stream
                                .pipe( iconv.decodeStream( subtitle.encoding || 'CP1252' ) )
                                .pipe( iconv.encodeStream( 'utf8' ) );

                            resolve( stream );
                        } );

                        file.close();
                    }
                } );

                file.on( 'error', reject );
            } );
        } );

        if ( options.cache ) {
            this.cache.set( url, result.then( stream => stream.pipe( new StreamCache ) ) );

            return this.cache.get( url );
        }

        return result;
    }
}

export var MovieExtensions = [ '.mkv', '.mp4', '.avi', '.wmv' ];