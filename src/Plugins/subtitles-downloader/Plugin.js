import Plugin from '/Unicast/Components/Plugin';

export default class SubtitlesDownloaderPlugin extends Plugin {
    dependsOn : Array<String> = [];

    target : Array<String> = [ 'server' ];

    async install ( server, System ) {
        let { 'default' : SubtitlesManager } = await System.import( '/Unicast/Plugins/SubtitlesDownloader/Subtitles' );

        server.component( 'subtitles', new SubtitlesManager() );
    }
}