import episode from 'episode';
import sortBy from 'sort-by';
import fs from 'fs-promise';

export default class SmartDownload {
    constructor ( lang, title, file, imdb = null ) {
        this.lang = lang;
        this.title = title;
        this.file = file || title;
        this.imdb = imdb;
    }

    get isMovie () {
        return this.episode.matches.length === 0;
    }

    get episode () {
        return episode( this.file );
    }

    unique ( results ) {
        let providers = {};

        return results.filter( subtitle => {
            if ( !( subtitle.provider in providers ) ) {
                providers[ subtitle.provider ] = {};
            }

            let provider = providers[ subtitle.provider ];

            if ( subtitle.id in provider ) {
                return false;
            }

            provider[ subtitle.id ] = true;

            return true;
        } );
    }

    source () {
        let file = this.file.toLowerCase();

        for ( let pool of SourcePools ) {
            for ( let source of pool ) {
                if ( file.includes( source.toLowerCase() ) ) {
                    return pool;
                }
            }
        }

        return null;
    }

    quality () {
        let file = this.file.toLowerCase();

        for ( let pool of QualityPools ) {
            if ( file.includes( pool.toLowerCase() ) ) {
                return pool;
            }
        }

        return null;
    }

    matches ( pool, body ) {
        for ( let item of pool ) {
            if ( body.toLowerCase().includes( item.toLowerCase() ) ) {
                return true;
            }
        }

        return false;
    }

    boundaries ( property, array ) {
        let min = null;
        let max = null;

        for ( let item of array ) {
            if ( property in item ) {
                if ( min === null || item[ property ] < min ) {
                    min = item[ property ];
                }

                if ( max === null || item[ property ] > max ) {
                    max = item[ property ];
                }
            }
        }

        return [ min || 0, max || 0 ];
    }

    sort ( results ) {
        let source = this.source();
        let quality = this.quality();

        let [ minDownloads, maxDownloads ] = this.boundaries( 'downloads', results );

        results = results.map( subtitle => {
            if ( !( 'rank' in subtitle ) ) {
                subtitle.rank = 0;
            }

            if ( 'downloads' in subtitle ) {
                subtitle.rank += Math.round( ( subtitle.downloads * 10 / maxDownloads ) * 100 ) / 100;
            }

            if ( subtitle.matched ) {
                subtitle.rank += 100;
            }

            if ( source && this.matches( source, subtitle.releaseName ) ) {
                subtitle.rank += 50;
            }

            if ( quality && this.matches( [ quality ], subtitle.releaseName ) ) {
                subtitle.rank += 25;
            }

            return subtitle;
        } );

        return [].concat( results ).sort( sortBy( '-rank' ) );
    }

    async search ( manager ) {
        let matched = [], manual = [];
        if ( await fs.exists( this.file ) ) {
            matched = manager.searchForFile( this.lang, this.file );
        }

        if ( this.imdb ) {
            manual = manager.search( this.lang, {
                imdb: this.imdb
            } );
        } else if ( this.isMovie ) {
            manual = manager.searchMovie( this.lang, this.title );
        } else {
            let meta = this.episode;

            if ( meta.episode > 0 ) {
                manual = manager.searchShow( this.lang, this.title, meta.season, meta.episode );
            }
        }

        [ matched, manual ] = await Promise.all( [ matched, manual ] );

        matched = matched.map( s => {
            s.matched = true;

            return s;
        } );

        return this.unique( matched.concat( manual ) );
    }

    async download ( manager ) {
        let results = await this.search( manager );

        results = this.sort( results );

        return results;
    }
}

export var SourcePools = [
    [ 'BluRay', 'Blu-Ray', 'Blu Ray', 'BDR', 'BD5', 'BD9', 'BD25', 'BD50', 'BDRip', 'BRRip' ],
    [ 'WEB-DL', 'WEB.DL', 'WEBDL', 'WEB DL' ],
    [ 'WEBRIP', 'WEB-Rip', 'WEB Rip' ],
    [ 'WEBCAP', 'WEBCAP', 'WEB Cap' ],
    [ 'HDTV', 'PDTV', 'HDTVRip', 'TVRip', 'HDRip', 'DSR', 'DSRip', 'DTHRip', 'DVBRip' ],
    [ 'DVDR', 'DVD-Full', 'Full-Rip', 'ISO Rip', 'DVD-5', 'DVD-9' ],
    [ 'SCR', 'SCREENER', 'DVDSCR', 'DVDSCREENER', 'BDSCR' ],
    [ 'CAMRip', 'CAM' ],
    [ 'TS', 'TELESYNC', 'PDVD' ],
    [ 'WP', 'WORKPRINT' ],
    [ 'PPV', 'PPVRip' ],
    [ 'VODRip', 'VODR' ],
    [ 'DVDRip' ],
    [ 'R5' ]
];

export var QualityPools = [
    '720p', '1080p', '2160p'
];