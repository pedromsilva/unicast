import Plugin from '/Unicast/Components/Plugin';

export default class SendersPlugin extends Plugin {
    dependsOn : Array<String> = [];

    target : string = 'server';

    async install ( server, System ) {
        let { 'default' : SendersManager } = await System.import( '/Unicast/Plugins/Senders/Manager' );

        server.component( 'senders', new SendersManager( server, server.makeRouter.bind( server ) ) );
    }
}