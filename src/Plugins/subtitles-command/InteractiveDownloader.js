import SubtitlesTester from '/Unicast/Plugins/VlcRemote/SubtitlesTester';
import logUpdate from '/Unicast/Utilities/IO/logUpdate';
import chokidar from 'chokidar';
import Table from 'cli-table';
import fs from 'fs-promise';
import Kefir from 'kefir';
import chalk from 'chalk';
import path from 'path';
import inquirer from 'inquirer';
import is from 'is';

export default class InteractiveDownloader {
    constructor ( manager ) {
        this.manager = manager;
        this.subtitles = {};
    }

    getTitleForSource ( file : string, options : Object ) : string {
        if ( options.title ) {
            return options.title;
        }

        let dirname = path.dirname( file );
        let folder = path.basename( dirname );

        if ( /^(Season |S)[0-9][0-9]?/gi.test( folder ) ) {
            return path.basename( path.dirname( dirname ) );
        }

        return folder;
    }

    watch ( files ) {
        return Kefir.stream( emitter => {
            let watcher = chokidar.watch( files, {
                alwaysStat: true
            } );

            watcher.on( 'ready', () => {
                watcher.on( 'unlink', emitter.emit );
                watcher.on( 'error', emitter.error );
            } );

            //emitter.end();
        } );
    }

    async setup ( sources, options : Object ) {
        for ( let files of sources ) {
            let subtitles = await this.manager.mass( options.lang, this.getTitleForSource( files[ 0 ] ), files, {
                autoDownload: false
            } );

            for ( let result of subtitles ) {
                this.subtitles[ result.files.subtitles ] = result;

                result.cursor = 0;
            }
        }
    }

    extension ( file, newExtension = '' ) {
        if ( newExtension && newExtension != '' ) {
            newExtension = '.' + newExtension;
        }

        return path.join( path.dirname( file ), path.basename( file, path.extname( file ) ) + newExtension );
    }

    cyclic ( item ) {
        if ( item.subtitles.length === 0 ) {
            return null;
        }

        let { subtitles } = item;

        if ( item.cursor == subtitles.length ) {
            item.cursor = 0;
        }

        return subtitles[ item.cursor++ ];
    }

    table ( title, subtitles, current = null ) {
        var table = new Table( {
            head: [ 'Rank', `Release Name (${title})`, 'Downloads', 'Language' ],
            colWidths: [ 13, 80, 12, 20 ]
        } );


        for ( let subtitle of subtitles ) {
            let row = [ subtitle.rank, subtitle.releaseName || chalk.bgRed( 'undefined' ), subtitle.downloads, subtitle.language ];

            if ( subtitle === current ) {
                row = row.map( value => chalk.green( value ) );
            }

            table.push( row );
        }

        return table.toString();
    }

    fromPromiseDefer ( fn ) {
        return Kefir.stream( async emitter => {
            try {
                emitter.emit( await fn() );
                emitter.end();
            } catch ( error ) {
                emitter.error( error );
                emitter.end();
            }
        } );
    }

    run ( sources, options = {} ) {
        let flatten = sources.reduce( ( memo, items ) => memo.concat( items ), [] );

        for ( let file of flatten ) {
            let subtitle = this.extension( file, 'srt' );

            this.subtitles[ subtitle ] = {
                files: {
                    video: file,
                    subtitles: subtitle
                }
            };
        }

        let files = Object.keys( this.subtitles );

        let watcher = options.watch ? this.watch( files ) : Kefir.never();

        let startup = Kefir.sequentially( 0, files );

        let inexistent = startup.flatMapConcat( file => Kefir.fromPromise( fs.exists( file ) ) );

        let needing = Kefir.zip( [ startup, inexistent ] ).filter( ( [ a, b ] ) => !b ).map( ( [ a, b ] ) => a );

        console.log( 'READY' );
        Kefir.concat( [ needing, watcher ] ).map( file => this.subtitles[ file ] ).filter( item => !item.writing )
            .flatMapConcat( item => this.fromPromiseDefer( this.search.bind( this, options, item ) ) )
            .flatMapConcat( item => this.fromPromiseDefer( this.download.bind( this, options, item ) ) )
            .onValue( e => e )
            .onError( error => console.error( 'ERROR', error.message, error.stack ) );
    }

    async ask ( question, defaultValue = true ) {
        return new Promise( ( resolve, reject ) => {
            inquirer.prompt( [ {
                type: 'confirm',
                'default' : defaultValue,
                message: question,
                name: 'ask'
            } ], ( answers ) => {
                resolve( answers[ 'ask' ] );
            } )
        } );
    }

    async verify ( options, item ) {
        let tester = new SubtitlesTester( item.files.video );

        let answer = this.ask( 'Are the subtitles synchonized?', true );

        tester.launch();

        answer = await answer;

        await tester.stop();

        return answer;
    }

    async search ( options, item ) {
        if ( !( 'subtitles' in item ) ) {
            item.subtitles = await this.manager.smart( options.lang, this.getTitleForSource( item.files.video, options ), item.files.video );

            item.cursor = 0;
            item.tries = 0;

            if ( item.subtitles.length ) {
                await this.manager.download( item.subtitles[ 0 ] );
            }
        }

        return item;
    }

    async download ( options, item ) {
        item.writing = true;

        try {
            let current = this.cyclic( item );

            let video = item.files.video;
            let title = path.basename( video, path.extname( video ) );

            if ( current ) {
                if ( this.lastItem !== item ) {
                    logUpdate.done();

                    this.lastItem = item;
                }

                if ( item.tries >= options.retries || current.rank < options.score ) {
                    logUpdate( this.table( title, item.subtitles, current ) );
                }

                item.tries++;

                let stream = ( await this.manager.download( current ) ).pipe( fs.createWriteStream( item.files.subtitles ) );

                await new Promise( ( resolve, reject ) => {
                    stream.on( 'finish', resolve );
                    stream.on( 'error', reject );
                } );

                if ( options.verify ) {
                    logUpdate.expand();
                    if ( !( await this.verify( options, item ) ) ) {
                        await fs.unlink( item.files.subtitles );

                        logUpdate.expand();
                        if ( await this.ask( 'Continue searching for this file?' ) ) {
                            logUpdate.clear();

                            await this.download( options, item );
                        } else {
                            logUpdate.clear();

                            console.log( chalk.bgRed( title ) );
                            console.log( chalk.red( 'No subtitles selected\n' ) );
                        }
                    } else {
                        logUpdate.clear();

                        console.log( chalk.bgCyan( title ) );
                        console.log( chalk.green( current.releaseName ), '\n' );
                    }
                }

                item.writing = false;
            } else {
                console.log( chalk.bgRed( title ) );
                console.log( chalk.red( 'No subtitles found\n' ) );
            }
        } catch ( error ) {
            console.error( 'ERROR', error.message, error.stack );
            item.writing = false;
        }

        return item;
    }
}