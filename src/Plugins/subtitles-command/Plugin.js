import Plugin from '/Unicast/Components/Plugin';

export default class SubtitlesCommandPlugin extends Plugin {
    dependsOn : Array<String> = [ 'vlc-remote' ];

    target : string = 'application';

    async install ( app, System ) {
        let { 'default' : SubtitlesCommand } = await System.import( '/Unicast/Plugins/SubtitlesCommand/Command' );

        await app.register( new SubtitlesCommand( app.parentImporter ) );
    }
}