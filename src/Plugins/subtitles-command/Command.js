import FolderLoader from '/Unicast/Components/FolderLoader';
import InteractiveDownloader from './InteractiveDownloader';
import prettyjson from 'prettyjson';
import Command from '/Unicast/Command';
import fs from 'fs-promise';
import path from 'path';
import chalk from 'chalk';

export default class Subtitles extends Command {
    requires : Array<string> = [ 'subtitles-downloader' ];

    constructor ( importer = null ) {
        super( importer );

        this.name = 'subtitles';
        this.description = 'Downloads subtitles for a movie/show/season/episode';
        this.args = '<source...>';
        this.options = [
            [ '-l, --lang <lang>', 'Choose a language for the subtitles', 'por' ],
            [ '-t, --title <title>', 'Specify a custom title to use when searching for the subtitles' ],
            [ '-r, --retries <retries>', 'Minimum number of retries for the console to start showing all results for a file', 1, s => +s ],
            [ '-s, --score <score>', 'Minimum subtitle score for not showing all results for a file', 50, s => +s ],
            [ '-v, --verify', 'Whether to launch vlc automatically and check if the subtitles are synced', false ],
            [ '-w, --watch', 'Watch files after downloading for the first time and replaced removed subtitles', false ]
        ];

        this.loader( new FolderLoader( 'bin/Plugins' ) );
    }

    getTitleForSource ( file : string, options : Object ) : string {
        if ( options.title ) {
            return options.title;
        }

        let dirname = path.dirname( file );
        let folder = path.basename( dirname );

        if ( /^(Season |S)[0-9][0-9]?/gi.test( folder ) ) {
            return path.basename( path.dirname( dirname ) );
        }

        return folder;
    }

    async execute ( sources : Array< string >, options : Object ) {
        sources = sources.map( source => {
            if ( !path.isAbsolute( source ) ) {
                return path.resolve( path.join( process.cwd(), source ) );
            }

            return source;
        } ).map( async source => {
            if ( ( await fs.lstat( source ) ).isDirectory() ) {
                return fs.readdir( source ).then( files => files.map( file => path.join( source, file ) ) );
            } else {
                return [ source ];
            }
        } );

        sources = await Promise.all( sources );

        sources = sources.map( source => source.filter( file => MovieExtensions.indexOf( path.extname( file ).toLowerCase() ) != -1 ) )
                         .filter( sources => sources.length > 0 );

        let manager = this.get( 'subtitles' );

        let interactive = new InteractiveDownloader( manager );

        interactive.run( sources, options );
    }
}

export var MovieExtensions = [ '.mkv', '.mp4', '.avi', '.wmv' ];