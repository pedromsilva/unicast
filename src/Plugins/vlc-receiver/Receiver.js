import BaseReceiver from '/Unicast/Plugins/DefaultReceiver/Receiver';
import RemoteControl from '/Unicast/Plugins/VlcRemote/RemoteControl';
import Server from '/Unicast/Plugins/Server/Server';
import extend from 'extend';
import Kefir from 'kefir';
import got from 'got';
import is from 'is';

//import chromecastTranscoder from '../Chromecast/Transcoder';

export default class Receiver extends BaseReceiver {
    static type : string = 'vlc';

    player : RemoteControl;
    host : string;
    port : number;

    constructor ( server : Server, name : string, data : Object = {} ) {
        super( server, name );

        this.player = null;
        this.host = data.host;
        this.port = data.port;

        //this.transcoders.register( chromecastTranscoder() );
    }

    senderUrl ( server, item, route : string, params : Object = {} ) : string {
        let sender = server.sender( this );

        let urlParams = extend( {
            receiver: this.name,
            media: item.id
        }, params );

        return sender.url( route, urlParams );
    }

    createPlayer () : RemoteControl {
        return new RemoteControl( this.host || '127.0.0.1', this.port || 1255 );
    }

    async stop () {
        if ( this.player ) {
            await this.player.stop();

            await this.player.quit();

            await this.player.wait( 1000 );

            this._status = null;

            this.player = null;

            super.stop();
        }

        return {};
    }

    async play ( item, stream = null, config : Object = {} ) {
        config = extend( {
            fullscreen: true
        }, config );

        let video : string = this.senderUrl( this.server, item, 'video' );
        let subtitles : string;
        if ( item.related( 'subtitles' ).at( 0 ) ) {
            subtitles = this.senderUrl( this.server, item, 'subtitles', { id: item.related( 'subtitles' ).at( 0 ).get( 'id' ) } );
        }

        if ( this.player ) {
            await this.stop();
        }

        this.player = this.createPlayer();

        await this.player.launch( { subtitles } );

        await this.player.add( video );

        if ( config.fullscreen ) {
            await this.player.fullscreen();
        }

        this._status = {
            playerState: 'PLAYING',
            media: {
                metadata: {
                    itemId: item.id
                },
                duration: stream ? await stream.duration : null
            }
        };

        super.play( item );

        return this._status;
    }

    async pause () : Object {
        if ( this.player && this._status.playerState == 'PLAYING' ) {
            await this.player.pause();

            this._status.playerState = 'PAUSED';
        }

        return this._status;
    }

    async resume () : Object {
        if ( this.player && this._status.playerState == 'PAUSED' ) {
            await this.player.pause();

            this._status.playerState = 'PLAYING';
        }

        return this._status;
    }

    async seekTo ( increment : number ) {
        await this.player.seek( increment );

        return this._status;
    }

    async seekToTime ( time : number ) : Object {
        let item = await this.current;

        let video = await this.video( item );

        if ( await video.transcodable ) {
            return this.play( item, video, {
                range: {
                    start: time
                }
            } )
        } else {
            await this.seekTo( time );
        }

        return this._status;
    }

    async getStatus () : Object {
        return this._status || {};
    }
}