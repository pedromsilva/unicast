import Plugin from '/Unicast/Components/Plugin';

export default class VlcReceiverPlugin extends Plugin {
    dependsOn : Array<String> = [ 'default-receiver', 'vlc-remote' ];

    async install ( server, System ) {
        let { 'default' : Receiver } = await System.import( '/Unicast/Plugins/VlcReceiver/Receiver' );

        server.get( 'receivers' ).register( Receiver );
    }
}