import ProvidersManager from './Manager';
import Plugin from '/Unicast/Components/Plugin';

export default class ProvidersPlugin extends Plugin {
    dependsOn : Array<String> = [];

    target : string = 'server';

    async install ( server, System ) {
        let { 'default' : ProvidersManager } = await System.import( '/Unicast/Plugins/Providers/Manager' );

        server.component( 'providers', new ProvidersManager() );
    }
}