export var Buffered = Symbol( 'Buffered' );

export var Live = Symbol( 'Live' );

export var Transcoded = Symbol( 'Transcoded' );