import Plugin from '/Unicast/Components/Plugin';

export default class DefaultProviderPlugin extends Plugin {
    dependsOn : Array<String> = [ 'providers' ];
}