import ReceiverLifecycle from '/Unicast/Plugins/Transcoding/Lifecycles/Receiver';
import TranscodersManager from '/Unicast/Plugins/Transcoding/Manager';
import { Buffered, Transcoded } from '/Unicast/Plugins/Providers/StreamTypes';
import LiveVideo from '/Unicast/Utilities/LiveVideo';
import fs from 'fs-promise';
import is from 'is';
import mime from 'mime';

export default class VideoStream {
    constructor ( receiver = null, media = null ) {
        this.remake( receiver, media );
        this.allowsTranscode = false;

        this.transcodeStartDelay = 4000;
    }

    get streamType () {
        if ( this._streamType ) {
            return Promise.resolve( this._streamType );
        }

        return this.metadata.then( async metadata => {
            let transcoder = this.receiver.transcoders;

            let type = ( await transcoder.evaluate( metadata, this.media ) ) ? Transcoded : Buffered;

            this._streamType = type;

            return type;
        } );
    }

    get transcodable () {
        return this.streamType.then( type => type === Transcoded && this.allowsTranscode );
    }

    async transcode ( offset = {}, range = {} ) {
        let metadata = await this.metadata;

        let transcoder = this.receiver.transcoders;

        let container = new TranscodersManager( transcoder );

        container.custom( this.range.bind( this, range || {} ) );

        let source = await this.transcodeSource( offset, range );

        let process = await container.request( source, metadata, this.media, await this.transcodeKey( offset, range ), new LiveVideo() );

        process.lifecycle( ReceiverLifecycle, this.receiver );

        let stream = process.createReader( offset );

        if ( is.number( this.transcodeStartDelay ) ) {
            await ( new Promise( ( resolve ) => setTimeout( resolve, this.transcodeStartDelay ) ) );
        }

        return stream;
    }

    remake ( receiver, media ) {
        this.receiver = receiver;
        this.media = media;
    }

    range ( range, transcoder ) {
        if ( range.start || range.end ) {
            transcoder.range( range );
        }
    }

    async toJSON () {
        return {
            transcodable: await this.transcodable
        };
    }
}