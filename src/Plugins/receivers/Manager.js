import Evented from '/Unicast/Utilities/Evented';
import config from 'config';
import is from 'is';

export default class Manager extends Evented {
	constructor ( server ) {
        super();

		this.types = {};

		this.server = server;
	}

    async loadAll () : void {
        let types = Object.keys( this.types );

        for ( let type of types ) {
            let receiver = this.getType( type );

            if ( !receiver.loaded ) {
                await this.load( type );
            }
        }
    }

	async load ( Receiver ) {
		let receiver = this.getType( Receiver );

		if ( is.string( Receiver ) ) {
			Receiver = receiver.constructor;
		}

		if ( config.has( 'devices.custom' ) ) {
			let custom = config.get( 'devices.custom' );

			custom = custom.filter( device => device.type == Receiver.type );

			for ( let device of custom ) {
				this.registerDevice( new Receiver( this.server, device.name, device ) );
			}

			receiver.loaded = true;
		}

		if ( !config.has( 'devices.scan' ) || config.get( 'devices.scan' ) ) {
			if ( is.fn( Receiver.scan ) ) {
				let scan = Receiver.scan();

                scan.filter( device => !( device.name in receiver.devices ) ).onValue( device => {
					this.registerDevice( new Receiver( this.server, device.name, device ) );
				} );
			}

			receiver.scanned = true;
		}
	}

	async listen ( Receiver ) {
		let receiver = this.getType( Receiver );

		if ( is.string( Receiver ) ) {
			Receiver = receiver.constructor;
		}

		await Promise.resolve( Receiver.senders.listen() );
	}

	register ( receiver ) {
		this.types[ receiver.type ] = {
			type: receiver.type,
			constructor: receiver,
			devices: {}
		};
	}

	registerDevice ( receiver, type = null ) {
		type = this.getType( type || receiver );

		type.devices[ receiver.name ] = receiver;

        this.emit( 'receiver', receiver );

		return this;
	}

	getType ( type ) {
		if ( is.object( type ) ) {
			type = type.type;
		}

		return this.types[ type ];
	}

    all () : Array<Receiver> {
        return this.allTypes().map( type => {
            return Object.keys( type.devices )
                .map( name => type.devices[ name ] );
        } ).reduce( ( memo, devices ) => memo.concat( devices ), [] );
    }

    allTypes () {
		return Object.keys( this.types ).map( key => this.types[ key ] );
	}

	async get ( name, type ) {
		let types;

		if ( !type ) {
			types = this.allTypes();
		} else {
			types = [ this.getType( type ) ];
		}

		for ( let receiver of types ) {
			if ( !receiver.loaded ) {
				await this.load( receiver.type );
			}

			if ( name in receiver.devices ) {
				return receiver.devices[ name ];
			}
		}

		throw new Error( 'No device found with the name "' + name + '".' );
	}
}