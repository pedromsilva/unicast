import Plugin from '/Unicast/Components/Plugin';

export default class ReceiversPlugin extends Plugin {
    dependsOn : Array<String> = [ 'senders' ];

    target : string = 'server';

    async install ( server, System ) {
        let { 'default' : ReceiversManager } = await System.import( '/Unicast/Plugins/Receivers/Manager' );

        server.component( 'receivers', new ReceiversManager( server ) );
    }
}