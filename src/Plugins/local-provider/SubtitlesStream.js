import path from 'path';
import fs from 'fs-promise';
import Subbox from 'subbox';
import { RepackRule, RemoveSignatures, RemoveFormatting, RemoveEmptyLines } from 'subbox/rules';
import DefaultSubtitlesStream from '/Unicast/Plugins/DefaultProvider/SubtitlesStream';

export default class SubtitlesStream extends DefaultSubtitlesStream {
    constructor ( filepath ) {
        super();

        this.filepath = filepath;
    }

    async get () {
        return Subbox.file( this.filepath, [], 'srt' );
    }

    async repack ( subtitles, range ) {
        return Subbox.string( subtitles, [ new RepackRule( [ range ] ) ] );
    }

    async convert ( subtitles, format ) {
        return Subbox.string( subtitles, [], 'srt', format );
    }

    async transform ( subtitles ) {
        return Subbox.string( subtitles, [ new RemoveFormatting, new RemoveSignatures, new RemoveEmptyLines ] );
    }

    async read ( range, format = null ) {
        let subtitles = await this.get();

        if ( range && ( range.start || range.end ) ) {
            subtitles = await this.repack( subtitles, range );
        }

        subtitles = await this.transform( subtitles );

        if ( format ) {
            subtitles = await this.convert( subtitles, format );
        }

        await fs.writeFile( 'storage/subs.txt', subtitles );

        return subtitles;
    }
}