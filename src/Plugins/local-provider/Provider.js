import { Item as PlaylistItem } from '/Unicast/Plugins/Server/Database/Models';
import TranscodersManager from '/Unicast/Plugins/Transcoding/Manager';
import Provider from '/Unicast/Plugins/Providers/Provider';
import { guessLanguage } from 'guesslanguage';
import promisify from 'es6-promisify';
import config from 'config';
import fs from 'fs-promise';
import langs from 'langs';
import path from 'path';
import is from 'is';

import LocalVideoStream from './VideoStream';
import LocalSubtitlesStream from './SubtitlesStream';
import LocalEmbeddedSubtitlesStream from './EmbeddedSubtitlesStream';

export default class LocalProvider extends Provider {
    constructor () {
        super();

        this.transcodingProcesses = new TranscodersManager();
    }

    get identity () {
        return 'local';
    }

    identify ( source, type ) {
        if ( type == 'subtitles' && source.startsWith( 'embed://' ) ) {
            return this.identity + '-embed';
        }

        let windows = /^[a-zA-Z]:(\\|\/)/;
        let linux = /^(\.|~)?\//;

        return windows.test( source ) || linux.test( source );
    }

    async itemEmbeddedSubtitles ( source, language = null ) {
        let stream = this.video( source );

        if ( !language && config.has( 'providers.local.subtitles.defaultLanguage' ) ) {
            language = config.get( 'providers.local.subtitles.defaultLanguage' );
        }

        let metadata = await stream.embedded.subtitles( language );

        if ( metadata.length ) {
            return metadata[ 0 ];
        }

        return null;
    }

    async embeddedSubtitles ( source ) {
        return [];
        let stream = this.video( source );

        let metadata = await stream.embedded.subtitles();

        return metadata.map( track => ( {
            source: 'embed://subtitles/' + track.index,
            format: track.format,
            language: track.language,
            manual: false
        } ) );
    }

    async localSubtitles ( source ) {
        let language = ( ...a ) => new Promise( ( resolve ) => guessLanguage.detect( ...a, resolve ) );

        let folder = path.dirname( source );

        let subtitles = await fs.readdir( folder );

        let sourceName = path.basename( source, path.extname( source ) ).toLowerCase();

        subtitles = subtitles.filter( sub => path.extname( sub ).toLowerCase() == '.srt' )
            .filter( sub => sub.toLowerCase().startsWith( sourceName ) );

        subtitles.sort();

        subtitles = subtitles.map( sub => ( {
            source: path.join( folder, sub ),
            format: path.extname( source ).toLowerCase().slice( 1 ) || 'srt',
            manual: false
        } ) );

        for ( let subtitle of subtitles ) {
            subtitle.language = langs.where( '1', ( await language( await fs.readFile( subtitle.source, 'utf8' ) ) ) )[ '3' ];
        }

        return subtitles;
    }

    async defaultSubtitles ( source ) {
        return [].concat( await this.localSubtitles( source ), await this.embeddedSubtitles( source ) );
    }

    async itemSubtitles ( source, subtitles = null ) {
        if ( !subtitles ) {
            subtitles = [];
        }

        if ( is.string( subtitles ) ) {
            subtitles = [ subtitles ];
        }

        return subtitles.map( subtitle => {
            if ( !( 'manual' in subtitle ) ) {
                subtitle.manual = true;
            }

            return subtitle;
        } ).concat( await this.defaultSubtitles( source ) );

        if ( !subtitles ) {
            subtitles = path.join( path.dirname( source ), path.basename( source, path.extname( source ) ) + '.srt' );

            if ( !( await fs.exists( subtitles ) ) ) {
                subtitles = null;
            }
        }

        if ( !subtitles ) {
            subtitles = await this.itemEmbeddedSubtitles( source );
        }

        return subtitles;
    }

    async item ( playlist, request ) {
        let source = request.body.source;

        if ( !( await fs.exists( source ) ) ) {
            throw new Error( `Could not find the local file "${source}"` );
        }

        let subtitles = await this.itemSubtitles( source, request.body.subtitles );

        return {
            type: request.body.type,
            source: source,
            subtitles: subtitles,
            title: request.body.title,
            cover: request.body.cover,
            order: await PlaylistItem.maxOrder( playlist ) || 0,
            data: request.body.data || {},
            transcode: request.body.transcode || {}
        };
    }

    video ( source, media, receiver ) {
        return new LocalVideoStream( source, receiver, media );
    }

    subtitlesExternal ( source ) {
        return new LocalSubtitlesStream( source );
    }

    subtitlesEmbedded ( source, media ) {
        return new LocalEmbeddedSubtitlesStream( this.video( media.source ), source );
    }

    register ( manager ) {
        super.register( manager );

        manager.subtitles.define( this.identity, this.subtitlesExternal.bind( this ) );

        manager.subtitles.define( this.identity + '-embed', this.subtitlesEmbedded.bind( this ) );
    }
}