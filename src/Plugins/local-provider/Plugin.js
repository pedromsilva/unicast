import Plugin from '/Unicast/Components/Plugin';

export default class LocalProviderPlugin extends Plugin {
    dependsOn : Array<String> = [ 'default-provider' ];

    async install ( server, System ) {
        let { 'default' : LocalProvider } = await System.import( '/Unicast/Plugins/LocalProvider/Provider' );

        server.get( 'providers' ).defaultProvider = 'local';

        server.get( 'providers' ).register( new LocalProvider );
    }
}