import cluster from 'cluster';
import Command from '/Unicast/Command';
import MasterServer from '/Unicast/Plugins/Server/Master';
import MediaServer from '/Unicast/Plugins/Server/MediaServer';
import logger from '/Unicast/Plugins/Server/Logger';

export default class Server extends Command {
    name : string = 'server';
    description : string = 'Starts the server';
    options : Array<string> = [
        [ '-c, --no-cluster', 'Start the server outside of a cluster' ]
    ];

	async execute ( options ) {
		let server, isMaster = cluster.isMaster && options.cluster;

        if ( isMaster ) {
			server = new MasterServer( this.parentImporter );

			server.createClient();
		} else {
			server = new MediaServer( this.parentImporter );
		}

		logger.addTag( { msg: isMaster ? 'Master' : 'Worker', colors: 'red' } );

		return server.listen().then( status => {
			logger.message( 'Server listening on http://' + status.ip + ':' + status.port );

            return cluster;
		} ).catch( ( error ) => {
			console.error( 'ERROR', error.message, error.stack );
		} );
	}
}