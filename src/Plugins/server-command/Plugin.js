import Plugin from '/Unicast/Components/Plugin';

export default class ServerCommandPlugin extends Plugin {
    dependsOn : Array<String> = [ 'server' ];

    target : string = 'application';

    async install ( app, System ) {
        let { 'default' : ServerCommand } = await System.import( '/Unicast/Plugins/ServerCommand/Command' );

        await app.register( new ServerCommand( app.parentImporter ) );
    }
}