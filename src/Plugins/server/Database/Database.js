import knex from 'knex';
import config from 'config';
import bookshelf from 'bookshelf';
import is from 'is';

let knexInstance = knex( config.get( 'database' ) );

export var ModelSchemas = {
    json ( data ) {
        if ( is.object( data ) ) {
            data = JSON.stringify( data );
        }

        return data;
    },
    date ( data ) {
        return data;
    }
};

export var Bookshelf = bookshelf( knexInstance );

Bookshelf.plugin( 'registry' );
Bookshelf.plugin( 'visibility' );
Bookshelf.plugin( 'virtuals' );

export var Model = Bookshelf.Model.extend( {
    set ( data, ...args ) {
        if ( this.schema ) {
            for ( let field of Object.keys( this.schema ) ) {
                if ( field in data ) {
                    data[ field ] = ModelSchemas[ this.schema[ field ] ]( data[ field ], data, this );
                }
            }
        }

        return Bookshelf.Model.prototype.set.call( this, data, ...args );
    }
} );
