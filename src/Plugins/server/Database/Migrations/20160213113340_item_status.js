exports.up = function ( knex, Promise ) {
    return knex.schema.createTable( 'item_status', function ( table ) {
        table.increments( 'id' ).primary();
        table.integer( 'item_id' ).nullable().references( 'items.id' );
        table.string( 'state' ).defaultTo( 'STOPPED' );
        table.float( 'currentTime' ).defaultTo( 0 );
        table.float( 'duration' ).defaultTo( 0 );
        table.timestamps();
    } );
};

exports.down = function ( knex, Promise ) {
    return knex.schema.dropTable( 'item_status' );
};
