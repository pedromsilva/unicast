exports.up = function ( knex, Promise ) {
    return knex.schema.createTable( 'playlists', function ( table ) {
        table.increments( 'id' ).primary();
        table.string( 'device' );
        table.string( 'current_id' );
        table.timestamps();
    } ).createTable( 'items', function ( table ) {
        table.increments( 'id' ).primary();
        table.integer( 'playlist_id' ).nullable().references( 'playlists.id' );
        table.string( 'type' );
        table.string( 'source' );
        table.string( 'subtitles' );
        table.string( 'title' );
        table.string( 'cover' );
        table.integer( 'order' );
        table.json( '_data' );
        table.json( '_transcode' );
        table.timestamps();
    } );
};

exports.down = function ( knex, Promise ) {
    return knex.schema.dropTable( 'playlists' ).dropTable( 'items' );
};
