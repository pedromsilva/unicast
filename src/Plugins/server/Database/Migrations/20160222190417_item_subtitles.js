exports.up = function ( knex, Promise ) {
    return knex.schema.createTable( 'item_subtitles', function ( table ) {
        table.increments( 'id' ).primary();
        table.integer( 'item_id' ).nullable().references( 'items.id' );
        table.string( 'source' );
        table.string( 'language' ).nullable();
        table.float( 'format' ).nullable();
        table.boolean( 'manual' ).defaultTo( true );
        table.timestamps();
    } );
};

exports.down = function ( knex, Promise ) {
    return knex.schema.dropTable( 'item_subtitles' );
};
