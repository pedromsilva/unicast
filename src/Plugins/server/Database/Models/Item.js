import { Bookshelf, Model } from '../Database';

var Item = Model.extend( {
    tableName: 'items',
    hasTimestamps: true,
    hidden: [ '_data', '_transcode' ],
    subtitles () {
        return this.hasMany( 'ItemSubtitle' );
    },
    playlist () {
        return this.belongsTo( 'Playlist' );
    },
    status () {
        return this.hasOne( 'ItemStatus' );
    },
    async move ( newOrder ) {
        if ( !this.has( 'playlist_id' ) ) {
            await this.set( newOrder ).save();

            return this;
        }

        let query = Item.where( { playlist_id: this.get( 'playlist_id' ) } );

        if ( this.has( 'id' ) ) {
            query = query.query( 'where', 'id', '!=', this.get( 'id' ) );
        }

        query = query.query( 'orderBy', 'order' );

        let items = await query.fetchAll();

        let index = 0;
        for ( let item of items.models ) {
            if ( index < newOrder ) {
                await item.set( 'order', index ).save();
            } else {
                await item.set( 'order', index + 1 ).save();
            }

            index++;
        }

        await this.set( 'order', newOrder ).save();

        return this;
    },
    virtuals: {
        currentTime () {
            if ( !this.status || !this.status.currentTime ) {
                return 0;
            }

            return +this.status.currentTime;
        },
        data: {
            get () {
                let data = this.get( '_data' );

                return data ? JSON.parse( data ) : null;
            },
            set ( value ) {
                this.set( '_data', value ? JSON.stringify( value ) : null );
            }
        },
        transcode: {
            get () {
                let data = this.get( '_transcode' );

                return data ? JSON.parse( data ) : null;
            },
            set ( value ) {
                this.set( '_transcode', value ? JSON.stringify( value ) : null );
            }
        }
    }
}, {
    async maxOrder ( playlist ) {
        if ( !playlist ) {
            return null;
        }

        let items = await playlist.items().fetch();

        return items.reduce( ( memo, item ) =>
            ( item && item.get( 'order' ) >= memo ) ? item.get( 'order' ) + 1 : memo, 0 );
    }
} );

Bookshelf.model( 'Item', Item );

export default Item;