import { Bookshelf, Model } from '../Database';

var ItemSubtitle = Model.extend( {
    tableName: 'item_subtitles',
    hasTimestamps: true,
    item () {
        return this.belongsTo( 'Item' );
    }
} );

Bookshelf.model( 'ItemSubtitle', ItemSubtitle );

export default ItemSubtitle;