import { Bookshelf, Model } from '../Database';
import is from 'is';

var Playlist = Model.extend( {
    tableName: 'playlists',
    hasTimestamps: true,
    current () {
        return this.belongsTo( 'Item', 'current_id' );
    },
    items () {
        return this.hasMany( 'Item' );
    },
    async followingItems ( order, limit = null ) {
        let current = await this.current().fetch();

        let query = this.items().query( 'orderBy', 'order', order ? 'asc' : 'desc' );
        if ( current ) {
            query = query.query( 'where', 'order', order ? '>' : '<', current.get( 'order' ) );
        }

        if ( limit !== null ) {
            query = query.query( 'limit', limit );
        }

        return query;
    },
    nextItems ( limit = null ) {
        return this.followingItems( true, limit );
    },
    previousItems ( limit = null ) {
        return this.followingItems( false, limit );
    },
    async remove ( item, index = null ) {
        let playlist = this;

        if ( is.number( item ) ) {
            index = item;

            item = this.items[ index ];
        }

        if ( !item ) {
            return;
        }

        if ( this.current == item ) {
            this.current = null;

            playlist = await this.save();
        } else if ( item ) {
            await item.destroy();
        }

        if ( index === null ) {
            index = this.items.indexOf( item );
        }

        if ( index >= 0 ) {
            this.items[ index ] = null;
        }

        return playlist;
    }
} );

Bookshelf.model( 'Playlist', Playlist );

export default Playlist;