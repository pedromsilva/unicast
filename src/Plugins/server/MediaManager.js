import { Item as PlaylistItem, ItemSubtitles as PlaylistItemSubtitles } from './Database/Models';
import Dictionary from '/Unicast/Utilities/Dictionary';
import is from 'is';

export default class MediaManager {
	constructor ( server ) {
		this.server = server;

		this.devices = {
			senders: new Map(),
			status: new Map()
		};
	}

	has ( id ) {
		return PlaylistItem.where( { id: +id } ).count().then( c => c > 0 );
	}

	get ( id ) {
		return PlaylistItem.where( { id: +id } ).fetch( { withRelated: [ 'subtitles', 'playlist' ] } );
	}

	async store ( data ) {
		let item = await new PlaylistItem( data ).save();

        if ( !data.subtitles ) {
            data.subtitles = [];
        }

		if ( is.string( data.subtitles ) ) {
            data.subtitles = [ data.subtitles ];
        }

        data.subtitles.map( subtitle => {
            if ( is.string( subtitle ) ) {
                subtitle = { source: subtitle };
            }

            subtitle.item_id = item.id;

            return new PlaylistItemSubtitles( subtitle ).save();
        } );

        data.subtitles = await Promise.all( data.subtitles );

		return item.load( 'subtitles' );
	}

	async play ( media, receiver, reset = false ) {
		if ( reset ) {
			media.status.reset();

			await media.save();
		}

		let sender = this.server.providers.video( media.get( 'source' ), media, receiver );

		await this.listen( receiver );

		let message = await receiver.play( media, sender );

		let playlist = await media.playlist().fetch();

		if ( playlist ) {
			await playlist.set( { current_id: media.id } ).save();
		}

		await this.setup( receiver );

		return message;
	}

	async stop ( device ) {
		if ( device.current ) {
			let playlist = await device.playlist;

			if ( playlist ) {
				await playlist.set( { current_id: null } ).playlist.save();
			}
		}

		await device.stop();
	}

	async listen ( receiver ) {
		if ( !this.devices.senders.has( receiver ) ) {
			let router = this.server.senders.register( receiver );

			this.devices.senders.set( receiver, router );

			return router;
		}

		return this.devices.senders.get( receiver );
	}

	async setup ( receiver ) {
		if ( this.devices.status.has( receiver ) ) {
			return;
		}

		this.devices.status.set( receiver, true );

		receiver.status.on( 'update', this.update.bind( this, receiver ) );
	}

	async update ( device, status ) {
        // TODO re-activate
		//if ( device.current ) {
		//	await device.current.status.update( device.current, status );
        //
		//	console.log( device.current.status.state, device.current.status.percentage, device.current.status.currentTime, device.current.status.duration );
		//}
	}

	static getInstance () {
		if ( !this.instance ) {
			this.instance = new this();
		}

		return this.instance;
	}
}