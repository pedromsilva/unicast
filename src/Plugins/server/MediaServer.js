import Server from './Server';
import LiveVideo from '/Unicast/Utilities/LiveVideo';
import StayAwake from '/Unicast/Utilities/StayAwake/Command';
import Deconstructor from '/Unicast/Utilities/Deconstructor';
import ObserversManager from './Observers/Manager';
import MediaManager from './MediaManager';
import config from 'config';
import Logger from './Logger';
import FolderLoader from '/Unicast/Components/FolderLoader';

// Controllers
import DeviceController from './Controllers/DeviceController';
import SubtitlesController from './Controllers/SubtitlesController';


export default class MediaServer extends Server {
    name : string = 'server';

    constructor ( importer = null ) {
        super( importer );

        this.stayAwake = new StayAwake();
        this.deconstructor = new Deconstructor( () => this.stayAwake.sleep( true ) );

        this.component( 'media', new MediaManager( this ) );
        //this.component( 'observers', new ObserversManager( this ) );
    }

    sender ( receiver ) {
        return this.senders.get( receiver );
    }

    async install () {
        this.controller( DeviceController );
        this.controller( SubtitlesController );

        this.loader( new FolderLoader( 'bin/Plugins' ) );

        await this.load();
    }

    setupQuit () {
        process.on( 'message', ( msg ) => {
            if ( msg && msg.cmd === 'shutdown' ) {
                process.send( { cmd: 'sleeping' } );
            }
        } );
    }

    async initialize () {
        await LiveVideo.clear();

        this.app.on( 'error', Logger.koaError() );

        this.use( Logger.koa() );

        await this.install();

        this.setupQuit();

        return super.initialize();
    }

    async listen ( port = null ) {
        if ( !port ) {
            port = config.get( 'server.worker.port' );
        }

        await this.stayAwake.awake();

        let status = await super.listen( port );

        if ( process.send ) {
            process.send( { cmd: 'listening', status: status } );
        }

        return status;
    }
}