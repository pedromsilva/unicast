import config from 'config';
import cluster from 'cluster';
import Server from './Server';

// Controllers
import MasterController from './Controllers/MasterController';

export default class Master extends Server {
    constructor ( importer = null ) {
        super( importer );

        this.workers = [];
    }

    async restartAll () {
        for ( let worker of Array.from( this.workers ) ) {
            await this.restartClient( worker );
        }
    }

    async killAll () {
        for ( let worker of Array.from( this.workers ) ) {
            await this.killClient( worker );
        }
    }

    async restartClient ( client ) {
        await this.killClient( client );

        let worker = this.createClient();

        return this.awaitClient( worker );
    }

    async killClient ( client ) {
        client.send( { cmd: 'shutdown' } );

        await this.readClient( client, 'sleeping' );

        client.kill();
    }

    createClient () {
        let worker = cluster.fork();

        this.workers.push( worker );

        return worker;
    }

    async awaitClient ( client ) {
        let msg = await this.readClient( client, 'listening' );

        return msg.status;
    }

    readClient ( client, command ) {
        return new Promise( ( resolve, reject ) => {
            try {
                let handler = msg => {
                    if ( msg && msg.cmd == command ) {
                        client.removeListener( 'error', reject );
                        client.removeListener( 'message', handler );

                        resolve( msg );
                    }
                };

                client.on( 'message', handler );

                client.on( 'error', reject );
            } catch ( error ) {
                reject( error );
            }
        } );
    }

    async initialize () {
        this.controller( MasterController );

        await super.initialize();
    }

    async listen ( port = null ) {
        if ( !port ) {
            port = config.get( 'server.master.port' );
        }

        return super.listen( port );
    }
}