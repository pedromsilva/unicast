import is from 'is';
import path from 'path';
import fs from 'fs-promise';
import extend from 'extend';
import config from 'config';
import ReceiverController from './ReceiverController';
import { Playlist, Item as PlaylistItem } from '../Database/Models';

export default class PlaylistsItemsController extends ReceiverController {
    static routes ( router, make ) {
        let items = make();

        items.post( '/play', this.action( 'playList' ) );
        items.get( '/', this.action( 'list' ) );
        items.post( '/', this.action( 'create' ) );
        items.post( '/reorder', this.action( 'reorder' ) );
        items.get( '/youtube/:video', this.action( 'youtube' ) );
        items.get( '/:item', this.action( 'get' ) );
        items.post( '/:item', this.action( 'update' ) );
        items.delete( '/:item', this.action( 'remove' ) );
        items.post( '/:item/play', this.action( 'play' ) );
        items.delete( '/clear', this.action( 'clear' ) );
        items.get( '/previous', this.action( 'getPrevious' ) );
        items.post( '/previous/play', this.action( 'playPrevious' ) );
        items.get( '/current', this.action( 'getCurrent' ) );
        items.get( '/next', this.action( 'getNext' ) );
        items.post( '/next/play', this.action( 'playNext' ) );

        router.use( '/items', items.routes() );
    }

    async getRequestedPlaylist () {
        let device = await this.receiver;

        let playlist = await Playlist.where( { device: device.name, id: +this.params.playlist } ).fetch();

        if ( !playlist ) {
            throw new Error( `Could not find a playlist with id "${this.params.playlist}"` );
        }

        return playlist;
    }

    async list () {
        let playlist = await this.getRequestedPlaylist();

        return PlaylistItem.where( { playlist_id: playlist.get( 'id' ) } ).query( 'orderBy', 'order' ).fetchAll( { withRelated: [ 'subtitles' ].concat( this.request.query.with || [] ) } );
    }

    async get () {
        let playlist = await this.getRequestedPlaylist();

        return playlist.items().where( 'id', +this.params.item ).fetch( { withRelated: [ 'subtitles' ].concat( this.request.query.with ) } );
    }

    async playItem ( playlist, item, device ) {
        if ( !item ) {
            await playlist.set( { current_id: null } ).save();

            await device.stop();

            return null;
        }

        playlist.current_id = item.id;

        await playlist.save();

        if ( is.object( this.request.body.update ) ) {
            item.set( this.request.body.update );

            await item.save();
        }

        let media = await this.server.media.play( item, device );

        return {
            item: item,
            media: media
        };
    }

    async reorder () {
        let playlist = await this.getRequestedPlaylist();

        let newOrders = this.request.body.items;

        let items = await PlaylistItem.where( { playlist_id: playlist.get( 'id' ) } ).query( 'orderBy', 'order' ).fetchAll();

        for ( let id of Object.keys( newOrders ) ) {
            let item = items.get( +id );

            if ( !item ) {
                throw new Error( `Trying to order item id{${id}} that doesn't exist in playlist id{${playlist.get( 'id' )}}` );
            }

            await item.move( +newOrders[ id ] );
        }

        return this.list();
    }

    async playList () {
        let device = await this.receiver;

        let playlist = await this.getRequestedPlaylist();

        let item = await playlist.items().query( 'orderBy', 'order' ).query( 'limit', 1 ).fetchOne( { withRelated: [ 'subtitles' ].concat( this.request.query.with ) } );

        return this.playItem( playlist, item, device );
    }

    async play () {
        let device = await this.receiver;

        let playlist = await this.getRequestedPlaylist();

        let item = await PlaylistItem.where( { playlist_id: playlist.get( 'id' ), id: this.params.item } ).fetch( { withRelated: [ 'subtitles' ].concat( this.request.query.with || [] ) } );

        return this.playItem( playlist, item, device );
    }

    async clear () {
        let playlist = await this.getRequestedPlaylist();

        await PlaylistItem.where( { playlist_id: playlist.get( 'id' ) } ).destroy();

        return playlist;
    }

    async create () {
        let device = await this.receiver;

        let playlist = await this.getRequestedPlaylist();

        let item = await this.server.providers.item( this.request.body.source, playlist, this.request );

        item = await this.server.media.store( extend( item, {
            playlist_id: playlist.get( 'id' )
        } ) );

        if ( +this.request.body.autoplay ) {
            await this.server.media.play( item, device );
        }

        return item;
    }

    async update () {
        let playlist = await this.getRequestedPlaylist();

        let item = await PlaylistItem.where( { playlist_id: playlist.get( 'id' ), 'id': this.params.item } ).fetch();

        if ( 'order' in this.data ) {
            item.move( this.data.order );
        }

        await item.set( this.data ).save();

        return item;
    }

     async remove () {
        let playlist = await this.getRequestedPlaylist();

        await PlaylistItem.where( { playlist_id: playlist.get( 'id' ), id: this.params.item } ).destroy();

        return { success: true };
    }

    async getCurrent () {
        let playlist = await this.getRequestedPlaylist();

        return playlist.current().fetch( { withRelated: this.request.with || [] } );
    }

    async getNext () {
        let playlist = await this.getRequestedPlaylist();

        return ( await playlist.nextItems( 1 ) ).fetchOne( { withRelated: 'subtitles' } );
    }

    async playNext () {
        let device = await this.receiver;

        let playlist = await this.getRequestedPlaylist();

        let item = await ( await playlist.nextItems( 1 ) ).fetchOne( { withRelated: 'subtitles' } );

        return this.playItem( playlist, item, device );
    }

    async getPrevious () {
        let playlist = await this.getRequestedPlaylist();

        return ( await playlist.previousItems( 1 ) ).fetchOne( { withRelated: 'subtitles' } );
    }

    async playPrevious () {
        let device = await this.receiver;

        let playlist = await this.getRequestedPlaylist();

        let item = await ( await playlist.previousItems( 1 ) ).fetchOne( { withRelated: 'subtitles' } );

        return this.playItem( playlist, item, device );
    }
}