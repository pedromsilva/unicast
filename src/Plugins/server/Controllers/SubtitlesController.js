import md5 from 'md5';
import path from 'path';
import langs from 'langs';
import fs from 'fs-promise';
import uuid from 'node-uuid';
import Controller from './Controller';
import { guessLanguage } from 'guesslanguage';
import LiveVideo from '/Unicast/Utilities/LiveVideo';
//import SubtitlesManager from '../../Subtitles/Subtitles';
import SubtitlesTester from '/Unicast/Plugins/VlcRemote/SubtitlesTester';
//import OpenSubtitles from '../../Subtitles/Providers/OpenSubtitles';

export class Registry {
    constructor ( movies : Array = [] ) {
        this.hash = new DoubleMap( movies.map( row => [ row[ 0 ], {
            path: row[ 1 ],
            subtitles: new DoubleMap()
        } ] ), 'path' );
    }

    hasVideoId ( id : string ) : boolean {
        return this.hash.left.has( id );
    }

    getVideoPath ( id : string ) : string {
        if ( !this.hasVideoId( id ) ) {
            throw new Error( `Could not find the video associated with the id "${id}"` );
        }

        return this.hash.left.get( id ).path;
    }

    getVideo ( path : string ) : Object {
        if ( this.hash.right.has( path ) ) {
            return this.hash.left.get( this.hash.right.get( path ) );
        }

        let video = {
            id: uuid.v4(),
            path: path,
            subtitles: new DoubleMap()
        };

        this.hash.set( video.id, video );

        return video;
    }

    getSubtitlesId ( videoPath : string, path : string ) : string {
        let video = this.getVideo( videoPath );

        if ( video.subtitles.right.has( path ) ) {
            return video.subtitles.right.get( path );
        }

        let id = uuid.v4();

        video.subtitles.set( id, path );

        return id;
    }

    getSubtitlesPath ( videoPath : string, id : string ) : string {
        let video = this.getVideo( videoPath );

        return video.subtitles.left.get( id );
    }

    updateSubtitlesPath ( videoPath : string, id : string, newPath : string ) : string {
        let video = this.getVideo( videoPath );

        let oldPath = this.getSubtitlesPath( videoPath, id );

        video.subtitles.left.set( id, newPath );

        video.subtitles.right.delete( oldPath );

        video.subtitles.right.set( newPath, id );
    }
}

export class DoubleMap {
    accessor : string;

    constructor ( data : Array = [], accessor : string = null ) {
        this.left = new Map();
        this.right = new Map();

        this.accessor = accessor;
    }

    access ( object ) {
        if ( this.accessor ) {
            return object[ this.accessor ];
        }

        return object;
    }

    set ( left, right ) {
        this.left.set( left, right );
        this.right.set( this.access( right ), left );
    }
}

export default class SubtitlesController extends Controller {
    static registry = new Registry();

	static routes ( router, make ) {
		let subtitles = make();

		subtitles.get( '/search', this.action( 'search' ) );
		subtitles.post( '/register', this.action( 'register' ) );
		subtitles.get( '/:video', this.action( 'list' ) );
		subtitles.post( '/:video', this.action( 'add' ) );
		subtitles.post( '/:video/verify', this.action( 'verifyRemote' ) );
		subtitles.get( '/:video/:id', this.action( 'get' ) );
		subtitles.post( '/:video/:id', this.action( 'update' ) );
		subtitles.get( '/:video/:id/download', this.action( 'download' ) );
		subtitles.get( '/:video/:id/verify', this.action( 'verify' ) );
		subtitles.delete( '/:video/:id', this.action( 'delete' ) );
		subtitles.delete( '/:video', this.action( 'deleteAll' ) );

		router.use( '/subtitles', subtitles.routes() );
	}

    get registry () {
        return this.constructor.registry;
    }

    get manager () {
        return this.server.get( 'subtitles' );
        let manager = new SubtitlesManager();

        manager.registerProvider( new OpenSubtitles() );

        return manager;
    }

    get video () {
        let id = this.params.video;

        return this.registry.getVideoPath( id );
    }

    get subtitles () {
        let videoId = this.params.video;
        let id = this.params.id;

        let video = this.registry.getVideoPath( videoId );

        return this.registry.getSubtitlesPath( video, id );
    }

    getSubtitlesId ( video, subtitles ) {
        return this.registry.getSubtitlesId( video, subtitles );
    }

    getVideo ( video ) {
        if ( !this.registry.hasVideoId( video ) ) {
            return video;
        }

        return this.registry.getVideoPath( video );
    }

    async getSubtitles ( video, filter = null ) {
        let language = ( ...a ) => new Promise( ( resolve ) => guessLanguage.detect( ...a, resolve ) );

        let folder = path.dirname( video );
        let file = path.basename( video, path.extname( video ) );

        let subtitles = ( await fs.readdir( folder ) ).filter( subtitles => {
            return path.extname( subtitles ) == '.srt' && subtitles.startsWith( file );
        } ).map( subtitles => {
            return {
                id: this.getSubtitlesId( video, path.join( folder, subtitles ) ),
                path: path.join( folder, subtitles ),
                filename: subtitles,
                extension: path.extname( subtitles ),
                basename: path.basename( subtitles, path.extname( subtitles ) )
            };
        } );

        if ( filter ) {
            subtitles = subtitles.filter( filter );
        }

        for ( let subtitle of subtitles ) {
            let fileContent = await fs.readFile( subtitle.path );

            subtitle.hash = md5( fileContent );

            subtitle.language = langs.where( '1', ( await language( fileContent.toString( 'utf8' ) ) ) )[ '3' ];
        }

        return subtitles;
    }

    async search () {
        let manager = this.manager;

        let language = this.request.query.language || 'por';
        let title = this.request.query.title;
        let video = this.getVideo( this.request.query.video );
        let imdb = this.request.query.imdb || null;

        let results = await manager.smart( language, title, video, imdb );

        if ( this.request.query.collapsed ) {
            results = results.map( subtitle => {
                return {
                    releaseName: subtitle.releaseName,
                    rank: subtitle.rank,
                    downloads: subtitle.downloads,
                    encoding: subtitle.encoding,
                    download: subtitle.download
                };
            } );
        }

        return results;
    }

    async register () {
        let video = this.request.body.video;

        return {
            id: this.registry.getVideo( video ).id,
            video: video
        };
    }

    async list () {
        let video = this.video;

        return this.getSubtitles( video );
    }

    async get () {
        let video = this.video;

        let subtitles = this.getSubtitles( video, subtitles => subtitles.id === this.params.id );

        if ( subtitles.length == 0 ) {
            return null;
        }

        return subtitles;
    }

    async verifyRemote () {
        let video = this.video;

        let subtitles = await LiveVideo.reserve( '.srt' );

        let manager = this.manager;

        await new Promise( async ( resolve, reject ) => {
            let stream = ( await manager.download( this.request.body ) )
                .pipe( fs.createWriteStream( subtitles ) );

            stream.on( 'finish', resolve );
            stream.on( 'error', reject );
        } );

        let tester = new SubtitlesTester( video, subtitles );

        await tester.launch();
    }

    async add () {
        let video = this.video;

        let basename = this.request.body.basename || path.basename( video, path.extname( video ) );

        let overwrite = this.request.body.overwrite || false;

        let filePath = path.join( path.dirname( video ), basename );

        let extension = '.srt';

        if ( await fs.exists( filePath + extension ) && !overwrite ) {
            let index = 2;
            while ( await fs.exists( filePath + '-' + index + extension ) ) {
                index += 1;
            }

            filePath += '-' + index + extension;
        } else {
            filePath += extension;
        }

        let manager = this.manager;

        await new Promise( async ( resolve, reject ) => {
            let stream = ( await manager.download( this.request.body ) )
                .pipe( fs.createWriteStream( filePath ) );

            stream.on( 'finish', resolve );
            stream.on( 'error', reject );
        } );

        let id = this.getSubtitlesId( video, filePath );

        return this.getSubtitles( video, subtitles => subtitles.id === id );
    }

    async update () {
        let video = this.video;
        let id = this.params.id;

        if ( 'basename' in this.request.body ) {
            let filePath = this.registry.getSubtitlesPath( video, id );

            let newFilePath = path.join( path.dirname( filePath ), this.request.body.basename + path.extname( filePath ) );

            await fs.rename( filePath, newFilePath );

            this.registry.updateSubtitlesPath( video, id, newFilePath );
        }

        return ( await this.getSubtitles( video, subtitles => subtitles.id === id ) )[ 0 ];
    }

    async download () {
        let subtitles = this.subtitles;

        return fs.createReadStream( subtitles, 'utf8' );
    }

    async [ 'delete' ] () {
        let subtitles = this.subtitles;

        await fs.unlink( subtitles );
    }

    async verify () {
        let video = this.video;

        let subtitles = this.subtitles;

        let tester = new SubtitlesTester( video, subtitles );

        await tester.launch();
    }
}