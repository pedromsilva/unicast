import path from 'path';
import fs from 'fs-promise';
import moment from 'moment';
import config from 'config';
import sortBy from 'sort-by';
import ReceiverController from './ReceiverController';
import PlaylistsItemsController from './PlaylistsItemsController';
import { Playlist, Item as PlaylistItem } from '../Database/Models';

export default class PlaylistsController extends ReceiverController {
	static routes ( router, make ) {
		let playlists = make();
		let playlistDetails = make();

		playlists.get( '/', this.action( 'list' ) );
		playlists.get( '/last', this.action( 'last' ) );
		playlists.get( '/:playlist', this.action( 'get' ) );
		playlists.post( '/', this.action( 'create' ) );
		playlists.delete( '/', this.action( 'removeAll' ) );
		playlists.delete( '/:playlist', this.action( 'remove' ) );

        // Playlist Items
		PlaylistsItemsController.routes( playlistDetails, make );
		playlists.use( '/:playlist', playlistDetails.routes() );

		router.use( '/playlists', playlists.routes() );
	}

	async list () {
		let device = await this.receiver;

		let query = Playlist.where( 'device', device.name );

        if ( 'empty' in this.request.query ) {
            let allowEmpty = +this.request.query.empty ? true : false;

            if ( !allowEmpty ) {
                let subquery = query.items().query().whereNotNull( 'playlist_id' ).select( 'playlist_id' );

                query = query.where( 'id', 'in', subquery );
            }
        }

        if ( 'newer_than' in this.request.query ) {
            let newerThan = moment( this.request.query.newer_than );

            query = query.where( 'created_at', '>', newerThan.format( 'YYYY-MM-DD' ) );
        }

        if ( 'older_than' in this.request.query ) {
            let olderThan = moment( this.request.query.older_than );

            query = query.where( 'created_at', '<', olderThan.format( 'YYYY-MM-DD' ) );
        }

        if ( 'skip' in this.request.query ) {
            query = query.query( 'offset', +this.request.query.skip );
        }

        if ( 'take' in this.request.query ) {
            query = query.query( 'limit', +this.request.query.take );
        }

		query = query.query( 'orderBy', 'created_at', 'desc' );

        return query.fetchAll( {
            withRelated: [ { items: ( query ) => query.orderBy( 'order' ) }, 'items.subtitles', 'current' ].concat( this.request.query.with || [] )
		} );
	}

	async last () {
		let device = await this.receiver;

        let query = Playlist.where( 'device', device.name );

		if ( 'empty' in this.request.query ) {
            let allowEmpty = +this.request.query.empty ? true : false;

            if ( !allowEmpty ) {
                let subquery = query.items().query().whereNotNull( 'playlist_id' ).select( 'playlist_id' );

                query = query.where( 'id', 'in', subquery );
            }
		}

        query = query.query( 'orderBy', 'created_at', 'desc' );

		if ( 'take' in this.request.query ) {
			return query.query( 'limit', +this.request.query.take ).fetchAll( { withRelated: [ 'items', 'items.subtitles' ].concat( this.request.query.with || [] ) } );
		} else {
            return query.query( 'limit', 1 ).fetch( { withRelated: [ 'items', 'items.subtitles' ].concat( this.request.query.with || [] ) } );
        }
    }

	async get () {
		let device = await this.receiver;

		return Playlist.where( { device: device.name, id: this.params.playlist } ).fetch( { withRelated: [ {
			items: ( query ) => query.orderBy( 'order' )
		}, 'items.subtitles' ].concat( this.request.query.with || [] ) } );
    }

	async create () {
		let device = await this.receiver;

		return new Playlist( {
			device: device.name
		} ).save();
    }

	async remove () {
		let device = await this.receiver;

		return Playlist.where( { device: device.name, id: this.params.playlist } ).destroy();
	}

	async removeAll () {
		let device = await this.receiver;

		return Playlist.where( { device: device.name } ).destroy();
	}
}