import Factory from '/Unicast/Utilities/Factory';
import MediaSubject from './Subjects/MediaSubject';
import PlaylistObserver from './Media/Playlist';
import is from 'is';

export default class ObserversFactory extends Factory {
    constructor () {
        super();

        this.define( 'playlist', this.makePlaylistObserver.bind( this ) );
    }

    makePlaylistObserver ( options, media, receiver ) {
        let observer = new PlaylistObserver();

        let subject = new MediaSubject( media, receiver );

        observer.register( subject );

        return observer;
    }

    make ( observer, ...args ) {
        if ( is.string( observer ) ) {
            return super.make( observer, ...args );
        }

        return super.make( observer.type, observer, ...args );
    }
}