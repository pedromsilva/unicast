import Immutable from 'immutable';
import is from 'is';

export default class State {
    constructor ( subject, values = {} ) {
        this.subject = subject;

        this._innerCycle = true;
        this._inner = Immutable.fromJS( values );
        this._oldInner = this._inner;
        this._watchers = [];
    }

    get old () {
        return this._oldInner;
    }

    get current () {
        return this._inner;
    }

    get changed () {
        return Immutable.is( this.old, this.current );
    }

    callInnerGetter ( method, ...args ) {
        return this._inner[ method ]( ...args );
    }

    callInnerSetter ( method, ...args ) {
        if ( !this._innerCycle ) {
            this._innerCycle = true;
            this._oldInner = this._inner;
        }

        this._inner = this._inner[ method ]( ...args );

        this.setDisposeCycle();

        return this.this;
    }

    callInnerGetterIn ( method, property, ...args ) {
        if ( is.string( property ) ) {
            property = property.split( '.' );
        }

        return this.callInnerGetter( method, property, ...args );
    }

    callInnerSetterIn ( method, property, ...args ) {
        if ( is.string( property ) ) {
            property = property.split( '.' );
        }

        return this.callInnerSetter( method, property, ...args );
    }

    setDisposeCycle () {
        if ( this._disposeCycleTimeout ) {
            clearTimeout( this._disposeCycleTimeout );
        }

        this._disposeCycleTimeout = setTimeout( this.disposeBound, 100 );
    }

    dispose () {
        this._innerCycle = false;

        for ( let watcher of this._watchers ) {
            watcher.execute( this._oldInner, this._inner );
        }
    }

    get disposeBound () {
        if ( !this._disposeBound ) {
            this._disposeBound = this.dispose.bind( this );
        }

        return this._disposeBound;
    }

    when ( property, origin, target ) {
        let watcher = new StateWatcher( this, property, origin, target );

        this._watchers.push( watcher );

        return watcher;
    }

    // Proxy
    get ( ...args ) {
        return this.callInnerGetter( 'get', ...args );
    }

    set ( ...args ) {
        return this.callInnerSetter( 'set', ...args );
    }

    setIn ( ...args ) {
        return this.callInnerSetterIn( 'setIn', ...args );
    }

    getIn ( ...args ) {
        return this.callInnerGetterIn( 'getIn', ...args );
    }
}

export class StateWatcher {
    constructor ( state, property, origin = ( a, b ) => a != b, target = () => true ) {
        this.state = state;
        this.property = property;
        this.origin = origin;
        this.target = target;

        this.actions = [];
    }

    get property () {
        return this._property;
    }

    set property ( value ) {
        if ( !is.fn( value ) ) {
            value = ( ( value, state ) => state.getIn( value.split( '.' ) ) ).bind( this, value );
        }

        this._property = value;
    }

    get origin () {
        return this._origin;
    }

    set origin ( value ) {
        if ( !is.undef( value ) && !is.fn( value ) ) {
            value = ( ( value, s ) => s == value ).bind( this, value );
        }

        this._origin = value;
    }

    get target () {
        return this._target;
    }

    set target ( value ) {
        if ( !is.undef( value ) && !is.fn( value ) ) {
            value = ( ( value, s ) => s == value ).bind( this, value );
        }

        this._target = value;
    }

    matches ( newProperty, oldProperty ) {
        return ( !this.origin || this.origin( oldProperty, newProperty ) ) && ( !this.target || this.target( newProperty, oldProperty ) );
    }

    execute ( oldState, newState ) {
        let [ newProperty, oldProperty ] = [ this.property( newState ), this.property( oldState ) ];

        if ( this.matches( newProperty, oldProperty ) ) {
            for ( let action of this.actions ) {
                action( newProperty, oldProperty, newState, oldState );
            }
        } else {
            return false;
        }

        return true;
    }

    [ 'do' ] ( bound, action = null ) {
        if ( bound && is.fn( action ) ) {
            bound = action.bind( bound );
        } else if ( is.string( bound ) ) {
            bound = this.state[ bound ].bind( this.state );
        }

        this.actions.push( bound );

        return this;
    }

    emit ( event, ...args ) {
        return this.emitWith( event, ( ...values ) => args.concat( values ) );
    }

    emitState ( event, property = null ) {
        return this.emitWith( event, ( nV, oV, nS, oS ) => {
            if ( property ) {
                if ( !is.fn( property ) ) {
                    property = ( state ) => state.getIn( property.split( '.' ) );
                }

                return [ property( nS ), property( oS ), nS, oS ];
            }

            return [ nS, oS ];
        } );
    }

    emitWith ( event, resolver ) {
        return this.do( ( ...args ) => {
            this.state.subject.emit( event, ...( resolver( ...args ) ) );
        } );
    }
}