import Evented from '/Unicast/Utilities/Evented';
import State from './State';

export default class Subject extends Evented {
    constructor ( state = {} ) {
        super();

        this.state = new State( this, state );
    }

    relay ( object, event, eventAs ) {
        eventAs = eventAs || event;
        object.on( event, ( ...args ) => this.emit( eventAs, ...args ) );
    }
}