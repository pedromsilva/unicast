import Subject from './Subject';

export default class MediaSubject extends Subject {
    constructor ( media, receiverSubject ) {
        super();

        this.media = media;
        this.receiverSubject = receiverSubject;
        this.playing = true;
    }

    relayMedia ( object, event, eventAs = null ) {
        eventAs = eventAs || event;
    }
}