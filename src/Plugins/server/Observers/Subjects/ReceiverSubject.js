import Subject from './Subject';
import leet from 'l33teral';

export default class ReceiverSubject extends Subject {
    constructor ( receiver ) {
        super( {
            player: 'stopped',
            media: {
                source: null,
                item: null,
                duration: 0,
                time: 0
            },
            volume: {
                level: 1,
                muted: false
            }
        } );

        this.receiver = receiver;
        this.receiver.status.on( 'update', this.update.bind( this ) );

        this.relay( receiver, 'connected' );
        this.relay( receiver, 'disconnected' );
        this.relay( receiver.status, 'status' );

        this.state.when( 'player', 'playing', 'stopped' ).emitState( 'stopped' );
        this.state.when( 'player', 'buffering', 'stopped' ).emitState( 'stopped' );
        this.state.when( 'player', 'stopped', 'playing' ).emitState( 'played' );
        this.state.when( 'player', 'stopped', 'buffering' ).emitState( 'played' );
        this.state.when( 'player', 'playing', 'paused' ).emit( 'paused', false );
        this.state.when( 'player', 'paused', 'playing' ).emit( 'resumed', false );
        this.state.when( 'player', 'playing', 'buffering' ).emit( 'paused', true );
        this.state.when( 'player', 'buffering', 'playing' ).emit( 'resumed', true );
        this.state.when( 'volume.muted', false, true ).emit( 'muted' );
        this.state.when( 'volume.muted', true, false ).emit( 'unmuted' );
        this.state.when( 'volume.level' ).emit( 'volumeChanged' );
        this.state.when( 'media.source' ).emit( 'media' );
        this.state.when( 'media.item' ).emit( 'item' );
        this.state.when( 'media.time' ).emitWith( 'viewed', ( current, previous, newState, oldState ) => {
            let time = {
                change: current - previous,
                current: current,
                previous: previous,
                percentage: current * 100 / newState.getIn( 'media.duration' )
            };

            return [ time, newState, oldState ];
        } );
    }

    update ( status ) {
        status = leet( status || {} );

        this.state.set( 'player', status.tap( 'playerState',  'stopped' ).toLowerCase() );
        this.state.setIn( 'volume.muted', status.tap( 'volume.muted', false ) );
        this.state.setIn( 'volume.level', status.tap( 'volume.level', 1 ) );
        this.state.setIn( 'media.source', status.tap( 'media.contentId', null ) );
        this.state.setIn( 'media.item', status.tap( 'media.metadata.itemId', null ) || null );
        this.state.setIn( 'media.time', status.tap( 'currentTime', 0 ) );
        this.state.setIn( 'media.duration', status.tap( 'media.duration', null ) );
    }
}

var statusIpsum =
{
    "mediaSessionId": 1,
    "playbackRate": 1,
    "playerState": "PLAYING",
    "currentTime": 50.904179,
    "supportedMediaCommands": 15,
    "volume": {
        "level": 1,
        "muted": false
    },
    "activeTrackIds": [
        0
    ],
    "media": {
        "contentId": "http://192.168.0.4:3000/watch/tetS2SUS1lcQQtZV",
        "contentType": "video/mp4",
        "tracks": [
            {
                "trackId": 0,
                "type": "TEXT",
                "trackContentId": "http://192.168.0.4:3000/subtitles/tetS2SUS1lcQQtZV",
                "trackContentType": "text/vtt",
                "name": "Português",
                "language": "pt-PT",
                "subtype": "SUBTITLES"
            }
        ],
        "metadata": {
            "type": 0,
            "metadataType": 1,
            "itemId": "tetS2SUS1lcQQtZV",
            "title": "Inside Out",
            "images": [
                {
                    "url": "http://image.tmdb.org/t/p/original/aAmfIX3TT40zUHGcCKrlOZRKC7u.jpg"
                }
            ],
            "releaseDate": "2015-05-06"
        },
        "textTrackStyle": {
            "backgroundColor": "#FFFFFF00",
            "foregroundColor": "#FFFFFFFF",
            "edgeType": "OUTLINE",
            "edgeColor": "#000000FF",
            "fontScale": 1.6,
            "fontStyle": "BOLD",
            "fontFamily": "Droid Sans",
            "fontGenericFamily": "CURSIVE",
            "windowColor": "#AA00FFFF",
            "windowRoundedCornerRadius": 10,
            "windowType": "ROUNDED_CORNERS"
        },
        "duration": null
    },
    "currentItemId": 1,
    "items": [
        {
            "itemId": 1,
            "media": {
                "contentId": "http://192.168.0.4:3000/watch/tetS2SUS1lcQQtZV",
                "contentType": "video/mp4",
                "tracks": [
                    {
                        "trackId": 0,
                        "type": "TEXT",
                        "trackContentId": "http://192.168.0.4:3000/subtitles/tetS2SUS1lcQQtZV",
                        "trackContentType": "text/vtt",
                        "name": "Português",
                        "language": "pt-PT",
                        "subtype": "SUBTITLES"
                    }
                ],
                "metadata": {
                    "type": 0,
                    "metadataType": 1,
                    "itemId": "tetS2SUS1lcQQtZV",
                    "title": "Inside Out",
                    "images": [
                        {
                            "url": "http://image.tmdb.org/t/p/original/aAmfIX3TT40zUHGcCKrlOZRKC7u.jpg"
                        }
                    ],
                    "releaseDate": "2015-05-06"
                },
                "textTrackStyle": {
                    "backgroundColor": "#FFFFFF00",
                    "foregroundColor": "#FFFFFFFF",
                    "edgeType": "OUTLINE",
                    "edgeColor": "#000000FF",
                    "fontScale": 1.6,
                    "fontStyle": "BOLD",
                    "fontFamily": "Droid Sans",
                    "fontGenericFamily": "CURSIVE",
                    "windowColor": "#AA00FFFF",
                    "windowRoundedCornerRadius": 10,
                    "windowType": "ROUNDED_CORNERS"
                },
                "duration": null
            },
            "autoplay": true,
            "activeTrackIds": [
                0
            ]
        }
    ],
    "repeatMode": "REPEAT_OFF"
};