import MediaObserver from '../Default/MediaObserver';

export default class Playlist extends MediaObserver {
    constructor ( events ) {
        super( events );
    }

    onViewed ( subject, time ) {
        console.log( 'PLAYED', time.change, time.percentage );
    }

    init ( subject ) {

    }
}