import Subject from '../Subjects/Subject';
import { capitalize } from '/Unicast/Utilities/String';
import is from 'is';

export default class Observer extends Subject {
    constructor ( events = [] ) {
        super();

        this._events = events;
        this._subjects = [];
    }

    listen ( event, subjects = null ) {
        subjects = subjects || this._subjects;

        let onceKey = 'once' + capitalize( event );
        let onKey = 'on' + capitalize( event );

        for ( let subject of subjects ) {
            if ( is.fn( this[ onceKey ] ) ) {
                subject.once( event, this[ onceKey ].bind( this ) );
            }

            if ( is.fn( this[ onKey ] ) ) {
                subject.on( event, this[ onKey ].bind( this ) );
            }
        }
    }

    register ( subject ) {
        this._subjects.push( subject );

        for ( let event of this._events ) {
            this.listen( event, [ subject ] );
        }
    }

    logCallback ( event, subject, callback = null ) {
        subject.on( 'event', ( ...args ) => {
            if ( is.fn( callback ) ) {
                callback( event, subject, args );
            } else {
                console.log( event, subject.receiver.name, args );
            }
        } );
    }

    log ( callback = null ) {
        for ( let event of this._events ) {
            for ( let subject of this._subjects ) {
                this.logCallback( event, subject, callback );
            }
        }
    }
}