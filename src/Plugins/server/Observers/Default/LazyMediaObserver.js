import ReceiverObserver from './ReceiverObserver';
import ReceiverSubject from '../Subjects/ReceiverSubject';

export default class LazyMediaObserver extends ReceiverObserver {
    constructor ( observers, receivers ) {
        super( [ 'item', 'played', 'viewed' ] );

        this.observers = observers;
        this.receivers = receivers;

        this.receivers.on( 'receiver', ( e, receiver ) => this.register( new ReceiverSubject( receiver ) ) );
    }

    async onItem ( receiver, mediaId ) {
        if ( !mediaId ) {
            return;
        }

        await this.observers.observeMedia( receiver, mediaId ).catch( e => console.error( e.message, e.stack ) );
    }
}