import Observer from './Observer';

export default class MediaObserver extends Observer {
    constructor ( events = [] ) {
        super( [ 'played', 'stopped', 'paused', 'resumed', 'volumeChanged', 'muted', 'unmuted', 'viewed', 'media', 'item' ].concat( events ) );
    }
}