import Observer from './Observer';

export default class ReceiverObserver extends Observer {
    constructor ( events ) {
        super( events || [ 'played', 'stopped', 'paused', 'resumed', 'volumeChanged', 'muted', 'unmuted', 'media', 'item', 'connected', 'disconnected' ] );
    }
}