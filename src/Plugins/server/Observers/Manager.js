import LazyMediaObserver from './Default/LazyMediaObserver';
import ObserversFactory from './Factory';

export default class Manager {
    constructor ( server ) {
        this.factory = new ObserversFactory();
        this.lazyObserver = new LazyMediaObserver( this, server.receivers );
        this.server = server;
        this.media = {};

        this.defaultObservers = [ {
            type: 'playlist'
        } ];
    }

    async observeMedia ( receiver, mediaId ) {
        if ( !( mediaId in this.media ) ) {
            let media = await this.server.media.get( mediaId );

            let observers = [];

            for ( let observer of media.observers.concat( this.defaultObservers ) ) {
                observers.push( this.server.observers.factory.make( observer, media, receiver ) );
            }

            this.media[ mediaId ] = {
                id: mediaId,
                media: media,
                observers: observers
            };
        }
    }
}