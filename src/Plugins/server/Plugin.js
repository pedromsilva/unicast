import Plugin from '/Unicast/Components/Plugin';

export default class MediaServerPlugin extends Plugin {
    dependsOn : Array<String> = [ 'vlc-remote' ];
}