import Transcoder from '/Unicast/Plugins/Transcoding/Transcoder';
import LiveVideo from '/Unicast/Utilities/LiveVideo';
import RemoteVideo from '/Unicast/Utilities/RemoteVideo';
import ytdl from 'ytdl-core';
import fs from 'fs-promise';

export default class YoutubeVideo extends RemoteVideo {
	constructor ( url ) {
		super( url );

		console.log( url );
	}

	createReadStream ( offset ) {
		let options = { quality: 'highest', filter: format => {
			return format.container === 'mp4';
		} };

		if ( offset ) {
			options.range = offset.start + '-' + offset.end;
		}

        let stream = ytdl( this.source, options );

        //let transcoder = new Transcoder( stream );
        //
        //transcoder.set( 'c:a', 'copy' ).set( 'c:v', 'libx264' ).set( 'format', 'matroska' ).set( 'vf', 'pad=max(iw\\,ih*(16/9)):ow/(16/9):(ow-iw)/2:(oh-ih)/2' ).set( 'aspect', '16:9' );
        //
        //return transcoder.stream().pipe( new LiveVideo );

        return stream;
	}

	read ( offset ) {
		let result = super.read( offset );

		this.total = new Promise( ( resolve ) => {
			this.incoming.on( 'info', ( info, format ) => {
				resolve( parseInt( format.size, 10 ) );
			} )
		} );

		return result;
	}
}