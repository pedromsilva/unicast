import CommandWrapper, { CommandOptionMeta } from '/Unicast/Utilities/CommandWrapper';
import TrackTranscoder from './TrackTranscoder';
import { spawn } from 'child_process';
import readline from 'readline';
import config from 'config';
import extend from 'extend';
import util from 'util';
import path from 'path';
import os from 'os';
import is from 'is';

const CONFIG_FFMPEG_PATH = ( config.has( 'ffmpeg.path' ) ? config.get( 'ffmpeg.path' ) : null );
const FFMPEG_BIN_PATH = path.join( process.env.FFMPEG_BIN_PATH || CONFIG_FFMPEG_PATH || '', 'ffmpeg' );

/*
 Transcodes a media stream from one format to another.
 @source A file or a readable stream.

 Events:
 'metadata' emitted when media metadata is available.
 @metadata (callback parameter) The media mediadata.

 'progress' emitted when transcoding has progressed.
 @progress (callback parameter) The status of the transcoding process.

 'finish' emitted when transcoding has completed.

 'error' emmited if an error occurs.
 @error (callback parameter) The error that occured.

 */
export default class Transcoder extends CommandWrapper {
    constructor ( source ) {
        super();

        this.source = source;

        this.args = {};
        this._range = { start: null, end: null };
        this.lastErrorLine = null;

        this.setOptionMeta( 'map', { multiple: true, prepend: true } );
        this.setOptionMeta( 'ss', { index: () => 0 } );
        this.setOptionMeta( [ 'c', 'codec', 'b', 'bitrate' ], { specific: true } );
        this.setOptionMeta( 'codec', { rename: 'c' } );
        this.setOptionMeta( 'videoCodec', { rename: 'c:v', parent: 'c' } );
        this.setOptionMeta( 'audioCodec', { rename: 'c:a', parent: 'c', specific: true } );
        this.setOptionMeta( 'bitrate', { rename: 'b' } );
        this.setOptionMeta( 'videoBitrate', { rename: 'b:v', parent: 'b' } );
        this.setOptionMeta( 'audioBitrate', { rename: 'b:a', parent: 'b' } );
        this.setOptionMeta( 'fps', { rename: 'r' } );
        this.setOptionMeta( 'format', { rename: 'f', fn: this.format.bind( this ) } );
        this.setOptionMeta( 'vf', { multiple: true } );
        this.setOptionMeta( 'maxSize', { rename: 'vf', fn: this.maxSize.bind( this ) } );
        this.setOptionMeta( 'minSize', { rename: 'vf', fn: this.minSize.bind( this ) } );
        this.setOptionMeta( 'size', { rename: 's', fn: this.size.bind( this ) } );
        this.setOptionMeta( 'passes', { rename: 'pass' } );
        this.setOptionMeta( 'aspectRatio', { rename: 'aspect' } );
        this.setOptionMeta( 'sampleRate', { rename: 'ar' } );
        this.setOptionMeta( 'channels', { rename: 'ac' } );
        this.setOptionMeta( 'range', { rename: 'ac' } );
    }

    makeCommandOptionMeta ( ...args ) {
        return new FFMpegCommandOptionMeta( ...args );
    }

    getOptionMeta ( option, force = false ) {
        return super.getOptionMeta( option.split( ':' )[ 0 ], force );
    }

    /* Converts a FFmpeg time format to milliseconds */
    _parseDuration ( duration ) {
        let d = duration.split( /[:.]/ );

        return parseInt( d[ 0 ] ) * 60 * 60 * 1000
            + parseInt( d[ 1 ] ) * 60 * 1000
            + parseInt( d[ 2 ] ) * 1000
            + parseInt( d[ 3 ] );
    }

    _parseMetadata ( child ) {
        /* Filters for parsing metadata */
        var metadataFilters = {
            'type': {
                match: /Stream #[0-9]+:[0-9]+.*?: (\w+):/i,
                transform: r => {
                    if ( r[ 1 ] ) {
                        return r[ 1 ].toLowerCase();
                    }
                }
            },
            'codec': {
                match: /Stream.*?:.*?: \w+: (.*?)(?: |\()/i,
                idx: 1
            },
            'samplerate': {
                match: /(\d+) Hz/i,
                idx: 1,
                transform: parseInt
            },
            'channels': {
                match: /\d+ Hz, (.*?)(?:,|$)/i,
                idx: 1,
                transform: r => {
                    if ( r == 'mono' ) {
                        return 1;
                    }
                    if ( r == 'stereo' ) {
                        return 2;
                    } else {
                        return parseInt( r );
                    }
                }
            },
            'bitrate': {
                match: /(\d+) (\w)?b\/s/i,
                transform: r => {
                    if ( r[ 2 ] == 'k' ) {
                        return parseInt( r[ 1 ] ) * 1000;
                    }
                    if ( r[ 2 ] == 'm' ) {
                        return parseInt( r[ 1 ] ) * 1000 * 1000;
                    }
                    return parseInt( r[ 1 ] );
                }
            },
            'fps': {
                match: /(\d+) fps/i,
                idx: 1,
                transform: parseInt
            },
            'size': {
                match: /(\d+)x(\d+)(?:,|$)/i,
                transform: r => {
                    if ( r[ 1 ] && r[ 2 ] ) {
                        return { width: parseInt( r[ 1 ] ), height: parseInt( r[ 2 ] ) };
                    }
                }
            },
            'aspect': {
                match: /(\d+)x(\d+)(?:,|$)/i,
                transform: r => {
                    if ( r[ 1 ] && r[ 2 ] ) {
                        return parseInt( r[ 1 ] ) / parseInt( r[ 2 ] );
                    }
                }
            },
            'colors': {
                match: /Video:.*?, (.*?)(?:,|$)/i,
                idx: 1
            }
        };

        /* Filters for parsing progress */
        var progressFilters = {
            'frame': {
                match: /frame= .?([\d]+)/i,
                idx: 1,
                transform: parseInt
            },
            'fps': {
                match: /fps=([\d.]+)/i,
                idx: 1,
                transform: parseInt
            },
            'quality': {
                match: /q=([\d.]+)/i,
                idx: 1,
                transform: parseInt
            },
            'size': {
                match: /size=[\s]+?([\d]+)(\w)?b/i,
                transform: r => {
                    if ( r[ 2 ] == 'k' ) {
                        return parseInt( r[ 1 ] ) * 1024;
                    }
                    if ( r[ 2 ] == 'm' ) {
                        return parseInt( r[ 1 ] ) * 1024 * 1024;
                    }
                    return parseInt( r[ 1 ] );
                }
            },
            'time': {
                match: /time=(\d+:\d+:\d+.\d+)/i,
                idx: 1,
                transform: this._parseDuration
            },
            'bitrate': {
                match: /bitrate=[\s]+?([\d.]+)(\w)?bits\/s/i,
                transform: r => {
                    if ( r[ 2 ] == 'k' ) {
                        return parseInt( r[ 1 ] ) * 1000;
                    }
                    if ( r[ 2 ] == 'm' ) {
                        return parseInt( r[ 1 ] ) * 1000 * 1000;
                    }
                    return parseInt( r[ 1 ] );
                }
            }
        };

        /* Applies a set of filters to some data and returns the result */
        var _applyFilters = ( data, filters ) => {
            var ret = {};
            for ( let key of Object.keys( filters ) ) {
                let filter = filters[ key ];

                var r = filter.match.exec( data ) || [];

                if ( filter.idx ) {
                    r = r[ filter.idx ];
                }

                var v = (filter.transform ? filter.transform( r ) : r);

                if ( v ) {
                    ret[ key ] = v;
                }
            }

            return ret;
        };

        var metadata = { input: {}, output: {} };
        var current = null;

        var metadataLines = readline.createInterface( {
            input: child.stderr,
            output: process.stdout,
            terminal: false
        } );

        var ended = false;
        var _endParse = () => {
            if ( !ended ) {
                this.emit( 'metadata', metadata );
            }

            ended = true;
        };

        child.on( 'exit', _endParse );

        metadataLines.on( 'line', line => {
            try {
                if ( !ended ) {
                    /* Process metadata */
                    line = line.replace( /^\s+|\s+$/g, '' );

                    if ( line.length > 0 ) {
                        this.lastErrorLine = line;
                    }

                    if ( /^input/i.test( line ) ) {
                        current = metadata.input = { streams: [] };
                    } else if ( /^output/i.test( line ) ) {
                        current = metadata.output = { streams: [] };
                    } else if ( /^Metadata:$/i.test( line ) ) {
                        if ( current.streams.length ) {
                            current.streams[ current.streams.length - 1 ].metadata = {};
                        } else {
                            current.metadata = {};
                        }
                    } else if ( /^duration/i.test( line ) ) {
                        let d = /duration: (\d+:\d+:\d+.\d+)/i.exec( line );
                        current.duration = this._parseDuration( d[ 1 ] );
                        current.synched = (/start: 0.000000/.exec( line ) != null);
                    } else if ( /^stream mapping/i.test( line ) ) {
                        _endParse();
                    } else if ( /^stream #/i.test( line ) ) {
                        current.streams.push( _applyFilters( line, metadataFilters ) );
                    } else {
                        var metadataTarget;
                        if ( current.streams.length && current.streams[ current.streams.length - 1 ].metadata ) {
                            metadataTarget = current.streams[ current.streams.length - 1 ].metadata;
                        } else if ( current.metadata ) {
                            metadataTarget = current.metadata;
                        }

                        if ( metadataTarget ) {
                            var metadataInfo = line.match( /^(\S+?)\s*:\s*(.+?)$/ );
                            if ( metadataInfo && metadataInfo.length ) {
                                metadataTarget[ metadataInfo[ 1 ] ] = metadataInfo[ 2 ];
                            }
                        }
                    }
                }

                /* Track progress */
                if ( /^(frame|size)=/i.test( line ) ) {
                    if ( !ended ) {
                        _endParse();
                    }

                    let progress = _applyFilters( line, progressFilters );

                    if ( metadata.input.duration ) {
                        progress.progress = progress.time / metadata.input.duration;
                    }

                    this.emit( 'progress', progress );
                }
            } catch ( e ) {
                this.emit( 'parseError', line, e );
            }
        } );
    }

    /* Spawns child and sets up piping */
    _exec ( a ) {
        console.log('Spawning ffmpeg ' + a.join(' '));

        var child = spawn( FFMPEG_BIN_PATH, a, {
            cwd: os.tmpdir()
        } );
        this._parseMetadata( child );

        child.stdin.on( 'error', err => {
            try {
                if ( 'object' == typeof this.source ) {
                    this.source.unpipe( child.stdin );
                }
            } catch ( e ) {
                // Do nothing
            }
        } );

        child.on( 'exit', code => {
            if ( !code ) {
                this.emit( 'finish' );
            } else {
                this.emit( 'error', new Error( 'FFmpeg error: ' + this.lastErrorLine ) );
            }
        } );

        this.emit( 'message' );

        if ( is.object( this.source ) ) {
            this.source.pipe( child.stdin );
        }

        return child;
    }

    /* Compile arguments for FFmpeg */
    compileOptions () {
        let options = super.compileOptions();

        if ( this._range.end ) {
            options = [ '-to', this._range.end ].concat( options );
        }

        if ( is.string( this.source ) ) {
            options = [ '-i', this.source ].concat( options );
        } else {
            options = [ '-i', '-' ].concat( options );
        }

        if ( this._range.start ) {
            options = [ '-ss', this._range.start ].concat( options );
        }

        return options;
    }

    exec () {
        return this._exec( this.compileOptions() );
    }

    /* Makes FFmpeg write to stdout. Executes and returns stdout. */
    stream () {
        var a = this.compileOptions();

        a.push( 'pipe:1' );

        return ( this.child = this._exec( a ) ).stdout;
    }

    /* Makes FFmpeg write to file. Executes */
    writeToFile ( file ) {
        var a = this._compileArguments();
        a = a.concat( '-y', file );
        this._exec( a );
        return this;
    }

    map ( tracks, mapper ) {
        for ( let [ index, track ] of tracks.entries() ) {
            mapper( new TrackTranscoder( this, [ track ] ), index );
        }
    }

    tracks ( tracks ) {
        return new TrackTranscoder( this, [ tracks ] );
    }

    /* Set transcoding timerange */
    range ( start = null, end = null ) {
        if ( !is.object( start ) ) {
            start = {
                start: start,
                end: end
            };
        }

        this._range = extend( {}, this._range, start );

        return this;
    }

    /* Set output format */
    format ( format ) {
        if ( format.toLowerCase() == 'mp4' ) {
            this.set( 'movflags', 'frag_keyframe+faststart' );
        }

        return format;
    }

    /* Set maximum video size. Adjusts size to maintain aspect ratio, making it fit within the size */
    maxSize ( width, height, alwaysScale ) {
        if ( alwaysScale === undefined ) {
            alwaysScale = true;
        }
        var fltWdth = 'min(trunc(' + width + '/hsub)*hsub\\,trunc(a*' + height + '/hsub)*hsub)';
        var fltHght = 'min(trunc(' + height + '/vsub)*vsub\\,trunc(' + width + '/a/vsub)*vsub)';
        if ( !alwaysScale ) {
            fltWdth = 'min(trunc(iw/hsub)*hsub\\,' + fltWdth + ')';
            fltHght = 'min(trunc(ih/vsub)*vsub\\,' + fltHght + ')';
        }

        return 'scale=' + fltWdth + ':' + fltHght;
    }

    /* Set minimum video size. Adjusts size to maintain aspect ratio, making it grow to size. */
    minSize ( width, height, alwaysScale ) {
        if ( alwaysScale === undefined ) {
            alwaysScale = true;
        }
        var fltWdth = 'max(trunc(' + width + '/hsub)*hsub\\,trunc(a*' + height + '/hsub)*hsub)';
        var fltHght = 'max(trunc(' + height + '/vsub)*vsub\\,trunc(' + width + '/a/vsub)*vsub)';
        if ( !alwaysScale ) {
            fltWdth = 'max(trunc(iw/hsub)*hsub)\\,' + fltWdth + ')';
            fltHght = 'max(trunc(ih/vsub)*vsub)\\,' + fltHght + ')';
        }
        this.args[ 'vfscale' ] = [ '-vf', 'scale=' + fltWdth + ':' + fltHght ];
        return this;
    }

    /* Sets the video size. Does not maintain aspect ratio. */
    size ( width, height = null ) {
        if ( height === null ) {
            return width;
        }

        return width + 'x' + height;
    }

    /* Capture still frame. Exports jpeg. */
    captureFrame ( time ) {
        var secs = time / 1000;

        var hours = Math.floor( secs / (60 * 60) );
        var divisor_for_minutes = secs % (60 * 60);
        var minutes = Math.floor( divisor_for_minutes / 60 );

        var divisor_for_seconds = divisor_for_minutes % 60;
        var seconds = Math.ceil( divisor_for_seconds );

        while ( seconds >= 60 ) {
            seconds -= 60;
            minutes++;
        }

        while ( minutes >= 60 ) {
            minutes -= 60;
            hours++;
        }

        var timestamp = hours.toString() + ':' + minutes.toString() + ':' + seconds.toString();

        //this.args[ 'ss' ] = [ '-ss', timestamp, '-an', '-r', '1', '-vframes', '1', '-y' ];
        this.set( 'ss', timestamp ).set( 'an' ).set( '-r', 1 ).set( 'vframes', 1 ).set( 'y' );

        return this.set( 'videoCodec', 'mjpeg' ).set( 'format', 'mjpeg' );
    }
}

export class FFMpegCommandOptionMeta extends CommandOptionMeta {
    getName ( option ) {
        let [ name, ...suffixs ] = option.split( ':' );

        name = super.getName( name );

        if ( suffixs.length ) {
            name += ':' + suffixs.join( ':' );
        }

        return name;
    }
}