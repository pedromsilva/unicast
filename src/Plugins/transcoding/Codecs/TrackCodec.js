import Codec from './Codec';
import _ from 'lodash';
import is from 'is';

export default class TrackCodec extends Codec {
    constructor ( criteria = {} ) {
        super();

        this.criteria = criteria;
    }

    tracks ( metadata, criteria ) {
        if ( is.undef( criteria ) ) {
            criteria = this.criteria;
        }

        return metadata.get( 'streams' ).filter( stream => {
            return _.where( [ stream.toJS() ], criteria ).length == 1;
        } );
    }

    async matches ( metadata ) {
        return this.tracks( metadata ).size > 0;
    }

    async convert ( transcoder, metadata, media, ...args ) {
        return transcoder.map( metadata.get( 'streams' ), ( trackTranscoder => {
            this.convertTrack( trackTranscoder, metadata, media, ...args );
        } ) );
    }
}