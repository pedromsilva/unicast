import Codec from './Codec';

export default class Bitrate extends Codec {
    async matches ( metadata ) {
        return metadata.getIn( [ 'format', 'bit_rate' ] );
    }
}