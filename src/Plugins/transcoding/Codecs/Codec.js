export default class Codec {
    async evaluate ( ...args ) {
        return this.matches( ...args );
    }

    async execute ( ...args ) {
        return this.convert( ...args );
    }

    static make ( ...args ) {
        return new this( ...args );
    }
}