import TrackCodec from '../TrackCodec';
import extend from 'extend';

export default class Overlay extends TrackCodec {
	constructor ( options = {} ) {
		super( extend( { codec_type: 'subtitle' }, options ) );
	}

	convert ( transcoder, metadata, media, subtitles ) {
		return transcoder.set( 'filter_complex', '"[0:v][0:s:' + subtitles + ']overlay[v]"' ).set( 'map', '"[v]"' );
		//return transcoder.custom( 'filter_complex', '"[0:v][0:s:' + subtitles + ']overlay[v]"' ).custom( 'map', '"[v]"' );
	}
}