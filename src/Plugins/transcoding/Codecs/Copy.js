import TrackCodec from './TrackCodec';

export default class Copy extends TrackCodec {
    convertTrack ( transcoder ) {
        return transcoder.set( 'c', 'copy' );
    }
}