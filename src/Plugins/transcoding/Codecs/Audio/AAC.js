import AudioCodec from './Audio';
import extend from 'extend';

export default class AAC extends AudioCodec {
    constructor () {
        super( { codec_name: 'aac' } );
    }

    convertTrack ( transcoder ) {
        return transcoder.set( 'codec', 'aac' );
    }
}