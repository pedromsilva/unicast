import AudioCodec from './Audio';
import extend from 'extend';

export default class AC3 extends AudioCodec {
    constructor () {
        super( { codec_name: 'ac3' } );
    }

    convertTrack ( transcoder ) {
        return transcoder.set( 'c:a', 'ac3' ).set( 'bitrate', '640k' );
    }
}