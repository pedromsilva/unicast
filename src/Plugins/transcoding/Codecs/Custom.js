import Codec from './Codec';
import is from 'is';

export default class Custom extends Codec {
    constructor ( converter, matcher ) {
        super();

        this.matcher = matcher;
        this.converter = converter;
    }

    matches ( metadata, media ) {
        if ( is.fn( this.matcher ) ) {
            return this.matcher( metadata, media );
        }

        return true;
    }

    convert ( transcoder, metadata, media ) {
        if ( is.fn( this.converter ) ) {
            return this.converter( transcoder, metadata, media );
        }
    }

    static make ( ...args ) {
        return new this( ...args );
    }
}
