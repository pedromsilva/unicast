import Codec from '../Codec';

export default class MKV extends Codec {
    async matches ( metadata ) {
        if ( metadata.getIn( [ 'format', 'format_name' ] ) == 'matroska,webm' ) {
            return true;
        }

        return false;
    }

    async convert ( transcoder ) {
        return transcoder.set( 'format', 'matroska' );
    }
}