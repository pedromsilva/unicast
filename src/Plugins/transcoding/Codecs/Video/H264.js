import VideoCodec from './Video';
import extend from 'extend';

export default class H264 extends VideoCodec {
    constructor () {
        super( { codec_name: 'h264' } );
    }

    convertTrack ( transcoder, metadata, media, crf = null, preset = null ) {
        return transcoder.set( 'c:v', 'libx264' ).set( 'crf', crf || '20' ).set( 'preset', preset || 'fast' );
    }
}