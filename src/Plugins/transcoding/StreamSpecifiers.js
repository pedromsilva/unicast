export default class StreamSpecifiers {
    static parse ( input ) {
        if ( input.startsWith( '#' ) ) {
            return new this( 'i', input.slice( 1 ) );
        }

        if ( input.startsWith( 'i:' ) ) {
            return new this( 'i', input.slice( 2 ) );
        }

        if ( input.startsWith( 'i:' ) ) {
            return new this( 'i', input.slice( 2 ) );
        }

        if ( input.contains( ':' ) ) {
            return new this( ...input.split( ':' ) );
        }

        if ( input.length == 1 && 'vVasdtm'.contains( input ) ) {
            return new this( input );
        }

        if ( isNumber( input ) ) {
            return new this( null, input );
        }

        throw new Error( `Couldn't parse stream specifier ${input}` );
    }

    constructor ( type, key, value = null ) {
        this.type = type;
        this.key = key;
        this.value = value;

        this.streamTypeMapper = {
            'v': 'video',
            'V': 'video',
            'a': 'audio',
            's': 'subtitles',
            'd': 'data',
            't': 'attachment'
        };
    }

    filterStreamTag ( stream ) {
        if ( this.value === value ) {
            return this.key in stream[ 'tag' ] || this.key in stream[ 'disposition' ];
        }

        return stream[ 'tag' ][ this.key ] == this.value || stream[ 'disposition' ][ this.key ] == this.value;
    }

    filterStream ( stream ) {
        if ( this.type == 'm' ) {
            return this.filterStreamTag( stream );
        }

        if ( this.type && stream.codec_type !== this.streamTypeMapper[ this.type ] ) {
            return false;
        }

        if ( !this.type && this.key ) {
            return stream.index == this.key;
        }

        return true;
    }

    filter ( metadata ) {
        metadata.streams = metadata.streams.filter( stream => {
            return this.filterStream( stream );
        } );

        return metadata;
    }

    filterImmutable ( metadata ) {
        return metadata.set( 'streams', metadata.get( 'streams' ).filter( stream => {
            return this.filterStream( stream.toJS() );
        } ) );
    }
}