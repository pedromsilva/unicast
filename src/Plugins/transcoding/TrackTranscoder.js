export default class TrackTranscoder {
    constructor ( transcoder, tracks ) {
        this.transcoder = transcoder;
        this.tracks = tracks;

        this.setMaps();
    }

    setMaps () {
        for ( let track of this.tracks ) {
            if ( !this.isMapped( track ) ) {
                this.transcoder.set( 'map', this.property( track, '0' ) );
            }
        }
    }

    isMapped ( track ) {
        return this.transcoder.has( 'map', option => option.value == this.property( track, '0' ) );
    }

    property ( track, property ) {
        if ( property.includes( ':' ) ) {
            return property + ':' + track.get( 'typeIndex' );
        }

        return property + ':' + track.get( 'index' );
    }

    hasOption ( property, filter = null ) {
        return this.transcoder.hasOption( this.property( this.tracks[ 0 ], property ), filter );
    }

    getOption ( property, filter = null ) {
        return this.transcoder.getOption( this.property( this.tracks[ 0 ], property ), filter );
    }

    setOptionForTrack ( track, property, ...values ) {
        this.transcoder.setOption( this.property( track, property ), ...values );
    }

    setOption ( property, ...values ) {
        let meta = this.transcoder.getOptionMeta( property, true );

        if ( meta.is( 'specific' ) ) {
            for ( let track of this.tracks ) {
                this.setOptionForTrack( track, property, ...values );
            }
        } else {
            this.transcoder.setOption( property, ...values );
        }

        return this;
    }

    has ( property, filter = null ) {
        return this.hasOption( property, filter );
    }

    get ( property, filter = null ) {
        return this.getOption( property, filter );
    }

    set ( property, ...values ) {
        return this.setOption( property, ...values );
    }
}