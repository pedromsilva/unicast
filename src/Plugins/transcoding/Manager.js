import StreamTranscoder from './Transcoder';
import TranscodingProcess from './Process';
import CustomCodec from './Codecs/Custom';
import Immutable from 'immutable';

export default class TranscodersManager {
    constructor ( parent = null, bubble = true ) {
        this.parent = parent;
        this.root = parent ? this.parent.root : this;

        this.bubbleProcesses = bubble;
        this.internalProcesses = new Map();

        this.criteria = [];
    }

    get processes () {
        if ( this.bubbleProcesses && this.parent ) {
            return this.parent.processes;
        }

        return this.internalProcesses;
    }

    register ( transcoders ) {
        this.criteria.push( transcoders );

        return this;
    }

    custom ( converter, matcher = null ) {
        return this.register( new CustomCodec( converter, matcher ) );
    }

    async matched ( metadata, media = null ) {
        let matched = [];

        for ( let criteria of this.criteria ) {
            if ( await criteria.evaluate( Immutable.fromJS( metadata ), media ) ) {
                matched.push( criteria );
            }
        }

        if ( this.parent ) {
            matched = ( await this.parent.matched( metadata, media ) ).concat( matched );
        }

        return matched;
    }

    async evaluate ( metadata, media = null ) {
        return ( await this.matched( metadata, media ) ).length > 0;
    }

    async execute ( transcoder, metadata, media = null ) {
        let matched = await this.matched( metadata, media );

        for ( let codec of matched ) {
            await codec.execute( transcoder, Immutable.fromJS( metadata ), media );
        }

        return transcoder;
    }

    async create ( source, metadata, media = null ) {
        let transcoder = new StreamTranscoder( source );

        transcoder = await this.execute( transcoder, metadata, media );

        return new TranscodingProcess( transcoder );
    }

    async spawn ( source, metadata, media = null, passthrough = null ) {
        let process = await this.create( source, metadata, media );

        return process.start( passthrough );
    }

    async request ( source, metadata, media = null, key = null, passtrough = null ) {
        let transcodingProcess;

        if ( key !== null ) {
            transcodingProcess = this.processes.get( key );
        }

        if ( !transcodingProcess ) {
            transcodingProcess = await this.spawn( source, metadata, media, passtrough );
        }

        if ( key !== null ) {
            this.processes.set( key, transcodingProcess );
        }

        return transcodingProcess;
    }
}