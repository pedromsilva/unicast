import Plugin from '/Unicast/Components/Plugin';

export default class TranscodingPlugin extends Plugin {
    dependsOn : Array<String> = [];

    target : string = 'server';

    async install ( server, System ) {
        let { 'default': TranscodersManager } = await System.import( '/Unicast/Plugins/Transcoding/Manager' );

        server.component( 'transcoders', new TranscodersManager( server ) );
    }
}