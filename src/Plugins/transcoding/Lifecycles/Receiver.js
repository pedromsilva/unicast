import Lifecycle from './Lifecycle';

export default class Receiver extends Lifecycle {
    static processes = new Map();

    get processes () {
        return this.constructor.processes;
    }

    constructor ( process, receiver ) {
        super( process );

        this.receiver = receiver;

        if ( this.processes.has( receiver ) ) {
            this.processes.get( receiver ).sleep();
        }

        this.processes.set( this.receiver, this );
    }
}