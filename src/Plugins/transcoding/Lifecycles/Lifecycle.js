export default class Lifecycle {
    constructor ( process ) {
        this.process = process;
        this._locked = false;
    }

    locked ( value = null ) {
        if ( value === null ) {
            return this._locked;
        }

        this._locked = value;

        return this;
    }

    lock () {
        return this.locked( true );
    }

    unlock () {
        return this.locked( false );
    }

    disable () {

    }

    awake () {
        this.process.resume();
    }

    sleep () {
        if ( !this.locked() ) {
            this.process.pause();
        }
    }

    terminate () {
        this.process.stop();
    }
}