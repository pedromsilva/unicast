import DefaultSender from '/Unicast/Plugins/DefaultReceiver/Sender';
import is from 'is';

export default class Sender extends DefaultSender {
    constructor ( router ) {
        super( router );

        this.streamEndMethods = [ 'release' ].concat( this.streamEndMethods );

        this.subtitlesFormat = 'vtt';
    }
}
