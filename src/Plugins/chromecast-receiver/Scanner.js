import Evented from '/Unicast/Utilities/Evented';
import mdns from 'multicast-dns';
import extend from 'extend';
import is from 'is';

export default class Scanner extends Evented {
    constructor ( options = {} ) {
        super();

        this.options = extend( {
            timeout: 10000,
            serviceName: '_googlecast._tcp.local',
            serviceType: 'PTR',
            mdns: {}
        }, options );

        this.mdns = mdns( this.options.mdns );

        this.mdns.on( 'response', this.onResponse.bind( this ) );

        this.mdns.query( {
            questions:[ {
                name: this.options.serviceName,
                type: this.options.serviceType
            } ]
        } );
    }

    update ( info, response ) {
        this.emit( 'device', {
            name: info.name.split( '.' ).slice( 0, -1 ).join( '.' ),
            address: info.data
        }, response );
    }

    onResponse ( response ) {
        var info = response.additionals.find( entry => entry.type === 'A' );

        if ( !info || ( this.options.name && info.name !== this.options.name) ) {
            return;
        }

        this.update( info, response );
    }
}