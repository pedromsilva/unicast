import Factory from '/Unicast/Utilities/Factory';
import truncate from 'truncate';
import extend from 'extend';
import is from 'is';

export default class MediaFactory extends Factory {
	constructor () {
		super();

		this.define( 'generic', this.makeGenericRequest.bind( this ) );
		this.define( 'movie', this.makeMovieRequest.bind( this ) );
		this.define( 'show', this.makeShowRequest.bind( this ) );
	}

	mediaRange ( options ) {
		let parts = [];

		if ( options.range && options.range.start ) {
			parts.push( 'start=' + options.range.start );
		}

		if ( options.range && options.range.end ) {
			parts.push( 'end=' + options.range.end );
		}

		if ( parts.length > 0 ) {
			return '?' + parts.join( '&' );
		}

		return '';
	}

	makeDefaultMedia ( media, server, receiver, options = {}, custom = {} ) {
		let sender = server.sender( receiver );

		let urlParams = {
			receiver: receiver.name,
			media: media.get( 'id' )
		};

		let range = this.mediaRange( options );

		let message = {
			contentId: sender.url( 'video', urlParams ) + range,
			contentType: 'video/mp4',
			tracks: null,
			metadata: {
				type: 0,
				metadataType: 0,
				itemId: media.get( 'id' ),
				title: truncate( media.get( 'title' ), 40 ),
				images: [
					{ url: media.get( 'cover' ) }
				]
			}
		};

		if ( true ) {
			let mediaSubtitles = media.related( 'subtitles' );

            let tracks = [];
			for ( let subtitles of mediaSubtitles.models ) {
				tracks.push( {
					trackId: subtitles.get( 'id' ),
					type: 'TEXT',
					trackContentId: sender.url( 'subtitles', extend( { id: subtitles.get( 'id' ) }, urlParams ) ) + range,
					trackContentType: 'text/vtt',
					name: subtitles.get( 'name' ) || 'Português',
					language: subtitles.get( 'language' ) || 'pt-PT',
					subtype: 'SUBTITLES'
				} );
			}

			if ( tracks.length > 0 ) {
				message.tracks = tracks;
			}
		}

		return extend( true, message, custom );
	}

	makeGenericRequest ( media, server, receiver, options = {}, custom = {} ) {
		return this.makeDefaultMedia( media, server, receiver, options, extend( true, {
			metadata: {
				metadataType: 0
			}
		}, custom ) );
	}

	makeMovieRequest ( media, server, receiver, options = {}, custom = {} ) {
		return this.makeDefaultMedia( media, server, receiver, options, extend( true, {
			metadata: {
				metadataType: 1,
				releaseDate: media.get( 'data' ).year ? media.get( 'data' ).year + '-01-01' : null
			}
		}, custom ) );
	}

	makeShowRequest ( media, server, receiver, options = {}, custom = {} ) {
		return this.makeDefaultMedia( media, server, receiver, options, extend( true, {
			metadata: {
				metadataType: 2,
				seriesTitle: media.get( 'data' ).showTitle,
				episode: media.get( 'data' ).episode,
				season: media.get( 'data' ).season
			}
		}, custom ) );
	}

	make ( media = null, ...options ) {
		return super.make( media.get( 'type' ), media, ...options );
	}
}