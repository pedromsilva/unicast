import Plugin from '/Unicast/Components/Plugin';

export default class ChromecastReceiverPlugin extends Plugin {
    dependsOn : Array<String> = [ 'default-receiver', 'providers', 'transcoding-exhale' ];

    async install ( server, System ) {
        let { 'default' : Receiver } = await System.import( '/Unicast/Plugins/ChromecastReceiver/Receiver' );

        server.get( 'receivers' ).register( Receiver );
    }
}