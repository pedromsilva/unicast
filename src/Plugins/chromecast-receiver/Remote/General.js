import Evented from '/Unicast/Utilities/Evented';
import objectPath from 'object-path';
import { Client } from 'castv2-client';
import promisify from 'es6-promisify';
import is from 'is';

export default class Remote extends Evented {
    connected : boolean = false;

    connection = null;

    appId : string = null;

    application = null;

    constructor ( address ) {
        super();

        this.address = address;

        this.eraseConnection();
    }

    eraseConnection () {
        this.connected = false;
        this.connection = null;

        this.client = null;
        this.player = null;
        this.playerAsync = {};
        this.clientAsync = {};
    }

    setConnection ( client, player ) {
        this.client = client;
        this.player = player;

        this.client.on( 'status', status => {
            let connectedStill = status && is.array( status.applications ) && status.applications.some( app => app.appId === this.application.APP_ID );

            if ( !connectedStill ) {
                this.eraseConnection();
            }

            this.emit( 'app-status', status );
        } );
    }

    async callPlayerMethod ( name : string, args : Array = [], events = [] ) {
        await this.ensureConnection();

        return this.callAsyncMethod( this.player, this.playerAsync, name, args, events );
    }

    async callClientMethod ( name : string, args : Array = [], events = [] ) {
        await this.ensureConnection();

        return this.callAsyncMethod( this.client, this.clientAsync, name, args, events );
    }

    async callAsyncMethod ( original : Object, async : Object, name : string, args : Array = [], events = [] ) {
        if ( is.string( events ) ) {
            events = [ null, events ];
        }

        if ( !( name in async ) ) {
            let parent = name.split( '.' ).length > 1 ? objectPath.get( original, name.split( '.' ).slice( 0, -1 ).join( '.' ) ) : original;

            async[ name ] = promisify( objectPath.get( original, name ).bind( parent ) );
        }

        if ( events[ 0 ] ) {
            this.emit( events[ 0 ] );
        }

        let result = async[ name ]( ...args );

        if ( events[ 1 ] ) {
            this.emit( events[ 1 ], result );
        }

        return result;
    }

    async ensureConnection () {
        if ( this.connected ) {
            return this.connection;
        }

        return this.reconnect();
    }

    reconnect () {
        this.connected = true;

        this.connection = new Promise( ( resolve, reject ) => {
            try {
                if ( this.client ) {
                    this.client.close();
                }

                let client = new Client();
                client.connect( this.address, () => {
                    client.launch( this.application, ( err, player ) => {
                        if ( err ) {
                            this.emit( 'error', error );

                            client.close();

                            return reject( error );
                        }

                        this.setConnection( client, player );

                        this.emit( 'connected' );

                        resolve( [ client, player ] );
                    } );
                } );

                client.on( 'error', err => {
                    this.emit( 'error', err );

                    client.close();

                    reject( err );
                } )
            } catch ( err ) {
                reject( err );
            }
        } );

        return this.connection;
    }

    getStatus () {
        return Promise.race( [
            this.callPlayerMethod( 'getStatus', [], 'status' ),
            new Promise( ( _, reject ) => setTimeout( reject.bind( null, new Error( 'Chromecast getStatus timeout.' ) ), 5000 ) )
        ] );
    }

    close () {
        if ( this.connected ) {
            this.emit( 'disconnecting' );

            this.player.close();

            this.client.close();

            this.eraseConnection();

            this.emit( 'disconnected' );
        }
    }
}