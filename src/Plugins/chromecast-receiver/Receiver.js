import { Buffered, Live } from '/Unicast/Plugins/Providers/StreamTypes';
import TranscodersManager from '/Unicast/Plugins/Transcoding/Manager';
import chromecastTranscoder from './Transcoder';
import MediaFactory from './MediaFactory';
import BaseReceiver from '/Unicast/Plugins/DefaultReceiver/Receiver';
import Sender from './Sender';
import DefaultMediaReceiver from './Remote/DefaultMedia';
import Scanner from './Scanner';
import relay from 'event-relay';
import extend from 'extend';
import is from 'is';

export default class Receiver extends BaseReceiver {
    static get defaultMediaFactory () {
        if ( !this._mediaFactory ) {
            this._mediaFactory = new MediaFactory();
        }

        return this._mediaFactory;
    }

    static type : string = 'chromecast';

    static scan () {
        let scanner = new Scanner();

        return scanner.on( 'device' ).map( device => {
            return device.data[ 0 ];
        } );
    }

    constructor ( server, name, data = {} ) {
        super( server, name );

        this.address = data.address;

        this.client = new DefaultMediaReceiver( data.address );

        this.client.on( 'connected', relay( 'connected', this ) );
        this.client.on( 'playing', relay( 'playing', this ) );
        this.client.on( 'played', relay( 'played', this ) );
        this.client.on( 'stopping', relay( 'stopping', this ) );
        this.client.on( 'stopped', relay( 'stopped', this ) );
        this.client.on( 'pausing', relay( 'pausing', this ) );
        this.client.on( 'paused', relay( 'paused', this ) );
        this.client.on( 'resuming', relay( 'resuming', this ) );
        this.client.on( 'resumed', relay( 'resumed', this ) );
        this.client.on( 'app-status', relay( 'app-status', this ) );
        this.client.on( 'status', relay( 'inner-status', this ) );
        this.client.on( 'error', relay( 'error', this ) );
        this.client.on( 'disconnected', relay( 'disconnected', this ) );

        this.mediaFactory = Receiver.defaultMediaFactory;

        this.transcoders.register( chromecastTranscoder() );
    }

    createSender ( router ) : Sender {
        return new Sender( router );
    }

    async play ( item, video = null, config = {}, options = {}, media = {} ) {
        if ( !is.object( options ) ) {
            options = { currentTime: options };
        }

        options = extend( {
            autoplay: true || config.autoplay,
            currentTime: item.currentTime
        }, options );

        if ( config && config.range && config.range.start ) {
            if ( await video.transcodable ) {
                options.currentTime = 0;

                if ( !( 'metadata' in media ) ) {
                    media.metadata = {};
                }

                media.metadata.offset = config.range.start;
            } else {
                options.currentTime = config.range.start;
            }
        }

        media = this.mediaFactory.make( item, this.server, this, config, media );

        if ( video ) {
            media.duration = await video.duration;
        }

        if ( video ) {
            media.streamType = 'BUFFERED';
        }

        if ( media.tracks && media.tracks.length ) {
            options.activeTrackIds = [ media.tracks[ 0 ].trackId ];
        }

        if ( !media.textTrackStyle ) {
            media.textTrackStyle = this.getSubtitlesStyle();
        }

        this.client.subtitles_style = media.textTrackStyle;

        this.emit( 'playing', item );

        await this.client.load( media, options );

        await super.play( item );

        this.emit( 'play', item );

        return media;
    }

    getSubtitlesStyle () {
        return {
            backgroundColor: '#FFFFFF00', // see http://dev.w3.org/csswg/css-color/#hex-notation
            foregroundColor: '#FFFFFFFF', // see http://dev.w3.org/csswg/css-color/#hex-notation
            edgeType: 'OUTLINE', // can be: "NONE", "OUTLINE", "DROP_SHADOW", "RAISED", "DEPRESSED"
            edgeColor: '#000000FF', // see http://dev.w3.org/csswg/css-color/#hex-notation
            fontScale: 1.5, // transforms into "font-size: " + (fontScale*100) +"%"
            fontStyle: 'BOLD', // can be: "NORMAL", "BOLD", "BOLD_ITALIC", "ITALIC",
            fontFamily: 'Droid Sans',
            fontGenericFamily: 'CURSIVE', // can be: "SANS_SERIF", "MONOSPACED_SANS_SERIF", "SERIF", "MONOSPACED_SERIF", "CASUAL", "CURSIVE", "SMALL_CAPITALS",
            windowColor: '#AA00FFFF', // see http://dev.w3.org/csswg/css-color/#hex-notation
            windowRoundedCornerRadius: 10, // radius in px
            windowType: 'ROUNDED_CORNERS' // can be: "NONE", "NORMAL", "ROUNDED_CORNERS"
        };
    }

    changeSubtitles ( ...args ) {
        return this.client.changeSubtitles( ...args );
    }

    subtitlesOff ( ...args ) {
        return this.client.subtitlesOff( ...args );
    }

    pause ( ...args ) {
        return this.client.pause( ...args );
    }

    resume ( ...args ) {
        return this.client.resume( ...args );
    }

    changeSubtitlesSize ( ...args ) {
        return this.client.changeSubtitlesSize( ...args );
    }

    async getStatus ( ...args ) {
        let status = await this.client.getStatus();

        if ( status && status.media ) {
            let item = await this.server.media.get( status.media.metadata.itemId );

            let video = await this.video( item );

            if ( await video.transcodable ) {
                status.media.duration = await video.duration;

                if ( status.media.metadata.offset ) {
                    status.currentTime += +status.media.metadata.offset;
                }
            }
        }

        this.emit( 'status', status );

        return status;
    }

    seek ( ...args ) {
        return this.client.seek( ...args );
    }

    seekTo ( ...args ) {
        return this.client.seekTo( ...args );
    }

    async seekToTime ( time ) {
        let item = await this.current;

        if ( !item ) {
            throw new Error( `No media currently playing in the receiver "${ this.name }"` );
        }

        let video = await this.video( item );

        if ( await video.transcodable ) {
            return this.play( item, video, {
                range: {
                    start: time
                }
            } )
        } else {
            await this.seekTo( time );

            return this.resume();
        }
    }

    async seekToPercentage ( percentage ) {
        let status = await this.getStatus();

        let position = status.media.duration * Math.min( 100, Math.max( 0, percentage ) ) / 100;

        await this.pause();

        await this.seekTo( position );

        await this.resume();
    }

    changeVolume ( ...args ) {
        return this.client.setVolume( ...args );
    }

    changeVolumeMuted ( ...args ) {
        return this.client.setVolumeMuted( ...args );
    }

    async stop ( ...args ) {
        try {
            let result = await this.client.stop();

            await super.stop();

            return result;
        } catch ( error ) {
            if ( error.message == 'Cannot read property \'mediaSessionId\' of null' ) {
                return false;
            } else {
                throw error;
            }
        }
    }

    async close () {
        return this.client.close();
    }
}