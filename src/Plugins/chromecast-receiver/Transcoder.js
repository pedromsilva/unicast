import dsl from '/Unicast/Plugins/TranscodingExhale/DSL';

export default function transcoder () {
    return dsl( `
        // Library
        def range $min $max {
            return { min: $min, max: $max };
        }

        def scale $value $rangeLinear $rangeLog =
            ( $value - $rangeLinear.min ) * ( $rangeLog.max - $rangeLog.min ) / ( $rangeLinear.max - $rangeLinear.min ) + $rangeLog.min

        def linearToLog $value $rangeLinear $rangeLog = scale $value $rangeLinear $rangeLog

        def qualityToCRF $quality $min $max = linearToLog $quality ( range 0 100 ) ( range $min $max )

        // Config
        let $video   = $media.transcode.video
        let $audio   = $media.transcode.audio
        let $quality = $media.transcode.quality or 70
        let $crf     = qualityToCRF $quality 28 18;

        // Tasks
        def audio :: a = if $audio or not ( aac or ac3 ) then ac3

        def video :: v = if $video or ( not h264 ) or ( bitrate >= 9M ) then h264 $crf

        def main :: default = ( copy, audio, video )

        // Execute
        if audio or video then ( main, mkv )
	` );

    /*
     //def config = (
     //    ui.list(
     //        ui.toggle  ( label = 'Video',       name = 'video',       value = $video                                   ),
     //        ui.toggle  ( label = 'Audio',       name = 'audio',       value = $audio                                   ),
     //        ui.slider  ( label = 'Quality',     name = 'quality',     value = $quality, min = 0, max = 100             ),
     //        ui.dropdown( label = 'Video Track', name = 'videoTracks', value = defaultVideoTrack, options = videoTracks ),
     //        ui.dropdown( label = 'Audio Track', name = 'audioTracks', value = defaultAudioTrack, options = audioTracks )
     //    )
     //)

     //import 'transcoding/ffmpeg'
     //import 'config' as ui
     //import 'math'

    */
}