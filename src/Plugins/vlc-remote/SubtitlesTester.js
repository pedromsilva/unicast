import RemoteControl from './RemoteControl';
import parser from 'subtitles-parser';
import Immutable from 'immutable';
import fs from 'fs-promise';
import path from 'path';
import mess from 'mess';

export default class SubtitlesTester {
    constructor ( video, subtitles ) {
        this.video = video;
        this.subtitles = subtitles || this.getVideoSubtitlesPath();
    }

    getVideoSubtitlesPath () {
        return path.join( path.dirname( this.video ), path.basename( this.video, path.extname( this.video ) ) + '.srt' );
    }

    async getSubtitles () {
        let contents = await fs.readFile( this.subtitles, 'utf8' );

        return Immutable.fromJS( parser.fromSrt( contents, true ) );
    }

    random ( low, high ) {
        return Math.random() * ( high - low + 1 ) + low;
    }

    getRandomLineIds ( lines, container, count ) {
        count = Math.min( count, lines.size );

        let found = 0;
        while ( found < count ) {
            let random = this.random( 0, lines.size - 1 );

            if ( lines.get( random ).get( 'id' ) in container ) {
                continue;
            }

            container[ lines.get( random ).get( 'id' ) ] = true;

            found++;
        }
    }

    getRandomLines ( lines, count = 5 ) {
        let ids = {};

        let long = lines.filter( line => line.get( 'endTime' ) - line.get( 'startTime' ) >= 3000 );

        this.getRandomLineIds( long, ids, Math.min( count, long.size ) );

        if ( long.size < count ) {
            let short = lines.filter( line => line.get( 'endTime' ) - line.get( 'startTime' ) < 3000 );

            this.getRandomLineIds( short, ids, count - long.size );
        }

        return Immutable.List( mess( Array.from( lines.filter( line => line.get( 'id' ) in ids ) ) ) );
    }

    getSectionedLines ( lines, count = 5, offset = 10 ) {
        lines = lines.filter( line => line.get( 'endTime' ) - line.get( 'startTime' ) >= 3000 );

        let skippedCount = Math.floor( offset * lines.size / 100 );

        let size = lines.size - skippedCount * 2;

        count = Math.min( count, size );

        let result = [];
        for ( let i = 0; i < count; i++ ) {
            result.push( lines.get( skippedCount + Math.floor( size / count * ( i + 1 ) ) ) );
        }

        result = mess( result );

        return Immutable.List( result );
    }

    async run ( controller, lines ) {
        await controller.launch( { subtitles: this.subtitles } );

        await controller.add( this.video );

        await controller.fullscreen();

        await controller.wait( 2000 );

        for ( let line of lines ) {
            let start = Math.floor( line.get( 'startTime' ) / 1000 ) - 1;

            let end = Math.ceil( line.get( 'endTime' ) / 1000 ) + 1;

            await controller.seek( start );

            await controller.wait( ( end - start ) * 1000 );
        }

        await controller.stop();

        await controller.quit();
    }

    async launch ( linesCount = 3, ...args ) {
        this.controller = new RemoteControl( ...args );

        let subtitles = await this.getSubtitles();

        let lines = this.getSectionedLines( subtitles, linesCount );

        await this.run( this.controller, lines );
    }

    async stop () {
        if ( this.controller.client ) {
            await this.controller.stop();

            await this.controller.quit();

            await this.controller.wait( 500 );
        }
    }
}