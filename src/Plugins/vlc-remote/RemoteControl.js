import net from 'net';
import readline from 'readline';
import EventEmitter from '/Unicast/Utilities/Evented';
import LiveVideo from '/Unicast/Utilities/LiveVideo';
import { spawn, exec } from 'child_process';
import path from 'path';
import got from 'got';
import fs from 'fs';

/**
 +----[ Remote control commands ]
 |
 | add XYZ  . . . . . . . . . . . . add XYZ to playlist
 | enqueue XYZ  . . . . . . . . . queue XYZ to playlist
 | playlist . . . . .  show items currently in playlist
 | play . . . . . . . . . . . . . . . . . . play stream
 | stop . . . . . . . . . . . . . . . . . . stop stream
 | next . . . . . . . . . . . . . .  next playlist item
 | prev . . . . . . . . . . . .  previous playlist item
 | goto . . . . . . . . . . . . . .  goto item at index
 | repeat [on|off] . . . .  toggle playlist item repeat
 | loop [on|off] . . . . . . . . . toggle playlist loop
 | random [on|off] . . . . . . .  toggle random jumping
 | clear . . . . . . . . . . . . . . clear the playlist
 | status . . . . . . . . . . . current playlist status
 | title [X]  . . . . . . set/get title in current item
 | title_n  . . . . . . . .  next title in current item
 | title_p  . . . . . .  previous title in current item
 | chapter [X]  . . . . set/get chapter in current item
 | chapter_n  . . . . . .  next chapter in current item
 | chapter_p  . . . .  previous chapter in current item
 |
 | seek X . . . seek in seconds, for instance `seek 12'
 | pause  . . . . . . . . . . . . . . . .  toggle pause
 | fastforward  . . . . . . . .  .  set to maximum rate
 | rewind  . . . . . . . . . . . .  set to minimum rate
 | faster . . . . . . . . . .  faster playing of stream
 | slower . . . . . . . . . .  slower playing of stream
 | normal . . . . . . . . . .  normal playing of stream
 | f [on|off] . . . . . . . . . . . . toggle fullscreen
 | info . . . . .  information about the current stream
 | stats  . . . . . . . .  show statistical information
 | get_time . . seconds elapsed since stream's beginning
 | is_playing . . . .  1 if a stream plays, 0 otherwise
 | get_title . . . . .  the title of the current stream
 | get_length . . . .  the length of the current stream
 |
 | volume [X] . . . . . . . . . .  set/get audio volume
 | volup [X]  . . . . . . .  raise audio volume X steps
 | voldown [X]  . . . . . .  lower audio volume X steps
 | adev [X] . . . . . . . . . . .  set/get audio device
 | achan [X]. . . . . . . . . .  set/get audio channels
 | atrack [X] . . . . . . . . . . . set/get audio track
 | vtrack [X] . . . . . . . . . . . set/get video track
 | vratio [X]  . . . . . . . set/get video aspect ratio
 | vcrop [X]  . . . . . . . . . . .  set/get video crop
 | vzoom [X]  . . . . . . . . . . .  set/get video zoom
 | snapshot . . . . . . . . . . . . take video snapshot
 | strack [X] . . . . . . . . . set/get subtitles track
 | key [hotkey name] . . . . . .  simulate hotkey press
 | menu . . [on|off|up|down|left|right|select] use menu
 |
 | @name marq-marquee  STRING  . . overlay STRING in video
 | @name marq-x X . . . . . . . . . . . .offset from left
 | @name marq-y Y . . . . . . . . . . . . offset from top
 | @name marq-position #. . .  .relative position control
 | @name marq-color # . . . . . . . . . . font color, RGB
 | @name marq-opacity # . . . . . . . . . . . . . opacity
 | @name marq-timeout T. . . . . . . . . . timeout, in ms
 | @name marq-size # . . . . . . . . font size, in pixels
 |
 | @name logo-file STRING . . .the overlay file path/name
 | @name logo-x X . . . . . . . . . . . .offset from left
 | @name logo-y Y . . . . . . . . . . . . offset from top
 | @name logo-position #. . . . . . . . relative position
 | @name logo-transparency #. . . . . . . . .transparency
 |
 | @name mosaic-alpha # . . . . . . . . . . . . . . alpha
 | @name mosaic-height #. . . . . . . . . . . . . .height
 | @name mosaic-width # . . . . . . . . . . . . . . width
 | @name mosaic-xoffset # . . . .top left corner position
 | @name mosaic-yoffset # . . . .top left corner position
 | @name mosaic-offsets x,y(,x,y)*. . . . list of offsets
 | @name mosaic-align 0..2,4..6,8..10. . .mosaic alignment
 | @name mosaic-vborder # . . . . . . . . vertical border
 | @name mosaic-hborder # . . . . . . . horizontal border
 | @name mosaic-position {0=auto,1=fixed} . . . .position
 | @name mosaic-rows #. . . . . . . . . . .number of rows
 | @name mosaic-cols #. . . . . . . . . . .number of cols
 | @name mosaic-order id(,id)* . . . . order of pictures
 | @name mosaic-keep-aspect-ratio {0,1} . . .aspect ratio
 |
 | help . . . . . . . . . . . . . . . this help message
 | longhelp . . . . . . . . . . . a longer help message
 | logout . . . . . . .  exit (if in socket connection)
 | quit . . . . . . . . . . . . . . . . . . .  quit vlc
 |
 +----[ end of help ]
 */

export default class RemoteControl extends EventEmitter {
    host : string;
    port : number;
    vlcPath : string;

    constructor ( host : string = '127.0.0.1', port : number = 1255 ) {
        super();

        this.host = host;
        this.port = port;
        this.vlcPath = 'C:\\Program Files (x86)\\VideoLAN\\VLC\\vlc.exe';
    }

    async command ( name : string, ...params : string ) : RemoteControl {
        await this.connecting;

        if ( !this.client ) {
            return this;
        }

        await new Promise( resolve => {
            this.client.write( [ name ].concat( params ).join( ' ' ) + '\n', () => {
                resolve();
            } );
        } );

        return this;
    }

    fullscreen ( state : boolean = true ) : RemoteControl {
        return this.command( 'f', state ? 'on' : 'off' );
    }

    add ( item : string ) : RemoteControl {
        return this.command( 'add', item );
    }

    enqueue ( item : string ) : RemoteControl {
        return this.command( 'enqueue', item );
    }

    play ( item : string ) : RemoteControl {
        return this.command( 'play', item );
    }

    stop () : RemoteControl {
        return this.command( 'stop' );
    }

    pause () : RemoteControl {
        return this.command( 'pause' );
    }

    seek ( seconds : number ) : RemoteControl {
        return this.command( 'seek', seconds );
    }

    async quit () : RemoteControl {
        await this.command( 'quit' );

        await this.disconnected();

        return this;
    }

    disconnected () {
        return new Promise( ( resolve, reject ) => {
            this.once( 'disconnect', () => {
                resolve();
            } );
        } );
    }

    wait ( mills ) {
        return new Promise( resolve => setTimeout( resolve, mills ) );
    }

    async spawn ( options : Object = {} ) {
        let command = path.basename( this.vlcPath, path.extname( this.vlcPath ) );

        let args = [ `-I`, `rc`, `--rc-host=${this.host}:${this.port}`, '--rc-quiet' ];

        if ( options.subtitles ) {
            let subs = options.subtitles;

            if ( subs.startsWith( 'http://' ) || subs.startsWith( 'https://' ) ) {
                let subsContent = await got( subs ).then( res => res.body );

                subs = await LiveVideo.reserve( 'srt' );

                await fs.writeFile( subs, subsContent, 'utf8' );
            }

            args.push( `--sub-file=${subs}`, '--no-sub-autodetect-file', '--sub-track-id=-1' );
        }

        spawn( command, args, {
            cwd: path.dirname( this.vlcPath ),
            stdio: 'inherit'
        } );
    }

    createClient ( tries = 5, maxTries = null ) {
        maxTries = maxTries || tries;

        return new Promise( ( resolve, reject ) => {
            let client = net.createConnection( this.port, this.host );

            client.on( 'connect', () => {
                this.emit( 'connect' );

                resolve( client );
            } );

            client.on( 'error', ( error ) => {
                if ( tries > 0 ) {
                    resolve( this.wait( 500 * ( Math.pow( 2, maxTries - tries ) ) ).then( () => this.createClient( tries - 1, maxTries ) ) );
                } else {
                    reject( error );
                }
            } );

            client.once( 'end', () => {
                this.client.unref();

                this.client = null;

                this.emit( 'disconnect' );
            } );
        } );
    }

    async launch ( options : Object = {} ) : RemoteControl {
        await this.spawn( options );

        this.connecting = this.createClient();

        this.client = await this.connecting;

        this.client.on( 'data', data => {
            //console.log( data.toString() );
        } );

        return this;
    }
}