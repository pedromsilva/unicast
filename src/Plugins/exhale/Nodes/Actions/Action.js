import Node from '../Node';
import Caster, { Unwrap } from '../Scalars/Caster';
import extend from 'extend';

export default class Action extends Node {
    constructor ( closure, action, parameters = [] ) {
        super( closure );
        this.action = action;
        this.parameters = parameters;
    }

    get isConditional () {
        let definition = this.closure.getDefinition( 'type', this.action );

        return definition && definition.isDefinition;
    }

    async getParameters ( context ) {
        return Promise.all( this.parameters.map( async parameter => {
            return extend( parameter, {
                value: Caster( this, await Unwrap( context, 'execute', parameter.value ) )
            } );
        } ) );
    }

    async getNamedParameters ( context ) {
        let values = await this.getParameters( context );

        let hash = {};

        for ( let parameter of values ) {
            if ( parameter.name ) {
                hash[ parameter.name ] = parameter;
            }
        }

        return hash;
    }

    async mapParameterValues ( context, target ) {
        let parameters = await target.getParameters( context );

        let values = ( await this.getParameters( context ) ).filter( p => !p.name );

        let namedValues = await this.getNamedParameters( context );

        let mapped = [];

        let index = 0;
        for ( let parameter of parameters ) {
            if ( parameter.name in namedValues ) {
                mapped.push( namedValues[ parameter.name ].value )
            } else {
                mapped.push( values[ index++ ].value );
            }
        }

        while ( index < values.length ) {
            mapped.push( values[ index++ ].value );
        }

        return mapped;
    }

    async evaluate ( context ) {
        let definition = this.closure.getDefinition( 'closure', this.action );

        if ( definition && definition.evaluate ) {
            return definition.call( context, 'evaluate', await this.mapParameterValues( context, definition ) );
        }

        throw new Error( `Action named "${this.action}" is not defined in the current scope.` );
    }

    async execute ( context ) {
        let definition = this.closure.getDefinition( 'closure', this.action );

        if ( definition ) {
            return definition.call( context, 'execute', await this.mapParameterValues( context, definition ) );
        }

        throw new Error( `Action named "${this.action}" is not defined in the current scope.` );
    }

    toString () {
        let body = this.action;

        if ( this.parameters.length ) {
            body += '( ' + this.parameters.join( ', ' ) + ' )';
        }

        return body;
    }
}