import Node from '../Node';

export default class ActionsList extends Node {
    constructor ( closure ) {
        super( closure );

        this.instructions = [];
    }

    get isConditional () {
        return true;
    }

    addInstruction ( instruction ) {
        this.instructions.push( instruction );

        return this;
    }

    //async evaluate ( context ) {
    //    let result = false;
    //
    //    for ( let instruction of this.instructions ) {
    //        if ( instruction.isIgnorable ) {
    //            continue;
    //        }
    //
    //        if ( instruction.isConditional ) {
    //            if ( !instruction.call ) {
    //                console.log( instruction );
    //            }
    //
    //            result = await instruction.call( context, 'evaluate' );
    //
    //            if ( result ) {
    //                return true;
    //            }
    //        } else {
    //            return true;
    //        }
    //    }
    //
    //    return result;
    //}

    async calculate ( context, mode ) {
        let result;

        for ( let instruction of this.instructions ) {
            if ( !instruction ) {
                console.log( this );
            }
            if ( instruction.isIgnorable ) {
                continue;
            }

            if ( !instruction.call ) {
                console.log( 'NOT CALL', instruction );
            } else {
                result = await instruction.call( context, mode );
            }
        }

        return result;
    }

    toString () {
        return this.instructions.join( ', ' );
    }
}