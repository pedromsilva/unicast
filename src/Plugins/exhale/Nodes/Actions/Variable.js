import Node from '../Node';

export default class Variable extends Node {
    constructor ( closure, name ) {
        super( closure );

        this.name = name;
    }

    get isVariable () {
        return true;
    }

    async calculate ( context, mode ) {
        return this.closure.getVariable( this.name, context ).call( context, mode );
    }

    toString () {
        return '$' + this.name;
    }
}