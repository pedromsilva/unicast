export default class Node {
    isExhalerNode : boolean = true;
    properties : Array<Node> = [];
    meta : Object = {};

    constructor ( closure, meta = {}, properties = [] ) {
        this.closure = closure;
        this.meta = meta;
        this.properties = properties;
    }

    async getAccessors ( context, value, mode : string = 'execute' ) {
        for ( let property of this.properties ) {
            if ( !value ) {
                return;
            }

            value = value[ await property[ mode ]( context ) ];
        }

        return value;
    }

    evaluate ( context, ...args ) {
        return this.calculate( context, 'evaluate', ...args );
    }

    execute ( context, ...args ) {
        return this.calculate( context, 'execute', ...args );
    }

    call ( context, mode, ...args ) {
        return this.apply( context, mode, args );
    }

    async apply ( context, mode, args ) {
        let value = await this[ mode ]( context, ...args );

        return this.getAccessors( context, value, mode );
    }
}