import Closure from './Definitions/Closures/Native';

export default class Group extends Closure {
    isDefinition : boolean = false;
    type : string = 'group';

    constructor ( parent ) {
        super( parent, null );
    }

    toString () {
        return '( ' + super.toString() + ' )';
    }
}