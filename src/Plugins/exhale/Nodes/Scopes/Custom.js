import Node from '../Node';

export default class Custom extends Node {
    constructor ( closure, name, args = [] ) {
        super( closure );
        this.name = name;
        this.args = args;
    }

    async execute ( context, mode = 'execute' ) {
        let definition = this.closure.getScopeDefinition( this.name );

        if ( definition ) {
            return definition( context, await Promise.all( this.args.map( node => node[ mode ]( context ) ) ) );
        }

        throw new Error( `Custom scope named "${this.name}" is not defined in the current scope.` );
    }

    toString () {
        return this.name;
    }
}