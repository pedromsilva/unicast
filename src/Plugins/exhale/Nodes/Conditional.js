import Node from './Node';

export default class Conditional extends Node {
    constructor ( closure, condition, consequent, alternative = null ) {
        super( closure );

        this.condition = condition;
        this.consequent = consequent;
        this.alternative = alternative;
    }

    get isConditional () {
        return true;
    }

    async evaluate ( context ) {
        let result = await this.condition.call( context, 'evaluate' );

        if ( result ) {
            if ( this.consequent.isConditional ) {
                return !!this.consequent.call( context, 'evaluate' );
            }

            return true;
        } else if ( this.alternative ) {
            if ( this.alternative.isConditional ) {
                return !!this.alternative.call( context, 'evaluate' );
            }

            return true;
        }

        return false;
    }

    async execute ( context ) {
        if ( await this.evaluate( context ) ) {
            return this.consequent.call( context, 'execute' );
        } else if ( this.alternative ) {
            return this.alternative.call( context, 'execute' );
        }
    }

    toString () {
        let body = 'if ' + this.condition.toString() + ' then ' + this.consequent.toString();

        if ( this.alternative ) {
            body += ' else ' + this.alternative.toString();
        }

        return body;
    }
}