import Node from '../../Node';

export default class Negation extends Node {
    constructor ( closure, expression ) {
        super( closure );

        this.expression = expression;
    }

    async calculate ( context, mode ) {
        return !( await this.expression.call( context, mode ) );
    }

    toString () {
        return 'not ' + this.expression.toString();
    }
}