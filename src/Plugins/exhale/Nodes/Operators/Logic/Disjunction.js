import Node from '../../Node';

export default class Disjunction extends Node {
    constructor ( closure, clauseA, clauseB ) {
        super( closure );

        this.clauseA = clauseA;
        this.clauseB = clauseB;
    }

    async calculate ( context, mode ) {
        let result = await this.clauseA.call( context, mode );

        if ( result ) {
            return true;
        }

        return this.clauseB.call( context, mode );
    }

    toString () {
        return this.clauseA.toString() + ' or ' + this.clauseB.toString();
    }
}