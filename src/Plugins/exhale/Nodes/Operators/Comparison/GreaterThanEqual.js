import Comparator from './Comparator';

export default class GreaterThanEqual extends Comparator {
    compare ( a, b ) {
        return a >= b;
    }

    toString () {
        return super.toString( '>=' );
    }
}