import Comparator from './Comparator';

export default class Equal extends Comparator {
    compare ( a, b ) {
        return a == b;
    }

    toString () {
        return super.toString( '==' );
    }
}