import Node from '../../Node';

export default class Comparator extends Node {
    constructor ( closure, clauseA, clauseB ) {
        super( closure );

        this.clauseA = clauseA;
        this.clauseB = clauseB;
    }

    async calculate ( context, mode ) {
        return this.compare( await this.clauseA.call( context, mode ), await this.clauseB.call( context, mode ) );
    }

    toString ( sign ) {
        return this.clauseA.toString() + ` ${sign} ` + this.clauseB.toString();
    }
}