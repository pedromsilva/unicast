import Node from '../Node';

export default class Execute extends Node {
    constructor ( closure, expression ) {
        super( closure );

        this.expression = expression;
    }

    async calculate ( context ) {
        return await this.expression.call( context, 'execute' );
    }

    toString () {
        return 'exec ' + this.expression.toString();
    }
}