import Node from '../Node';

export default class Evaluate extends Node {
    constructor ( closure, expression ) {
        super( closure );

        this.expression = expression;
    }

    async calculate ( context ) {
        return await this.expression.call( context, 'evaluate' );
    }

    toString () {
        return 'eval ' + this.expression.toString();
    }
}