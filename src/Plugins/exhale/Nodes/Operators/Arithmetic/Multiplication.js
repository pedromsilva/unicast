import Calculator from './Calculator';

export default class Multiplication extends Calculator {
    calc ( a, b ) {
        return a * b;
    }

    toString () {
        return super.toString( '*' );
    }
}