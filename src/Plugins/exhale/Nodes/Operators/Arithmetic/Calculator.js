import Node from '../../Node';

export default class Calculator extends Node {
    constructor ( closure, clauseA, clauseB ) {
        super( closure );

        this.clauseA = clauseA;
        this.clauseB = clauseB;
    }

    async calculate ( context, mode ) {
        return this.calc( await this.clauseA.call( context, mode ), await this.clauseB.call( context, mode ) );
    }

    toString ( sign ) {
        return this.clauseA.toString() + ` ${sign} ` + this.clauseB.toString();
    }
}