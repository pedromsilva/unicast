import Calculator from './Calculator';

export default class Division extends Calculator {
    calc ( a, b ) {
        return a / b;
    }

    toString () {
        return super.toString( '/' );
    }
}