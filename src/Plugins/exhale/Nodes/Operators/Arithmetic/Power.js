import Calculator from './Calculator';

export default class Power extends Calculator {
    calc ( a, b ) {
        return Math.pow( a,  b );
    }

    toString () {
        return super.toString( '^' );
    }
}