import Calculator from './Calculator';

export default class Addition extends Calculator {
    calc ( a, b ) {
        return a + b;
    }

    toString () {
        return super.toString( '+' );
    }
}