import Node from './Node';

export default class Comment extends Node {
    constructor ( closure, content ) {
        super( closure );

        this.content = content;
    }

    get isIgnorable () {
        return true;
    }

    toString () {
        return '//' + this.content;
    }
}