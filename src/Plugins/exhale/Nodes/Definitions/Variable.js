import Node from '../Node';

export default class Variable extends Node {
    isDefinition : boolean = true;

    type : string = 'variable';

    constructor ( closure, name, value ) {
        super( closure );
        this.name = name;
        this.value = value;
    }

    async calculate ( context, mode ) {
        return this.value.call( context, mode );
    }

    toString () {
        return '$' + this.name + ' = ' + this.value.toString();
    }
}