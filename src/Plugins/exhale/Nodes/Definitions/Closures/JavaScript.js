import Closure from './Closure';
import vm, { Script } from 'vm';
import { transform as babel } from 'babel-core';
import Caster from '../../Scalars/Caster';

export default class JavaScript extends Closure {
    constructor ( parent, name, args = [], body = '' ) {
        super( parent, name, args );

        this.body = body;
        this.script = new Script( this.transform( body ) );
        this.vm = vm;
    }

    transform ( code ) {
        // TODO Enable async functions
        let argumentsDefinition = this.arguments.map( arg => '$' + arg.name ).join( ', ' );

        code = `( ( ${argumentsDefinition} ) => { ${code} } )( ...$arguments )`;

        code = `
        //require( 'babel-polyfill' );
        ` + babel( code, {
            'presets': [ 'es2015', 'stage-3' ],
            'plugins': [ 'transform-class-properties', 'transform-flow-strip-types' ]
        } ).code;

        return code;
    }

    bindActionToMode ( action, context, mode ) {
        return ( ...args ) => {
            return action.call( context, mode, args );
        };
    }

    async addVariablesToVMContext ( jsContext, context, mode ) {
        jsContext.global = jsContext.GLOBAL = jsContext.root = jsContext;
        jsContext.eval = mode === 'evaluate';
        jsContext.exec = mode === 'execute';

        let actions = this.getAllVariables( context );

        for ( let name of Object.keys( actions ) ) {
            jsContext[ '$' + name ] = await actions[ name ].call( context, mode );
        }
    }

    async addActionsToVMContext ( jsContext, context, mode ) {
        jsContext.debug = console.log.bind( console );

        let actions = this.symbols( 'closure' );

        for ( let action of Object.keys( actions ) ) {
            jsContext[ action ] = this.bindActionToMode( actions[ action ], context, mode );
            jsContext[ action ].eval = this.bindActionToMode( actions[ action ], context, 'evaluate' );
            jsContext[ action ].exec = this.bindActionToMode( actions[ action ], context, 'execute' );
        }
    }

    async makeVMContext ( context, mode ) {
        let jsContext = {};

        await this.addActionsToVMContext( jsContext, context, mode );
        await this.addVariablesToVMContext( jsContext, context, mode );

        return this.vm.createContext( jsContext );
    }

    async getArgumentsVariable ( context, mode, args ) {
        return Caster( this, await Promise.all( args.map( variable => variable.call( context, mode ) ) ) );
    }

    getVariablesFromArgs ( args ) {
        return { arguments: args };
    }

    async calculate ( original, mode, args = [] ) {
        original = this.addToStack( await this.getArgumentsVariable( original, mode, args ), original );

        let context = await this.scoped( original );

        context = await this.addVariablesDefinitions( context, mode, this.variables );

        let jsContext = await this.makeVMContext( context, mode );

        let result = this.script.runInNewContext( jsContext );

        return Caster( this, result );
    }

    evaluate ( context, args = [] ) {
        return this.calculate( context, 'evaluate', args );
    }

    execute ( context, args = [] ) {
        return this.calculate( context, 'execute', args );
    }
}