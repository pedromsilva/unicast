import ActionsList from '../../Actions/ActionsList';
import Immutable from 'immutable';
import Caster from '../../Scalars/Caster';

export default class Closure extends ActionsList {
    isDefinition : boolean = true;
    type : string = 'closure';

    constructor ( parent, name, args = [] ) {
        super( parent );

        this.name = name;
        this.arguments = args;

        this.scopeDefinitions = [];
        this.scopes = [];
        this.definitions = {};
    }

    getParameters ( context ) {
        return this.arguments;
    }

    registerDefinition ( type, name, definition ) {
        if ( !( type in this.definitions ) ) {
            this.definitions[ type ] = { type: type, registry: {} };
        }

        this.definitions[ type ].registry[ name ] = definition;

        return this;
    }

    getDefinition ( type, name, local = false ) {
        if ( type in this.definitions && name in this.definitions[ type ].registry ) {
            return this.definitions[ type ].registry[ name ];
        }

        if ( local || !this.closure ) {
            return null;
        }

        return this.closure.getDefinition( type, name );
    }

    getAllDefinitions ( type = null, local = false ) {
        let definitions = [];

        if ( !local && this.closure ) {
            definitions = definitions.concat( this.closure.allDefinitions( type ) );
        }

        let types = ( type ? [ type ] : Object.keys( this.definitions ) ).map( type => this.definitions[ type ] ).filter( a => !!a );

        for ( let each of types ) {
            definitions = definitions.concat( Object.keys( each.registry ).map( name => ( {
                name: name,
                definition: each.registry[ name ]
            } ) ) );
        }

        return definitions;
    }

    getSymbolsType ( type ) {
        let customMethodName = 'getLocal' + type + 's';

        if ( customMethodName in this ) {
            return this[ customMethodName ]();
        } else {
            type = this.definitions[ type ];

            if ( !type ) {
                return {};
            }

            return type.registry;
        }
    }

    symbols ( type = null, local = false ) {
        let symbols = {};
        let definitions;

        if ( !local && this.closure ) {
            definitions = this.closure.symbols( type );

            for ( let def of Object.keys( definitions ) ) {
                symbols[ def ] = definitions[ def ];
            }
        }

        let types = ( type ? [ type ] : Object.keys( this.definitions ) ).map( type => this.definitions[ type ] ).filter( a => !!a );

        for ( let each of types ) {
            for ( let def of Object.keys( each.registry ) ) {
                symbols[ def ] = each.registry[ def ]
            }
        }

        return symbols;
    }

    registerScopeDefinition ( name, scope ) {
        return this.registerDefinition( 'scope', name, scope );
    }

    getScopeDefinition ( name, local = false ) {
        return this.getDefinition( 'scope', name, local );
    }

    addScope ( scope ) {
        this.scopes.push( scope );

        return this;
    }

    getVariablesFromArgs ( args ) {
        let variables = {};

        for ( let [ index, arg ] of this.arguments.entries() ) {
            variables[ arg.name ] = args[ index ];
        }

        return variables;
    }

    getParentStack ( context ) {
        let stack = context.get( 'stack' );

        let current = stack.get( 'current' );

        let map = stack.get( 'map' );

        while ( map.has( current ) ) {
            let segment = map.get( current );

            if ( segment.get( 'closure' ) == this.closure ) {
                return current;
            }

            current = segment.get( 'parent' );
        }

        return null;
    }

    addToStack ( args, context ) {
        if ( !context.has( 'stack' ) ) {
            context = context.set( 'stack', Immutable.Map( {
                current: null,
                generator: 1,
                map: Immutable.Map()
            } ) );
        }

        let stack = context.get( 'stack' );

        let current = this.getParentStack( context );

        let generator = stack.get( 'generator' );

        stack = stack.set( 'generator', generator + 1 ).set( 'current', generator );

        stack = stack.set( 'map', stack.get( 'map' ).set( generator, Immutable.Map( {
            id: generator,
            parent: current,
            closure: this,
            variables: Immutable.Map( this.getVariablesFromArgs( args ) )
        } ) ) );

        return context.set( 'stack', stack );
    }

    getCurrentStack ( context ) {
        let stack = context.get( 'stack' );

        let current = stack.get( 'current' );

        let map = stack.get( 'map' );

        return map.get( current );
    }

    getVariableStack ( name, context ) {
        let stack = context.get( 'stack' );

        let current = stack.get( 'current' );

        let map = stack.get( 'map' );

        let slice = map.get( current );
        while ( map.has( current ) && !slice.get( 'variables' ).has( name ) ) {
            current = slice.get( 'parent' );

            slice = map.get( current );
        }

        if ( slice ) {
            let variables = slice.get( 'variables' );

            if ( variables && variables.has( name ) ) { //&& typeof ( variables.get( name ) ) !== "undefined" ) {
                return slice;
            }
        }

        return null;
    }

    getVariable ( name, context ) {
        let stack = this.getVariableStack( name, context );

        if ( stack ) {
            return stack.get( 'variables' ).get( name );
        }

        throw new Error( `Couldn't find a variable in context with the name "${name}" in "${this.name}"` );
    }

    * traverseStacks ( context, starts ) {
        let stacks = context.get( 'stack' );

        let current = starts || stacks.get( 'current' );

        let map = stacks.get( 'map' );

        let slice;

        while ( current && map.has( current ) ) {
            slice = map.get( current );

            yield slice;

            current = slice.get( 'parent' );
        }
    }

    getStackAllVariables ( context, stack = null ) {
        let stacks = context.get( 'stack' );

        let current = stack || stacks.get( 'current' );

        let map = stacks.get( 'map' );

        let symbols = {};

        if ( map.has( current ) ) {
            let variables = map.get( current ).get( 'variables' );

            for ( let [ key, value ] of variables ) {
                symbols[ key ] = value;
            }
        }

        return symbols;
    }

    getAllVariables ( context, start = null ) {
        let symbols = {};

        for ( let stack of this.traverseStacks( context, start ) ) {
            let variables = this.getStackAllVariables( context, stack.get( 'id' ) );

            for ( let variable of Object.keys( variables ) ) {
                if ( !( variable in symbols ) ) {
                    symbols[ variable ] = variables[ variable ];
                }
            }
        }

        return symbols;
    }

    setVariable ( name, value, context, definition = false ) {
        let stack = this.getVariableStack( name, context );

        if ( !stack || definition ) {
            stack = this.getCurrentStack( context );
        }

        let variables = stack.get( 'variables' );

        return context.setIn( [ 'stack', 'map', stack.get( 'id' ), 'variables' ], variables.set( name, value ) );
    }

    deleteVariable ( name, context, definition = false ) {
        let stack = this.getVariableStack( name, context );

        if ( !stack || definition ) {
            stack = this.getCurrentStack( context );
        }

        let variables = stack.get( 'variables' );

        return context.setIn( [ 'stack', 'map', stack.get( 'id' ), 'variables' ], variables.delete( name ) );
    }

    addVariablesFromObject ( variables, context ) {
        if ( !variables ) {
            variables = {};
        }

        for ( let name of Object.keys( variables ) ) {
            context = this.setVariable( name, Caster( this, variables[ name ] ), context, true );
        }

        return context;
    }

    async addVariablesDefinitions ( context, mode = 'execute', definitions = null ) {
        if ( !definitions ) {
            definitions = this.getAllDefinitions( 'variable', true ).map( d => d.definition );
        }

        for ( let definition of definitions ) {
            context = this.setVariable( definition.name, Caster( this, await definition[ mode ]( context ) ), context, true );
        }

        return context;
    }

    async scoped ( original ) {
        let context = original;

        for ( let scope of this.scopes ) {
            context = await scope.execute( context );
        }

        return context;
    }

    async evaluate ( original, args = [], variables = {} ) {
        original = this.addToStack( args, original );

        let context = await this.scoped( original );

        context = await this.addVariablesDefinitions( context, 'execute', this.variables );

        context = this.addVariablesFromObject( variables, context );

        context = await this.addVariablesDefinitions( context, 'evaluate' );

        return super.evaluate( context );
    }

    async execute ( original, args = [], variables = {} ) {
        original = this.addToStack( args, original );

        let context = await this.scoped( original );

        context = await this.addVariablesDefinitions( context, 'execute', this.variables );

        context = this.addVariablesFromObject( variables, context );

        context = await this.addVariablesDefinitions( context, 'execute' );

        return super.execute( context );
    }

    // TODO Fix to string methods
    //toString () {
    //    let bodyMain = '';
    //    let definitions = this.getAllDefinitions( null, true ).map( d => d.definition );
    //
    //    if ( definitions.length > 0 ) {
    //        bodyMain += definitions.map( definition => {
    //            let body = definition.name;
    //
    //            if ( definition.arguments.length > 0 ) {
    //                body += ' ' + definition.arguments.map( p => '$' + p ).join( ' ' );
    //            }
    //
    //            if ( definition.scopes.length > 0 ) {
    //                body += ' :: ' + definition.scopes.join( ', ' );
    //            }
    //
    //            if ( definition.instructions.length = 1 ) {
    //                body += ' = ' + definition.instructions[ 0 ].toString();
    //            } else {
    //                body += ' (' +
    //                    definition.instructions.map( i => '\t' + i.toString() ).join( '\n' ) +
    //                    ')'
    //            }
    //
    //            return body + '\n\n';
    //        } ).join( '' );
    //    }
    //
    //    bodyMain += this.instructions.join( '\n' );
    //
    //    return bodyMain;
    //}
}