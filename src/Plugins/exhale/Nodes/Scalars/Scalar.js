import Node from '../Node';

export default class Scalar extends Node {
    constructor ( closure, value ) {
        super( closure );

        this.value = value;
    }

    calculate () {
        return this.value;
    }

    toString () {
        return this.value ? this.value.toString() : 'null';
    }
}