import Scalar from './Scalar';

export default class Null extends Scalar {
    constructor ( closure ) {
        super( closure, null );
    }

    toString () {
        return 'null';
    }
}