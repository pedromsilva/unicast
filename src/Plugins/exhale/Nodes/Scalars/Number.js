import Scalar from './Scalar';

export default class Number extends Scalar {
    toString () {
        return this.value;
    }
}