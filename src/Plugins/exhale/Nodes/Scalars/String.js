import Scalar from './Scalar';

export default class String extends Scalar {
    toString () {
        if ( this.value.includes( "'" ) ) {
            return '"' + this.value.replace( /\\/g, '\\\\' ).replace( /"/g, '\"' ) + '"';
        }

        return "'" + this.value.replace( /\\/g, '\\\\' ).replace( /'/g, '\'' ) + "'";
    }
}