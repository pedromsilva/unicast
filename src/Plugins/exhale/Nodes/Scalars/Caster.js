import is from 'is';
import StringScalar from './String';
import NumberScalar from './Number';
import BooleanScalar from './Boolean';
import Scalar from './Scalar';

export default function Caster ( closure, value, type = null ) {
    if ( !type ) {
        if ( is.string( value ) ) type = 'string';
        else if ( is.number( value ) ) type = 'number';
        else if ( is.bool( value ) ) type = 'boolean';
        else type = null;
    }

    if ( type && isString( type ) ) {
        return new StringScalar( closure, value );
    } else if ( type && isNumber( type ) ) {
        return new NumberScalar( closure, value );
    } else if ( type && isBoolean ) {
        return new BooleanScalar( closure, value );
    } else {
        return new Scalar( closure, value );
    }
}

export async function Unwrap ( context, mode, value ) {
    while ( value && value.isExhalerNode ) {
        value = await value.call( context, mode );
    }

    return value;
}

export async function UnwrapList ( context, mode, values ) {
    return Promise.all( values.map( value => Unwrap( context, mode, value ) ) );
}

export function isString ( type ) {
    return ( is.string( type ) && type.toLowerCase() === 'string' ) || type == String || type == StringScalar;
}

export function isNumber ( type ) {
    return ( is.string( type ) && type.toLowerCase() === 'number' ) || type == Number || type == NumberScalar;
}

export function isBoolean ( type ) {
    return ( is.string( type ) && ( type.toLowerCase() === 'boolean' || type.toLowerCase() === 'bool' ) ) || type == Boolean || type == BooleanScalar;
}