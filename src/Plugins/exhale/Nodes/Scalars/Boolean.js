import Scalar from './Scalar';

export default class Boolean extends Scalar {
    toString () {
        return this.value ? 'true' : 'false';
    }
}