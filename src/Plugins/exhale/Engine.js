import Closure from './Nodes/Definitions/Closures/Native';

export default class Engine extends Closure {
    constructor ( closure ) {
        super( null );

        if ( closure ) {
            this.addInstruction( closure );
        }
    }

    toString () {
        return this.instructions[ 0 ].toString();
    }
}