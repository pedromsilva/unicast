import Context from './Context';

export default class Symbols {
    // A map containing all local definitions
    definitions : Map = new Map();

    containers : Array<Object> = [];

    namedContainers : Object = {};

    get parent () {
        return null;
    }

    get root () {
        let current = this;

        while ( current ) { current = current.parent }

        return current;
    }

    addContainer ( name, container = null, prepend = false ) {
        if ( is.bool( container ) ) {
            [ container, prepend ] = [ null, container ];
        }

        if ( !container ) {
            [ name, container ] = [ null, name ];
        }

        if ( name ) {
            this.namedContainers[ name ] = container;
        }

        this.containers[ prepend ? 'unshift' : 'push' ]( container );

        return this;
    }

    // Static container
    locateLocalStatic ( context : object, name : string, types = null ) {
        if ( !types ) {
            types = this.getTypes();
        }

        if ( !is.array( types ) ) {
            types = [ types ];
        }

        for ( let type of types ) {
            if ( this.definitions.has( type ) ) {
                let container = this.definitions.get( type );

                if ( container.has( name ) ) {
                    return [ type ];
                }
            }
        }

        return null;
    }

    fetchLocalStatic ( context, name : string, type : string ) {
        return this.definitions.get( type ).get( name );
    }

    setLocalStatic ( context, name : string, type : string ) {
        if ( !this.definitions.has( type ) ) {
            this.definitions.set( type, new Map() );
        }

        this.definitions.get( type ).set( name, value );

        return context;
    }

    staticContainer () {
        return {
            locate: this.locateLocalStatic.bind( this ),
            fetch: this.fetchLocalStatic.bind( this ),
            set: this.setLocalStatic.bind( this )
        };
    }

    // Dynamic Container
    locateLocalDynamic ( context, name : string, types = null ) {
        if ( context ) {
            return Context.locateDefinitionFromStack( context, name, types, true );
        }

        return null;
    }

    fetchLocalDynamic ( context : object, name : string, stack : object, type : string ) {
        return stack.get( 'definitions' ).get( type ).get( name );
    }

    setLocalDynamic ( context : object, name : string, type : string, value ) {
        return Context.setDefinitionToStack( context, name, type, value, true );
    }

    dynamicContainer () {
        return {
            locate: this.locateLocalDynamic.bind( this ),
            fetch: this.fetchLocalDynamic.bind( this ),
            set: this.setLocalDynamic.bind( this )
        };
    }

    getTypes ( exclude : Array<string> = [] ) : Array<string> {
        return Array.from( this.definitions.keys() ).filter( item => !exclude.includes( item ) );
    }

    locateLocal ( context, name : string, types = null ) {
        if ( context && !is.object( context ) ) {
            [ context, name, types ] = [ null, context, name ];
        }

        let location = null;

        for ( let container of this.containers ) {
            location = container.locate( context, name, types );

            if ( location ) {
                location = [ container, ...location ];

                break;
            }
        }

        return location || null;
    }

    locate ( context, name : string, types = null ) {
        if ( context && !is.object( context ) ) {
            [ context, name, types ] = [ null, context, name ];
        }

        let location = this.locateLocal( context, name, types );

        // If no match has been found, try the upper stack
        if ( !location && this.parent ) {
            if ( context ) {
                context = Context.moveStackUp( context );
            }

            return this.parent.locate( context, name, types );
        }

        return location || null;
    }

    getLocal ( context, name : string, types = null ) {
        if ( context && !is.object( context ) ) {
            [ context, name, types ] = [ null, context, name ];
        }

        let location = this.locateLocal( context, name, types );

        if ( location ) {
            return this.fetch( context, name, ...location );
        }

        return null;
    }

    fetch ( context : object, name : string, ...location ) {
        let container;

        [ container, ...location ] = location;

        return container.fetch( context, name, ...location );
    }

    get ( context, name : string, types = null ) {
        if ( context && !is.object( context ) ) {
            [ context, name, types ] = [ null, context, name ];
        }

        let location = this.locate( context, name, types );

        if ( location ) {
            return this.fetch( context, name, ...location );
        }

        return null;
    }

    setLocal ( container, context : object, name : string, type : string, value, ...args ) {
        if ( context && !is.object( context ) ) {
            [ context, name, type, value ] = [ null, context, name, type ];
        }

        if ( is.string( container ) ) {
            container = this.namedContainers[ container ];
        }

        return container.set( context, name, type, value, ...args );
    }

    set ( container, context, name : string, type : string, value, ...args ) {
        return this.setLocal( container, context, name, type, value, ...args );
    }

    listLocal () {

    }

    list ( types = null, group = false ) {
        if ( !types ) {
            types = this.getTypes();
        }


    }
}