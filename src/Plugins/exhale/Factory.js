import NativeClosureNode from './Nodes/Definitions/Closures/Native';
import JavaScriptClosureNode from './Nodes/Definitions/Closures/JavaScript';
import VariableDefinitionNode from './Nodes/Definitions/Variable';
import CommentNode from './Nodes/Comment';
import GroupNode from './Nodes/Group';

// Scopes
import CustomScopeNode from './Nodes/Scopes/Custom';

// Logic Operator Nodes
import ConditionalNode from './Nodes/Conditional'
import NegationLogicNode from './Nodes/Operators/Logic/Negation';
import ConjunctionLogicNode from './Nodes/Operators/Logic/Conjunction';
import DisjunctionLogicNode from './Nodes/Operators/Logic/Disjunction';

// Comparator Operator Nodes
import LessThanNode from './Nodes/Operators/Comparison/LessThan';
import LessThanEqualNode from './Nodes/Operators/Comparison/LessThanEqual';
import EqualNode from './Nodes/Operators/Comparison/Equal';
import DifferentNode from './Nodes/Operators/Comparison/Different';
import GreaterThanEqualNode from './Nodes/Operators/Comparison/GreaterThanEqual';
import GreaterThanNode from './Nodes/Operators/Comparison/GreaterThan';

// Arithmetic Operator Nodes
import AdditionNode from './Nodes/Operators/Arithmetic/Addition';
import SubtractionNode from './Nodes/Operators/Arithmetic/Subtraction';
import MultiplicationNode from './Nodes/Operators/Arithmetic/Multiplication';
import DivisionNode from './Nodes/Operators/Arithmetic/Division';
import PowerNode from './Nodes/Operators/Arithmetic/Power';
import ModulusNode from './Nodes/Operators/Arithmetic/Modulus';

// Execution Mode Operators
import ExecuteModeNode from './Nodes/Operators/Execute';
import EvaluateModeNode from './Nodes/Operators/Evaluate';

// Scalars
import NumberScalarNode from './Nodes/Scalars/Number';
import StringScalarNode from './Nodes/Scalars/String';
import BooleanScalarNode from './Nodes/Scalars/Boolean';
import NullScalarNode from './Nodes/Scalars/Null';

// Actions
import ActionNode from './Nodes/Actions/Action';
import VariableNode from './Nodes/Actions/Variable';

export default class Factory {
    static make ( parent, node ) {
        let instance = new this();

        return instance.make( parent, node );
    }

    makeInstructionsList ( parent, instructions ) {
        for ( let instruction of instructions ) {
            let instructionNode = this.make( parent, instruction );

            if ( instructionNode.isDefinition ) {
                parent.registerDefinition( instructionNode.type, instructionNode.name, instructionNode );
            } else {
                parent.addInstruction( instructionNode );
            }
        }
    }

    make ( parent, node ) {
        let object = this.makeNode( parent, node );

        if ( object ) {
            object.meta = node.node;
            object.properties = node.properties;

            object.properties = object.properties.map( node => this.make( parent, node ) );
        }

        return object;
    }

    makeNode ( parent, node ) {
        switch ( node.node.type ) {
            case 'definition':
                return this.makeDefinitionNode( parent, node );
            case 'comment':
                return this.makeCommentNode( parent, node );
            case 'scope':
                return this.makeScopeNode( parent, node );
            case 'condition':
                return this.makeConditionalNode( parent, node );
            case 'operator':
                return this.makeOperatorNode( parent, node );
            case 'grouped':
                return this.makeGroupedNode( parent, node );
            case 'action':
                return this.makeActionNode( parent, node );
            case 'scalar':
                return this.makeScalarNode( parent, node );
            case 'variable':
                return this.makeVariableNode( parent, node );
        }
    }

    makeDefinitionNode ( parent, node ) {
        switch ( node.node.subtype ) {
            case 'closure':
                return this.makeClosureNode( parent, node );
            case 'variable':
                return this.makeVariableDefinitionNode( parent, node );
        }
    }

    makeVariableDefinitionNode ( parent, node ) {
        let definition = new VariableDefinitionNode( parent, node.name, this.make( parent, node.value ) );

        parent.registerDefinition( 'variable', definition.name, definition );

        return definition;
    }

    makeClosureNode ( parent, node ) {
        switch ( node.language ) {
            case 'exhale':
                return this.makeNativeClosureNode( parent, node );
            case 'javascript':
                return this.makeJavaScriptClosureNode( parent, node );
        }
    }

    makeBaseClosureNode ( parent, node, closure ) {
        for ( let scope of node.scopes ) {
            closure.addScope( this.make( closure, scope ) );
        }

        return closure;
    }

    makeNativeClosureNode ( parent, node ) {
        let closure = new NativeClosureNode( parent, node.name, node.arguments );

        this.makeBaseClosureNode( parent, node, closure );

        this.makeInstructionsList( closure, node.body );

        return closure;
    }

    makeJavaScriptClosureNode ( parent, node ) {
        let closure = new JavaScriptClosureNode( parent, node.name, node.arguments, node.body );

        return this.makeBaseClosureNode( parent, node, closure );
    }

    makeCommentNode ( parent, node ) {
        return new CommentNode( parent, node.content );
    }

    makeScopeNode ( parent, node ) {
        switch ( node.node.subtype ) {
            case 'custom':
                return this.makeScopeCustomNode( parent, node );
        }
    }

    makeScopeCustomNode ( parent, node ) {
        return new CustomScopeNode( parent, node.name, node.parameters.map( node => this.make( parent, node ) ) );
    }
    //
    //makeScopeSpecifierNode ( parent, node ) {
    //    return new SpecifierScopeNode( parent, node.category, node.key, node.value );
    //}

    makeConditionalNode ( parent, node ) {
        let condition = this.make( parent, node.condition );
        let consequent = this.make( parent, node.consequent );
        let alternative = node.alternative ? this.make( parent, node.alternative ) : null;

        return new ConditionalNode( parent, condition, consequent, alternative );
    }

    makeGroupedNode ( parent, node ) {
        let group = new GroupNode( parent );

        this.makeInstructionsList( group, node.body );

        return group;
    }

    makeOperatorNode ( parent, node ) {
        switch ( node.node.subtype ) {
            case 'lessThan':
                return this.makeComparatorLessThanNode( parent, node );
            case 'lessThanEqual':
                return this.makeComparatorLessThanEqualNode( parent, node );
            case 'equal':
                return this.makeComparatorEqualNode( parent, node );
            case 'different':
                return this.makeComparatorDifferentNode( parent, node );
            case 'greaterThanEqual':
                return this.makeComparatorGreaterThanEqualNode( parent, node );
            case 'greaterThan':
                return this.makeComparatorGreaterThanNode( parent, node );
            case 'negated':
                return this.makeLogicNegationNode( parent, node );
            case 'conjunction':
                return this.makeLogicConjunctionNode( parent, node );
            case 'disjunction':
                return this.makeLogicDisjunctionNode( parent, node );
            case 'addition':
                return this.makeArithmeticAdditionNode( parent, node );
            case 'subtraction':
                return this.makeArithmeticSubtractionNode( parent, node );
            case 'division':
                return this.makeArithmeticDivisionNode( parent, node );
            case 'multiplication':
                return this.makeArithmeticMultiplicationNode( parent, node );
            case 'modulus':
                return this.makeArithmeticModulusNode( parent, node );
            case 'power':
                return this.makeArithmeticPowerNode( parent, node );
            case 'execute':
                return this.makeExecuteModeNode( parent, node );
            case 'evaluate':
                return this.makeEvaluateModeNode( parent, node );
        }
    }

    makeComparatorGenericNode ( ComparatorNode, parent, node ) {
        let clauseA = this.make( parent, node.clauseA );
        let clauseB = this.make( parent, node.clauseB );

        return new ComparatorNode( parent, clauseA, clauseB );
    }

    makeArithmeticAdditionNode ( parent, node ) {
        return this.makeComparatorGenericNode( AdditionNode, parent, node );
    }

    makeArithmeticSubtractionNode ( parent, node ) {
        return this.makeComparatorGenericNode( SubtractionNode, parent, node );
    }

    makeArithmeticDivisionNode ( parent, node ) {
        return this.makeComparatorGenericNode( DivisionNode, parent, node );
    }

    makeArithmeticMultiplicationNode ( parent, node ) {
        return this.makeComparatorGenericNode( MultiplicationNode, parent, node );
    }

    makeArithmeticModulusNode ( parent, node ) {
        return this.makeComparatorGenericNode( ModulusNode, parent, node );
    }

    makeArithmeticPowerNode ( parent, node ) {
        return this.makeComparatorGenericNode( PowerNode, parent, node );
    }

    makeComparatorLessThanNode ( parent, node ) {
        return this.makeComparatorGenericNode( LessThanNode, parent, node );
    }

    makeComparatorLessThanEqualNode ( parent, node ) {
        return this.makeComparatorGenericNode( LessThanEqualNode, parent, node );
    }

    makeComparatorEqualNode ( parent, node ) {
        return this.makeComparatorGenericNode( EqualNode, parent, node );
    }

    makeComparatorDifferentNode ( parent, node ) {
        return this.makeComparatorGenericNode( DifferentNode, parent, node );
    }

    makeComparatorGreaterThanEqualNode ( parent, node ) {
        return this.makeComparatorGenericNode( GreaterThanEqualNode, parent, node );
    }

    makeComparatorGreaterThanNode ( parent, node ) {
        return this.makeComparatorGenericNode( GreaterThanNode, parent, node );
    }

    makeLogicNegationNode ( parent, node ) {
        return new NegationLogicNode( parent, this.make( parent, node.expression ) );
    }

    makeExecuteModeNode ( parent, node ) {
        return new ExecuteModeNode( parent, this.make( parent, node.expression ) );
    }

    makeEvaluateModeNode ( parent, node ) {
        return new EvaluateModeNode( parent, this.make( parent, node.expression ) );
    }

    makeLogicConjunctionNode ( parent, node ) {
        let clauseA = this.make( parent, node.clauseA );
        let clauseB = this.make( parent, node.clauseB );

        return new ConjunctionLogicNode( parent, clauseA, clauseB );
    }

    makeLogicDisjunctionNode ( parent, node ) {
        let clauseA = this.make( parent, node.clauseA );
        let clauseB = this.make( parent, node.clauseB );

        return new DisjunctionLogicNode( parent, clauseA, clauseB );
    }

    makeActionNode ( parent, node ) {
        return new ActionNode( parent, node.name, node.parameters.map( p => {
            p.value = this.make( parent, p.value );

            return p;
        } ) );
    }

    makeScalarNode ( parent, node ) {
        switch ( node.node.subtype ) {
            case 'number':
                return this.makeScalarNumberNode( parent, node );
            case 'string':
                return this.makeScalarStringNode( parent, node );
            case 'boolean':
                return this.makeScalarBooleanNode( parent, node );
            case 'null':
                return this.makeScalarNullNode( parent, node );
        }
    }

    makeScalarNumberNode ( parent, node ) {
        return new NumberScalarNode( parent, node.value );
    }

    makeScalarBooleanNode ( parent, node ) {
        return new BooleanScalarNode( parent, node.value );
    }

    makeScalarNullNode ( parent, node ) {
        return new NullScalarNode( parent );
    }

    makeScalarStringNode ( parent, node ) {
        return new StringScalarNode( parent, node.value );
    }

    makeVariableNode ( parent, node ) {
        return new VariableNode( parent, node.name );
    }
}