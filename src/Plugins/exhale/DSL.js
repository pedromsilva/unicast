import Parser from './Parser';
import Engine from './Engine';
import Factory from './Factory';

export default function dsl ( script ) {
    return DSLWrapper.make( script );
}

export class DSLWrapper {
    makeEngine () {
        return new Engine();
    }

    makeNodes ( engine, ast ) {
        return Factory.make( engine, ast );
    }

    makeParser ( script ) {
        return new Parser( script );
    }

    parse ( script ) {
        let engine = this.makeEngine();

        let parser = this.makeParser( script );

        let ast = parser.buildSync();

        let nodes = this.makeNodes( engine, ast );

        engine.addInstruction( nodes );

        return engine;
    }

    static make ( script ) {
        let instance = new this();

        return instance.parse( script );
    }
}

export * from './Parser';
export * from './Engine';
export * from './Factory';