import fs from 'fs-promise';
import path from 'path';
import PEG from 'pegjs';

export default class Parser {
    static parserSync () {
        if ( !this.parserInstance ) {
            let grammar = fs.readFileSync( path.join( __dirname, 'Grammar.peg' ), 'utf8' );

            this.parserInstance = PEG.buildParser( grammar );
        }

        return this.parserInstance;
    }

    static async parser () {
        if ( !this.parserInstance ) {
            let grammar = await fs.readFile( path.join( __dirname, 'Grammar.peg' ), 'utf8' );

            this.parserInstance = PEG.buildParser( grammar );
        }

        return this.parserInstance;
    }

    constructor ( script ) {
        this.script = script;
    }

    buildSync () {
        let parser = this.constructor.parserSync();

        return parser.parse( this.script );
    }

    async build () {
        let parser = this.constructor.parserSync();

        return parser.parse( this.script );
    }
}