import Immutable from 'immutable';
import extend from 'extend';

export default class Context {
    static create ( custom = {} ) {
        return Immutable.Map( extend( {
            'stack': Immutable.Stack()
        }, custom ) );
    }

    static getStackUp ( context, count : number = 1 ) {
        let stack = context.get( 'stack' ).asMutable();

        while ( count > 0 && stack.size > 0 ) {
            stack = stack.pop();
        }

        return stack.asImmutable();
    }

    static getStackToParent ( context, closure = null ) {
        let stack = context.get( 'stack' ).asMutable();

        if ( !closure ) {
            stack = stack.pop();
        } else {
            while ( stack.size > 0 && stack.peek().get( 'closure' ) !== closure ) {
                stack = stack.pop();
            }
        }

        return stack.asImmutable();
    }

    static getParentStack ( context, closure = null ) {
        return this.getStackToParent( context, closure ).peek() || null;
    }

    static getCurrentStack ( context ) {
        return context.get( 'stack' ).peek();
    }

    static moveStackUp ( context, count : number = 1 ) {
        let stack = this.getStackUp( context, count );

        return context.set( 'stack' );
    }

    static moveStackToParent ( context, closure = null ) {
        let stack = this.getStackToParent( context, closure );

        return context.set( 'stack', stack );
    }

    static createStack ( context, closure, definitions : object = {} ) {
        let stack = Immutable.Map( {
            'index': context.get( 'stack' ).size + 1,
            'closure': closure,
            'definitions': Immutable.fromJS( definitions )
        } );

        return context.set( 'stack', context.get( 'stack' ).push( stack ) );
    }

    static updateStack ( context, item ) {
        let tempStack = Immutable.Stack().asMutable();

        let stack = context.get( 'stack' ).asMutable();

        for ( let slice of stack ) {
            if ( slice.index >= item.index ) {
                tempStack = tempStack.push( slice );

                stack.pop();
            }
        }

        tempStack.pop().push( item );

        while ( tempStack.size > 0 ) {
            stack.push( tempStack.peek() );

            tempStack.pop();
        }

        return context.set( 'stack', stack.asImmutable() );
    }

    static locateDefinitionFromStack ( context : object, name : string, types : Array<string>, local : boolean = false ) {
        let stack = context.get( 'stack' );

        for ( let slice of stack ) {
            for ( let type of types ) {
                if ( slice.get( 'definitions' ).has( type ) && slice.getIn( [ 'definitions', type ] ).has( name ) ) {
                    return [ slice, type ];
                }
            }

            if ( local ) {
                break;
            }
        }

        return null;
    }

    static getDefinitionFromStack ( context : object, name : string, types : Array<string>, local : boolean = false ) {
        let [ slice, type ] = this.locateDefinitionFromStack( context, name, types, local );

        if ( slice ) {
            return slice.getIn( [ 'definitions', type, name ] );
        }

        return null;
    }

    static setDefinitionToStack ( context : object, name : string, type : string, value : any, local : boolean = false ) {
        let stack = context.get( 'stack' );

        let slice = stack.peek();

        slice.definitions.setIn( [ type, name ], value );

        context.get( 'stack', stack.pop().push( slice ) );
    }
}