import moment from 'moment';

export class ReceiverStatus {
    timestamp : Date = moment();

    state : string;

    time : ReceiverStatusTime = new ReceiverStatusTime;

    media : ReceiverStatusMedia = new ReceiverStatusMedia;

    items : Array<ReceiverStatusMedia> = [];

    volume : ReceiverStatusVolume = new ReceiverStatusVolume;

    prediction : boolean = false;

    touch () {
        this.timestamp = moment();

        return this;
    }

    clone () {
        let clone = new this.constructor();

        clone.state = this.state;

        clone.time = this.time.clone();

        clone.media = this.media.clone();

        clone.items = this.items.map( media => media.clone() );

        clone.volume = this.volume.clone();

        clone.prediction = this.prediction;

        clone.timestamp = moment( this.timestamp );
    }
}

export class ReceiverStatusTime {
    current : number;
    total : number;

    clone () {
        let cloned = new this.constructor();

        cloned.current = this.current;

        cloned.total = this.total;

        return cloned;
    }
}

export class ReceiverStatusVolume {
    level : number;
    muted : boolean = false;

    clone () {
        let cloned = new this.constructor();

        cloned.level = this.level;

        cloned.muted = this.muted;

        return cloned;
    }
}

export class ReceiverStatusMedia {
    id : number;

    source : string;

    duration : number;

    tracks : Array<ReceiverStatusTrack> = [];

    metadata : object = {};

    streamType : string;

    clone () {
        let clone = new this.constructor();

        clone.source = this.source;

        clone.metadata = extend( {}, metadata );

        clone.tracks = this.tracks.map( track => track.clone() );

        clone.streamType = this.streamType;

        return clone;
    }
}

export class ReceiverStatusTrack {
    id : any;

    type : string;

    source : string;

    contentType : string;

    name : string;

    language : string;

    clone () {
        let clone = new this.constructor();

        clone.id = this.id;
        clone.type = this.type;
        clone.source = this.source;
        clone.contentType = this.contentType;
        clone.name = this.name;
        clone.language = this.language;

        return clone;
    }
}

export class ReceiverStatusTrackStyle {
    backgroundColor : string;
    foregroundColor : string;
    edgeType : string;
    edgeColor : string;
    fontScale : number;
    fontStyle : string;
    fontFamily : string;
    fontGenericFamily : string;
    windowColor : string;
    windowRoundedCornerRadius : number;
    windowType : string;

    clone () {
        let clone = new this.constructor();

        clone.backgroundColor = this.backgroundColor;
        clone.foregroundColor = this.foregroundColor;
        clone.edgeType = this.edgeType;
        clone.edgeColor = this.edgeColor;
        clone.fontScale = this.fontScale;
        clone.fontStyle = this.fontStyle;
        clone.fontFamily = this.fontFamily;
        clone.fontGenericFamily = this.fontGenericFamily;
        clone.windowColor = this.windowColor;
        clone.windowRoundedCornerRadius = this.windowRoundedCornerRadius;
        clone.windowType = this.windowType;

        return clone;
    }
}