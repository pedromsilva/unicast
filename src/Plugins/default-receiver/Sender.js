import ItemSubtitles from '/Unicast/Plugins/Server/Database/Models/ItemSubtitles';
import Time from '/Unicast/Utilities/Time';
import rangeParser from 'range-parser';
import is from 'is';

export default class Sender {
    constructor ( router ) {
        this.router = router;
        this.receiver = router.receiver;
        this.server = router.manager.server;

        this.streamEndMethods = [ 'end', 'close', 'destroy' ];

        this.urlStore = {};

        this.subtitlesFormat = null;
    }

    async stream ( stream, ctx ) {
        stream = await Promise.resolve( stream );

        let closeResult = () => {
            if ( is.fn( this.streamEndMethods ) ) {
                this.streamEndMethods( stream );
            } else {
                for ( let method of this.streamEndMethods ) {
                    if ( is.fn( stream[ method ] ) ) {
                        stream[ method ]();
                        break;
                    }
                }
            }
        };

        ctx.res.on( 'close', closeResult );
        ctx.res.on( 'end', closeResult );

        return stream;
    }


    range ( ctx ) {
        if ( 'start' in ctx.query || 'end' in ctx.params ) {
            return {
                start: ctx.query.start ? Time.parse( ctx.query.start ).toString() : null,
                end: ctx.query.end ? Time.parse( ctx.query.end ).toString() : null
            };
        }

        return {};
    }

    async video ( receiver, media, request, response, ctx ) {
        let stream = this.server.providers.video( media.get( 'source' ), media, receiver );

        let size = await stream.size;
        let mime = await stream.mime;
        let range = request.headers.range;
        let timeRange = this.range( ctx );

        response.set( 'Content-Type', mime );
        response.set( 'Access-Control-Allow-Origin', '*' );

        if ( !range || await stream.live ) {
            if ( !( await stream.live ) && !( await stream.transcodable ) ) {
                response.set( 'Content-Length', size );
            }

            response.status = 200;

            return this.stream( stream.read( null, timeRange ), ctx );
        } else {
            let part = rangeParser( size, range )[ 0 ];

            response.set( 'Content-Range', 'bytes ' + part.start + '-' + part.end + '/' + size );
            response.set( 'Accept-Ranges', 'bytes' );
            response.set( 'Content-Length', ( part.end - part.start ) + 1 );
            response.status = 206;

            return this.stream( stream.read( part, timeRange ), ctx );
        }
    }

    async subtitles ( receiver, media, request, response, ctx ) {
        let subtitles = await ItemSubtitles.where( 'item_id', media.get( 'id' ) ).where( 'id', ctx.params.id ).fetch();

        let stream = this.server.providers.subtitle( subtitles.get( 'source' ), media, receiver );

        let size = await stream.size;

        response.status = 200;
        response.set( 'Access-Control-Allow-Origin', '*' );

        if ( size ) {
            response.set( 'Content-Length', size );
        }

        response.set( 'Content-type', 'text/vtt;charset=utf-8' );

        return stream.read( this.range( ctx ), this.subtitlesFormat || 'srt' );
    }

    registerUrl ( name, layer ) {
        this.urlStore[ name ] = layer;
    }

    url ( name, params = {} ) {
        let url = this.urlStore[ name ].url( params );

        if ( url ) {
            return this.server.url( url.split( '/' ) );
        }
    }

    register () {
        this.registerUrl( 'video', this.router.get( '/video', this.video.bind( this ) ) );

        this.registerUrl( 'subtitles', this.router.get( '/subtitles/:id', this.subtitles.bind( this ) ) );
    }
}