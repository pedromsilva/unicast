import TranscodersManager from '/Unicast/Plugins/Transcoding/Manager';
import Evented from '/Unicast/Utilities/Evented';
import ReceiverStatusController from './ReceiverStatusController';
import Sender from './Sender';

export default class Receiver extends Evented {
	constructor ( server, name ) {
		super();

		this.server = server;
		this.name = name;

		this.status = new ReceiverStatusController( this );

		this.transcoders = new TranscodersManager();
	}

	get type () {
		return this.constructor.type;
	}

	createSender ( router ) {
		return new Sender( router );
	}

	get current () {
		return this._currentItem;
	}

	async setCurrent ( value ) {
		if ( this.current && this.current.status ) {
            // TODO re-activate
            //await this.current.status.stop( this.current );
		}

		this._currentItem = value;
	}

	get playlist () {
		if ( !this.current ) {
			return null;
		}

		return this.current.playlist( this );
	}

	async video ( item = null ) {
		if ( !item ) {
			let status = await this.getStatus();

			item = await this.server.media.get( status.media.metadata.itemId );
		}

		return this.server.providers.video( item.get( 'source' ), item, this );
	}

	async replay ( item, options = {} ) {
		let status = await this.getStatus();

		let maxSeconds = status.media.duration;
		let startSeconds = status.currentTime;

        if ( options.offset ) {
			startSeconds = Math.min( maxSeconds, Math.max( 0, startSeconds + options.offset ) );
		}

		return this.play( item, await this.video( item ), {
            range: { start: startSeconds }
        } );
	}

	async play ( item ) {
		await this.setCurrent( item );
	}

	async stop () {
		await this.setCurrent( null );
	}
}