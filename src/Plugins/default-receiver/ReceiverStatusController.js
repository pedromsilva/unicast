import Evented from '/Unicast/Utilities/Evented';
import Kefir from 'kefir';

export default class ReceiverStatusController extends Evented {
    emitEventObject : boolean = false;

	constructor ( receiver ) {
		super();

		this.receiver = receiver;

		this.receiver.on( 'disconnected', this.stop.bind( this ) );

		this.receiver.on( 'connected', this.start.bind( this ) );

		this.playingStream = this.createPlayingStream();
		this.pingStream = this.createPingStream( this.playingStream );
		this.statusStream = this.createStatusStream( this.pingStream );

        //this.start();
        //
		//this.nextItem();
	}

	createPlayingStream () {
		let play = this.receiver.on( 'play' ).map( () => true );
		let stop = this.receiver.on( 'stop' ).map( () => false );

		return Kefir.merge( [ play, stop ] ).skipDuplicates().toProperty( () => false );
	}

	createPingStream ( control ) {
        // Emits true when the stream is in the last seven minutes
        this.lastSevenMinutes = this.on( 'update' ).filter().map( s => s.media.duration - s.currentTime < ( 7 * 60 ) ).skipDuplicates().toProperty( () => false );
        // Emits true when the stream is in the last minute
        this.lastMinute = this.on( 'update' ).filter().map( s => s.media.duration - s.currentTime < 60 ).skipDuplicates().toProperty( () => false );

		let pingRegular = Kefir.interval( 10000 ).filterBy( this.lastSevenMinutes.map( v => !v ) );
		let pingFaster  = Kefir.interval( 5000 ).filterBy( this.lastSevenMinutes ).filterBy( this.lastMinute.map( v => !v ) );
		let pingFastest = Kefir.interval( 1000 ).filterBy( this.lastMinute );

		return Kefir.merge( [ pingRegular, pingFaster, pingFastest ] ).delay( 1000 ).filterBy( control );
	}

	createStatusStream ( ping ) {
		return ping.flatMapLatest( () => Kefir.fromPromise( this.receiver.getStatus() ) );
	}

    nextItem () {
        let justPlayed = this.receiver.on( 'playing' ).flatMapLatest( () => {
            return Kefir.concat( [
				Kefir.sequentially( 0, [ true ] ),
				this.on( 'update' ).filter().map( () => false ).take( 1 )
			] );
        } ).toProperty( () => true );

        justPlayed.log( '[justPlayed]' );

        this.on( 'update' ).skipDuplicates().delay( 10 ).filterBy( justPlayed.map( v => !v ) ).map( s => {
            if ( s ) {
                return s.currentTime + ' / ' + s.media.duration;
            }

            return null;
        } ).log( '[status]' );
    }

	start () {
		if ( !this.listener ) {
			this.listener = this.changed.bind( this );

			this.statusStream.onValue( this.listener )
		}
	}

	stop () {
		if ( this.listener ) {
			this.statusStream.offValue( this.listener );

			this.listener = null;
		}
	}

	changed ( status ) {
		this.emit( 'update', status );
	}
}