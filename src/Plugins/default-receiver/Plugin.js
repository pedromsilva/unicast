import Plugin from '/Unicast/Components/Plugin';

export default class DefaultReceiverPlugin extends Plugin {
    dependsOn : Array<String> = [ 'receivers', 'transcoding' ];
}