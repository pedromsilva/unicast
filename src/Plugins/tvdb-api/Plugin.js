import Plugin from '/Unicast/Components/Plugin';
import config from 'config';

export default class TvdbApiPlugin extends Plugin {
    dependsOn : Array<String> = [ 'server' ];

    target : Array<String> = 'server';

    async install ( server, System ) {
        let { 'default' : TvdbWrapper } = await System.import( '/Unicast/Plugins/TvdbApi/Tvdb' );

        let { 'default' : TvdbController } = await System.import( '/Unicast/Plugins/TvdbApi/Controller' );

        if ( config.has( 'tvdb.apiKey' ) ) {
            server.component( 'tvdb', new TvdbWrapper( config.get( 'tvdb.apiKey' ) ) );
        }

        server.controller( TvdbController );
    }
}