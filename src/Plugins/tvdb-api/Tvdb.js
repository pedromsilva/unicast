import TVDB from 'node-tvdb';
import promisify from 'es6-promisify';
import moment from 'moment';
import Cacheman from 'cacheman';
import CachemanFile from 'cacheman-file';

export default class Tvdb {
    constructor ( api ) {
        this.client = new TVDB( api );
        this.cache = new Cacheman( 'tvdb', {
            ttl: 7 * 24 * 60 * 60,
            engine: new CachemanFile( { tmpDir: 'storage/cache' } )
        } );
    }

    async call ( method, ...args ) {
        let key = method + '-' + args.join( ',' );
        let result;

        if ( !( result = await this.cache.get( method + '-' + args.join( ',' ) ) ) ) {
            result = await this.client[ method ]( ...args );

            await this.cache.set( key, result );
        }

        return result;
    }

    async getShow ( id, all = false ) {
        let show = await this.call( all ? 'getSeriesAllById' : 'getSeriesById', id );

        if ( show.Episodes ) {
            for ( let episode of show.Episodes ) {
                if ( episode.FirstAired ) {
                    episode.airDate = moment( episode.FirstAired, 'YYYY-MM-DD' );
                }
            }
        }

        return show;
    }

    async getRemoteShow ( id ) {
        let show = await this.call( 'getSeriesByRemoteId', id );

        for ( let episode of show.Episodes ) {
            if ( episode.FirstAired ) {
                episode.airDate = moment( episode.FirstAired, 'YYYY-MM-DD' );
            }
        }

        return show;
    }

    getEpisodes ( show ) {
        return this.call( 'getEpisodesById', show );
    }
}