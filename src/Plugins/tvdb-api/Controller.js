import Controller from '/Unicast/Plugins/Server/Controllers/Controller';
import sortBy from 'sort-by';
import moment from 'moment';

export default class TvdbController extends Controller {
    static routes ( router, make ) {
        let subtitles = make();

        subtitles.get( '/show', this.action( 'show' ) );
        subtitles.get( '/last-episodes', this.action( 'lastEpisodes' ) );

        router.use( '/tvdb', subtitles.routes() );
    }

    normalize () {
        let days = [ 0, 5, 6 ];
        let start = moment();

        days = days.map( d => d - start.day() ).map( d => d < 0 ? d + 7 : d ).sort();

        start.add( days[ 0 ] );
    }

    async lastEpisodes () {
        let shows = ( this.request.query.ids || '' ).split( ',' ).map( id => +id );

        let result = {};

        for ( let show of shows ) {
            let info = await this.server.get( 'tvdb' ).getShow( show, true );

            result[ show ] = {
                id: show,
                seasons: []
            };

            let resultShow = result[ show ];
            let seasons = resultShow.seasons;
            let seasonsHash = {};

            for ( let episode of info.Episodes ) {
                if ( !( episode.SeasonNumber in seasonsHash ) ) {
                    seasonsHash[ episode.SeasonNumber ] = seasons.length;

                    seasons.push( {
                        number: episode.SeasonNumber,
                        episodes: 0,
                        firstDate: null,
                        lastDate: null
                    } );
                }

                let season = seasons[ seasonsHash[ episode.SeasonNumber ] ];

                season.episodes += 1;
                if ( episode.airDate && ( !season.firstDate || season.firstDate.isAfter( episode.airDate ) ) ) {
                    season.firstDate = moment( episode.airDate );
                }

                if ( episode.airDate && ( !season.lastDate || season.lastDate.isBefore( episode.airDate ) ) ) {
                    season.lastDate = moment( episode.airDate );
                }
            }

            seasons.sort( sortBy( 'number' ) );
        }

        return result;
    }
}