import Engine from './Engine';
import { DSLWrapper as DSL } from '/Unicast/Plugins/Exhale/DSL';

export default function dsl ( script ) {
    return DSLWrapper.make( script );
}

export class DSLWrapper extends DSL {
    makeEngine () {
        return new Engine();
    }
}

export * from '/Unicast/Plugins/Exhale/Parser';
export * from './Engine';
export * from '/Unicast/Plugins/Exhale/Factory';