import Plugin from '/Unicast/Components/Plugin';

export default class ExhalePlugin extends Plugin {
    dependsOn : Array<String> = [ 'exhale', 'transcoding' ];
}