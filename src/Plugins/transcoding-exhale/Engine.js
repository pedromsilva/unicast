import DefaultEngine from '/Unicast/Plugins/Exhale/Engine';
import StreamSpecifier from '/Unicast/Plugins/Transcoding/StreamSpecifiers';
import ActionCodec from './ActionCodec';
import Immutable from 'immutable';

// Codecs
import { AACCodec, AC3Codec, DTSCodec, MKVFormat, CopyCodec, H264Codec, BitrateProperty } from '/Unicast/Plugins/Transcoding/Codecs/Index';

export default class Engine extends DefaultEngine {
    constructor ( closure ) {
        super( closure );

        this.registerDefaultDefinitions();
        this.registerDefaultCustomScopes();
    }

    registerDefaultDefinitions () {
        this.registerCodec( 'aac', new AACCodec() );
        this.registerCodec( 'ac3', new AC3Codec() );
        this.registerCodec( 'dts', new DTSCodec() );
        this.registerCodec( 'mkv', new MKVFormat() );
        this.registerCodec( 'copy', new CopyCodec() );
        this.registerCodec( 'h264', new H264Codec() );
        this.registerCodec( 'bitrate', new BitrateProperty() );
    }

    registerCodec ( name, codec ) {
        return this.registerDefinition( 'closure', name, new ActionCodec( this, codec ) );
    }

    registerDefaultCustomScopes () {
        this.registerScopeDefinition( 'default', context => {
            let has = {
                'audio': false,
                'video': false
            };

            return context.setIn( [ 'metadata', 'streams' ], context.getIn( [ 'metadata', 'streams' ] ).filter( stream => {
                if ( !( stream.get( 'codec_type' ) in has ) ) {
                    return false;
                }

                if ( !has[ stream.get( 'codec_type' ) ] ) {
                    has[ stream.get( 'codec_type' ) ] = true;
                    return true;
                }

                return false;
            } ) );
        } );

        let streamSpecifiers = ( name, context, args = [] ) => {
            let specifiers = new StreamSpecifier( name, ...args );

            return context.set( 'metadata', specifiers.filterImmutable( context.get( 'metadata' ) ) );
        };
        this.registerScopeDefinition( 'a', streamSpecifiers.bind( null, 'a' ) );
        this.registerScopeDefinition( 'v', streamSpecifiers.bind( null, 'v' ) );
        this.registerScopeDefinition( 'V', streamSpecifiers.bind( null, 'V' ) );
        this.registerScopeDefinition( 'm', streamSpecifiers.bind( null, 'm' ) );
        this.registerScopeDefinition( 'i', streamSpecifiers.bind( null, 'i' ) );
        this.registerScopeDefinition( 's', streamSpecifiers.bind( null, 's' ) );
    }

    evaluate ( metadata, media = null ) {
        return super.evaluate( Immutable.Map( {
            metadata: metadata
        } ), [], {
            media: media
        } );
    }

    execute ( transcoder, metadata, media = null ) {
        return super.execute( Immutable.Map( {
            transcoder: transcoder,
            metadata: metadata
        } ), [], {
            media: media
        } );
    }
}