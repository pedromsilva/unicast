import Node from '/Unicast/Plugins/Exhale/Nodes/Node';
import { UnwrapList } from '/Unicast/Plugins/Exhale/Nodes/Scalars/Caster';

export default class ActionCodec extends Node {
    constructor ( closure, codec ) {
        super( closure );
        this.action = codec;
    }

    get isConditional () {
        return false;
    }

    getParameters () {
        return [];
    }

    async evaluate ( context, args = [] ) {
        args = await UnwrapList( context, 'evaluate', args );

        return this.action.matches( context.get( 'metadata' ), this.closure.getVariable( 'media', context ), ...args );
    }

    async execute ( context, args = [] ) {
        args = await UnwrapList( context, 'evaluate', args );

        return this.action.convert( context.get( 'transcoder' ), context.get( 'metadata' ), this.closure.getVariable( 'media', context ), ...args );
    }
}