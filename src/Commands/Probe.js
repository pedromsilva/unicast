import FFMpeg from '/Unicast/Utilities/FFMpeg';
import Command from '/Unicast/Command';
import prettyjson from 'prettyjson';
import fs from 'fs-promise';
import path from 'path';

export default class Probe extends Command {
    constructor () {
        super();

        this.name = 'probe';
        this.description = 'Probes a media file with ffmpeg and prints the output';
        this.args = '<source>';
        this.options = [
            [ '-s, --streams <specifier>', 'Filter which streams to show' ]
        ];
    }

    async execute ( source, options ) {
        if ( !path.isAbsolute( source ) ) {
            return path.resolve( path.join( process.cwd(), source ) );
        }

        let config = {};

        if ( options.streams ) {
            config.selectStreams = options.streams;
        }

        let metadata = await FFMpeg.probe( source, config );

        console.log( prettyjson.render( metadata, {} ) );
    }
}