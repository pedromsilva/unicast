import FFMpeg from '/Unicast/Utilities/FFMpeg';
import Transcoder from '/Unicast/Plugins/Transcoding/Transcoder';
import MediaServer from '/Unicast/Plugins/Server/MediaServer';
import Command from '/Unicast/Command';
import fs from 'fs-promise';
import path from 'path';

export default class Transcode extends Command {
	constructor () {
		super();

		this.name = 'transcode';
		this.description = 'Transcodes a file for a given receiver';
		this.args = '<source> <destination> <receiver>'
	}

	async execute ( source, destination, receiverName, options ) {
		[ source, destination ] = [ source, destination ].map( file => {
			if ( !path.isAbsolute( file ) ) {
				return path.resolve( path.join( process.cwd(), file ) );
			}

			return file;
		} );

		let server = new MediaServer();

		await server.initialize();

		let receiver = await server.receivers.get( receiverName );

		let metadata = await FFMpeg.probe( source );

		let transcoder = receiver.transcoders;

		transcoder.spawn( source, metadata ).pipe( fs.createWriteStream( destination ) );
	}
}