import path from 'path';
import fs from 'fs';
import is from 'is';

export default class Import {
    inherit : boolean = true;
    prefix : Array<String> = [];
    importers : Set = new Set;
    replacement : String;
    original;

    constructor ( prefix, replacement ) {
        this.prefix = is.array( prefix ) ? prefix : this.segmentize( prefix );
        this.replacement = replacement;
    }

    use ( importer ) {
        if ( !this.importers.has( importer ) ) {
            this.importers.add( importer );
        }

        return this;
    }

    hijack () {
        if ( this.original ) {
            return;
        }

        this.original = module.constructor._resolveFilename;

        let importer = this;
        module.constructor._resolveFilename = function ( ...args ) {
            return importer.resolve( importer, ...args );
        };
    }

    segmentize ( path ) {
        return path.split( '/' );
    }

    matches ( segments ) {
        for ( let [ index, segment ] of segments.entries() ) {
            if ( index < this.prefix.length && this.prefix[ index ] != segment ) {
                return false;
            }
        }

        return segments.length >= this.prefix.length;
    }

    normalizeChildren ( segments ) {
        let name;

        for ( let importer of this.importers ) {
            if ( importer.matches( segments ) ) {
                name = importer.normalize( segments );

                if ( importer.inherit ) {
                    name = path.join( this.replacement, name );
                }

                break;
            }
        }

        return name;
    }

    normalizeDefault ( segments ) {
        return path.join( this.replacement, ...segments );
    }

    normalize ( segments ) {
        let rest = segments.slice( this.prefix.length );

        let name = this.normalizeChildren( rest );

        if ( !name ) {
            name = this.normalizeDefault( rest );
        }

        return name;
    }

    resolve ( module, name, self ) {
        let segments = is.array( name ) ? name : this.segmentize( name );

        if ( this.matches( segments ) ) {
            name = this.normalize( segments );
        }

        return module.original( name, self );
    }

    restore () {
        if ( !this.original ) {
            return;
        }

        module.constructor._resolveFilename = this.original;

        this.original = null;
    }

    static hijack ( name, dir ) {
        let instance = new this( name, dir );

        instance.hijack();

        return instance;
    }
}