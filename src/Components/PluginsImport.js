import Import from './Import';
import Case from 'case';
import path from 'path';
import is from 'is';

export default class PluginsImport extends Import {
    inherit : boolean = false;

    constructor ( prefix, pluggable ) {
        super( prefix );

        this.pluggable = pluggable;
    }

    hasPlugin ( name ) {
        return this.pluggable.plugins.has( Case.kebab( name ) )
    }

    getPlugin ( name ) {
        return this.pluggable.plugins.get( Case.kebab( name ) )
    }

    matches ( segments ) {
        return super.matches( segments ) && segments.length > this.prefix.length && this.hasPlugin( segments[ this.prefix.length ] );
    }

    normalizeDefault ( segments ) {
        let name;
        let plugin = this.getPlugin( segments[ 0 ] );

        if ( !plugin ) {
            throw new Error( `Can not find the plugin "${ Case.kebab( segments[ 0 ] ) }" in the registry for the request ${ segments.join( '/' ) }` );
        }

        if ( segments.length == 1 ) {
            name = plugin.path;
        } else {
            name = path.join( plugin.folder, ...segments.slice( 1 ) );
        }

        return name;
    }
}