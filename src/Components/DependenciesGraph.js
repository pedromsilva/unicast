import Graph from '../Utilities/Graph';

export default class DependenciesGraph extends Graph {
    constructor ( app ) {
        super();

        this.app = app;
    }

    addChild ( node, child ) {
        super.addChild( node, child );

        if ( this.hasNode( child ) && this.getNode( child ).disabled ) {
            this.disable( node, true );
        }

        return this;
    }

    * dependencies ( dependent ) {
        let node = this.getNode( dependent );

        yield * node.children;
    }

    * dependents ( dependency ) {
        for ( let node of this ) {
            if ( node.children.includes( dependency ) ) {
                yield node;
            }
        }
    }

    disable ( name, dependents = false ) {
        let node = this.getNode( name );

        node.disabled = true;

        if ( dependents ) {
            for ( let dependent of this.dependents( name ) ) {
                this.disable( dependent.name, true );
            }
        }

        return this;
    }
}