import Graph from '../Utilities/Graph';
import is from 'is';

export default class Loader {
    async load ( pluggable, plugins ) {
        let graph = new Graph();

        for ( let plugin of plugins ) {
            graph.addNode( plugin.name, plugin );

            if ( is.array( plugin.dependsOn ) ) {
                graph.addChildren( plugin.name, plugin.dependsOn );
            }
        }

        let ordered = graph.topologicalSort().map( plugin => plugin.meta );

        for ( let plugin of ordered ) {
            await pluggable.extend( plugin );
        }
    }
}