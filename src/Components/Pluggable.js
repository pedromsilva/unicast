import PluginsImport from './PluginsImport';
import Plugin from './Plugin';
import Component from './Component';
import Loader from './Loader';
import DependenciesGraph from './DependenciesGraph';
import os from 'os';
import is from 'is';

export default class Pluggable {
    components : Map = new Map();
    plugins : Map = new Map();
    pluginLoaders : Array<Loader> = [];
    importer : PluginsImport = new PluginsImport( 'Plugins', this );
    parentImporter : PluginsImport = null;
    useParentImporter : boolean = true;
    loaded : boolean = false;

    constructor ( importer = null ) {
        this.parentImporter = importer;
    }

    component ( name, component ) {
        this.components.set( name, component );

        this[ name ] = component;

        return this;
    }

    get ( name ) {
        return this.components.get( name );
    }

    async extend ( plugin ) {
        this.plugins.set( plugin.name, plugin );

        if ( plugin.alias ) {
            let aliases = is.string( plugin.alias ) ? [ plugin.alias ] : plugin.alias;
            for ( let alias of aliases ) {
                this.plugins.set( alias, plugin );
            }
        }

        await plugin.install( this, new System() );

        return this;
    }

    loader ( loader ) {
        this.pluginLoaders.push( loader );

        return this;
    }

    async list () {
        let plugins = [];

        for ( let loader of this.pluginLoaders ) {
            plugins = plugins.concat( await loader.load( this ) );
        }

        return plugins;
    }

    requiresPlugin ( plugin ) {
        if ( this.requires ) {
            let required = is.string( this.requires ) ? [ this.requires ] : this.requires;

            if ( required.length && ( plugin.name && required.includes( plugin.name ) ) ) {
                return true;
            }
        }

        return false;
    }

    valid ( plugin ) {
        let required = this.requiresPlugin( plugin );

        if ( !required && plugin.target ) {
            let targets = is.string( plugin.target ) ? [ plugin.target ] : plugin.target;

            if ( targets.length && ( !this.name || !targets.includes( this.name ) ) ) {
                return false;
            }
        }

        if ( plugin.platform ) {
            let platforms = is.string( plugin.platform ) ? [ plugin.platform ] : plugin.platform;

            if ( platforms.length && !platforms.includes( os.platform() ) ) {
                return false;
            }
        }

        return true;
    }

    graph ( plugins ) {
        let graph = new DependenciesGraph();

        for ( let plugin of plugins ) {
            graph.addNode( plugin.name, plugin );

            if ( is.array( plugin.dependsOn ) ) {
                graph.addChildren( plugin.name, plugin.dependsOn || [] );
            }

            if ( !this.valid( plugin ) ) {
                graph.disable( plugin.name, true );
            }
        }

        let missing = graph.missingChildren( is.string( this.requires ) ? [ this.requires ] : this.requires );

        if ( missing.length ) {
            let name = missing[ 0 ].node;
            let deps = missing[ 0 ].missing.join( ', ' );

            throw new Error( `Missing dependencies for module ${ name }: ${ deps }` )
        }

        return graph;
    }

    async load () {
        if ( this.parentImporter && this.useParentImporter ) {
            this.parentImporter.use( this.importer )
        }

        let plugins = await this.list();

        let graph = this.graph( plugins );

        let ordered = graph.topologicalSort().filter( plugin => !plugin.disabled ).map( plugin => plugin.meta );

        for ( let plugin of ordered ) {
            await this.extend( plugin );
        }

        this.loaded = true;
    }
}

export class System {
    async [ 'import' ] ( path ) {
        return require( path )
    }
}