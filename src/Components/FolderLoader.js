import Loader from './Loader';
import fs from 'fs-promise';
import path from 'path';

export default class FolderLoader extends Loader {
    folder : String;

    constructor ( folder ) {
        super();

        this.folder = path.resolve( folder );
    }

    path ( ...files ) {
        return path.join( this.folder, ...files );
    }

    async list () {
        let folders = await fs.readdir( this.folder );

        return folders.filter( file => {
            return fs.statSync( this.path( file ) ).isDirectory();
        } );
    }

    async fetch ( folder ) {
        return require( path.resolve( this.path( folder, 'Plugin.js' ) ) ).default;
    }

    async load ( pluggable ) {
        let folders = await this.list();

        let plugins = [];
        for ( let folder of folders ) {
            let Plugin = await this.fetch( folder );

            let plugin = new Plugin( folder );
            plugin.path = this.path( folder, 'Plugin.js' );
            plugin.folder = this.path( folder );

            plugins.push( plugin );
        }

        return plugins;
    }
}