import Segments from './Segments';

export default class SegmentedStream {
	constructor ( start, end ) {
		this.source = source;
		this.storage = storage;

		this.segments = new Segments( start, end );

		this.incoming = null;
		this.outgoing = null;
	}

	endSegment () {
		this.incoming.end();

		this.outgoing.finish();
	}

	saveSegment ( segment ) {
		let options = { start: segment.start, end: segment.end };

		this.current = segment.forkEmpty();

		this.segments.add( this.current );

		this.incoming = this.read( options );

		this.incoming.on( 'data', s => this.segments.expand( this.current, s.length ) );

		this.incoming.on( 'close', this.saveNextSegment.bind( this ) );

		this.outgoing = this.write( options );

		this.incoming.pipe( this.outgoing );
	}

	saveNextSegment () {
		let gaps = this.segments.gapsBetween( this.current.start, null, true );

		if ( gaps.length > 0 ) {
			this.current = gaps[ 0 ];

			this.saveSegment( close );
		}
	}

	save ( options = {} ) {
		options.start = options.start || this.segments.start;
		options.end = options.end || this.segments.end;

		let gaps = this.segments.gapsBetween( options.start, null, true );
		if ( !this.current || ( gaps.length > 0 && gaps[ 0 ].start < this.current.start ) ) {
			if ( this.current ) {
				this.endSegment();
			}

			this.current = gaps[ 0 ];

			this.saveSegment( close );
		}
	}
}