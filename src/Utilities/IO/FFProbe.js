import CommandWrapper from '../CommandWrapper';
import changeCase from 'change-case';
import config from 'config';
import extend from 'extend';
import path from 'path';
import is from 'is';

export default class FFProbe extends CommandWrapper {
    constructor ( file, options ) {
        super();

        this.file = file;

        if ( config.has( 'ffmpeg.path' ) ) {
            this.commandPath = path.join( config.get( 'ffmpeg.path' ), 'ffprobe' );
        } else {
            this.commandPath = 'ffprobe';
        }

        if ( is.string( file ) ) {
            this.set( 'i', file );
        } else {
            this.set( 'i', 'pipe:0' );
        }

        options = extend( {
            showStreams: true,
            showFormat: true,
            logLevel: 'warning',
            format: 'json'
        }, options );

        this.addOptionNameTransformer( name => {
            return changeCase.snake( name );
        } );

        this.setOptionMeta( [ 'show_format', 'show_streams' ], { toggle: true } );
        this.setOptionMeta( 'format', { rename: 'of' } );
        this.setOptionMeta( 'log_level', { rename: 'loglevel' } );

        this.setManyOptions( options );
    }

    transformResult ( result ) {
        if ( this.has( 'of', option => option.value === 'json' ) ) {
            result = JSON.parse( result );

            let types = {};

            result.streams = result.streams.map( stream => {
                let type = stream.codec_type;

                if ( !( type in types ) ) {
                    types[ type ] = 0;
                }

                stream.typeIndex = types[ type ]++;

                return stream;
            } );

            return result;
        }

        return result;
    }

    run ( ...args ) {
        return super.run( is.string( this.file ) ? null : this.file, ...args );
    }
}