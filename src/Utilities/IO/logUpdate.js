import ansiEscapes from 'ansi-escapes';
import cliCursor from 'cli-cursor';

function main ( stream ) {
    var prevLineCount = 0;

    var render = function () {
        cliCursor.hide();
        var out = [].join.call( arguments, ' ' ) + '\n';
        stream.write( out );
        render.expand( out.split( '\n' ).length );
    };

    render.expand = function ( size = 1 ) {
        prevLineCount += size;
    };

    render.contract = function ( size = 1 ) {
        prevLineCount -= Math.max( size, 0 );
    };

    render.clear = function () {
        stream.write( ansiEscapes.eraseLines( prevLineCount ) );
        prevLineCount = 0;
    };

    render.done = function () {
        prevLineCount = 0;
        cliCursor.show();
    };

    return render;
}

export default main( process.stdout );

export var stderr = main( process.stderr );

export var create = main;
