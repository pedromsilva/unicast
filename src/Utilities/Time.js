import is from 'is';

export default class Time {
    constructor ( hours = 0, minutes = 0, seconds = 0 ) {
        this.hours = hours;
        this.minutes = minutes;
        this.seconds = seconds;
    }

    toSeconds () {
        return ( this.hours * 60 * 60 ) + this.minutes * 60 + this.seconds;
    }

    toString () {
        return this.hours + ':' + this.minutes + ':' + this.seconds;
    }

    copy () {
        return new this.constructor( this.hours, this.minutes, this.seconds );
    }

    static seconds ( value ) {
        let seconds = Math.floor( value % 60 );
        let minutes = Math.floor( value / 60 ) % ( 60 );
        let hours = Math.floor( value / ( 60 * 60 ) );

        return new this( hours, minutes, seconds );
    }

    static parse ( input ) {
        if ( input instanceof this ) {
            return input;
        }

        if ( is.string( input ) && input.indexOf( ':' ) !== -1 ) {
            let parts = input.split( ':' );

            return new this( parts[ 0 ] || 0, parts[ 1 ] || 0, parts[ 2 ] || 0 );
        }

        return this.seconds( +input );
    }
}
