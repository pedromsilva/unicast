import FFProbe from '../Utilities/IO/FFProbe';
import ffmpeg from 'fluent-ffmpeg';

export default class FFMpeg {
    static probe ( track, options ) {
		let probe = new FFProbe( track, options );

		return probe.promise();
	}

	static open ( ...args ) {
		return ffmpeg( ...args );
	}
}