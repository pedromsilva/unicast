import { EventEmitter } from 'events';
import { spawn } from 'child_process';
import extend from 'extend';
import path from 'path';
import is from 'is';

export default class CommandWrapper extends EventEmitter {
    constructor () {
        super();

        this.options = [];
        this.optionsMeta = {};
        this.optionsNameTransformers = [];
    }

    makeCommandOptionMeta ( ...args ) {
        return new CommandOptionMeta( ...args );
    }

    getOptionMeta ( name, force = false ) {
        if ( !( name in this.optionsMeta ) ) {
            if ( !force ) {
                return null;
            }
            this.setOptionMeta( name, {} );
        }

        return this.optionsMeta[ name ];
    }

    setOptionMeta ( name, meta = {}, overwrite = false ) {
        if ( is.array( name ) ) {
            for ( let n of name ) {
                this.setOptionMeta( n, meta, overwrite );
            }

            return this;
        }

        if ( overwrite ) {
            delete this.optionsMeta[ name.name || name ];
        }

        let currentMeta = this.getOptionMeta( name.name || name );

        let parent = meta.parent || meta.rename || null;

        if ( is.string( parent ) ) {
            parent = this.getOptionMeta( parent, true );
        }

        if ( !currentMeta ) {
            if ( is.string( name ) ) {
                name = this.makeCommandOptionMeta( name, meta, parent );
            }

            this.optionsMeta[ name.name ] = name;
        } else {
            currentMeta.extend( meta, parent );
        }

        return this;
    }

    addOptionNameTransformer ( transformer ) {
        this.optionsNameTransformers.push( transformer );

        return this;
    }

    getOptionName ( name ) {
        for ( let transformer of this.optionsNameTransformers ) {
            name = transformer( name );
        }

        return name;
    }

    hasOption ( option, filter ) {
        return this.getOption( option, filter, true ).length > 0;
    }

    getOption ( option, filter, forceArray = false ) {
        option = this.getOptionName( option );

        let options = this.options.filter( ( each, i )=> {
            if ( each.name !== option ) {
                return false;
            }

            if ( is.fn( filter ) ) {
                return filter( each, i );
            }

            return true;
        } );

        let meta = this.getOptionMeta( option, true );

        if ( meta.is( 'multiple' ) && !forceArray ) {
            return options[ 0 ];
        }

        return options;
    }

    setOption ( option, ...values ) {
        option = this.getOptionName( option );

        let meta = this.getOptionMeta( option, true );

        let value = meta.getValue( ...values );

        let name = meta.getName( option );

        if ( !meta.is( 'multiple' ) || meta.is( 'toggle' ) ) {
            this.options = this.options.filter( s => s.name !== name );
        }

        if ( !meta.is( 'toggle' ) || value !== false ) {
            let command = new CommandOption( name, meta.is( 'toggle' ) ? null : value, option );

            this.options.splice( meta.getIndex( this.options ), 0, command );
        }

        return this;
    }

    setManyOptions ( options ) {
        for ( let key of Object.keys( options ) ) {
            let value = options[ key ];

            if ( !is.array( value ) ) {
                value = [ value ];
            }

            this.setOption( key, ...value );
        }
    }

    has ( option, filter ) {
        return this.hasOption( option, filter );
    }

    get ( option, filter ) {
        return this.getOption( option, filter );
    }

    set ( option, ...values ) {
        return this.setOption( option, ...values );
    }

    compileOptions () {
        return [].concat( ...( this.options.map( option => option.toArray() ) ) );
    }

    compileOptionsToString () {
        return this.compileOptions().join( ' ' );
    }

    transformResult ( result ) {
        return result;
    }

    process ( ...args ) {
        let options = this.compileOptions( ...args );

        let node = spawn( path.basename( this.commandPath ), options, {
            cwd: path.dirname( this.commandPath )
        } );

        if ( this.encoding ) {
            node.stdin.setEncoding( this.encoding );
            node.stdout.setEncoding( this.encoding );
        }

        return node;
    }

    run ( input, ...args ) {
        let node = this.process( ...args );

        if ( is.string( input ) ) {
            input = new Buffer( input, 'utf8' );
        }

        if ( input ) {
            if ( Buffer.isBuffer( input ) ) {
                node.stdin.write( input );
                node.stdin.end();
            } else {
                input.pipe( node.stdin );
                input.resume();
            }
        }

        return node;
    }

    stream ( input, ...args ) {
        return this.run( input, ...args ).stdout;
    }

    promise ( input, ...args ) {
        return new Promise( ( resolve, reject ) => {
            try {
                let node = this.run( input, ...args );

                let exitCode;
                let result = '';
                let resultErr = '';

                node.stdout.on( 'data', data => result += data.toString( this.encoding ) );
                node.stderr.on( 'data', data => resultErr += data );
                node.stdout.on( 'end', () => {
                    if ( exitCode || !result ) {
                        return reject( resultErr );
                    }

                    resolve( this.transformResult( result ) );
                } );

                //node.stdout.on( 'end', () => resolve( result ) );

                node.on( 'exit', code => exitCode = code );
                node.on( 'error', err => reject( err ) );
            } catch ( error ) {
                reject( error );
            }
        } );
    }
}

export class CommandOptionMeta {
    constructor ( name, options = {}, parent = null ) {
        this.name = name;
        this.extend( options, parent );
    }

    getValue ( ...values ) {
        if ( is.fn( this.fn ) ) {
            return this.fn( ...values );
        }

        if ( this.parent ) {
            return this.parent.getValue( ...values );
        }

        return values[ 0 ];
    }

    is ( attribute, defaultValue = false ) {
        if ( attribute in this ) {
            return !!this[ attribute ];
        }

        if ( this.parent ) {
            this.parent.is( attribute );
        }

        return defaultValue;
    }

    getName ( option ) {
        if ( this.rename ) {
            return this.rename;
        }

        if ( this.parent ) {
            return this.parent.getName( option );
        }

        return option;
    }

    getIndex ( options ) {
        if ( is.fn( this.index ) ) {
            return this.index( options );
        }

        if ( this.parent ) {
            return this.parent.getIndex( options );
        }

        if ( this.prepend ) {
            return 0;
        }

        return options.length;
    }

    extend ( meta, parent ) {
        extend( this, meta );
        this.parent = parent;
    }
}

export class CommandOption {
    constructor ( name, value = null ) {
        this.name = name;
        this.value = value;
    }

    toArray () {
        let array = [ '-' + this.name ];

        if ( this.value !== null ) {
            array.push( this.value );
        }

        return array;
    }

    toString () {
        return this.toArray().join( ' ' );
    }
}