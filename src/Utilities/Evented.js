import Emitter from 'emmett';
import Kefir from 'Kefir';

export default class Evented extends Emitter {
    constructor () {
        super();

        this.emitEventObject = true;
    }

    on ( events, callback = null ) {
        if ( !callback ) {
            return Kefir.fromEvents( this, events );
        }

        return super.on( events, ( event ) => {
            if ( this.emitEventObject ) {
                callback( event, ...event.data );
            } else {
                callback( ...event.data );
            }
        } );
    }

    once ( events, callback = null ) {
        if ( !callback ) {
            return Kefir.fromEvents( this, events ).take( 1 );
        }

        return super.once( events, ( event ) => {
            callback( event, ...event.data );
        } );
    }

    emit ( event, ...args ) {
        return super.emit( event, args );
    }
}