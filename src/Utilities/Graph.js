export default class Graph {
    nodes : Object = {};

    missingNodes ( nodes : Array<string> ) : Array<string> {
        let missing = [];

        for ( let node of nodes ) {
            if ( !this.hasNode( node ) ) {
                missing.push( node );
            }
        }

        return missing;
    }

    missingChildren ( mandatory : Array<string> = [] ) : Array<Object> {
        let missing : Array< { node: string, missing: Array<string> } > = [];
        let missingNodes : Array<string>;

        let plugins = [ [ null, mandatory ] ].concat( Object.keys( this.nodes ).map( node => [ node, this.nodes[ node ].children ] ) );

        for ( let [ node, dependencies ] of plugins ) {
            missingNodes = this.missingNodes( dependencies );

            if ( missingNodes.length ) {
                missing.push( { node: node, missing: missingNodes } );
            }
        }

        return missing;
    }

    addNode ( name, meta = null ) {
        this.nodes[ name ] = {
            name: name,
            children: [],
            meta: meta
        };

        return this;
    }

    hasNode ( name ) {
        return name in this.nodes;
    }

    getNode ( name ) {
        return this.nodes[ name ];
    }

    addChild ( node, child ) {
        this.nodes[ node ].children.push( child );

        return this;
    }

    addChildren ( node, children ) {
        children.forEach( child => {
            this.addChild( node, child );
        } );

        return this;
    }

    topologicalSortUtil ( parent, visited, stack ) {
        visited[ parent ] = true;

        this.nodes[ parent ].children.forEach( node => {
            if ( !visited[ node ] && this.hasNode( node ) ) {
                this.topologicalSortUtil( node, visited, stack );
            }
        } );

        stack.push( this.getNode( parent ) );
    }

    topologicalSort ( full = true ) {
        var stack = [];

        var visited = {};
        Object.keys( this.nodes ).forEach( node => {
            visited[ node ] = false;
        } );

        Object.keys( this.nodes ).forEach( node => {
            if ( !visited[ node ] ) {
                this.topologicalSortUtil( node, visited, stack );
            }
        } );

        if ( !full ) {
            return stack.map( item => {
                return item.name;
            } );
        }

        return stack;
    }

    * [ Symbol.iterator ] () {
        for ( let key of Object.keys( this.nodes ) ) {
            yield this.nodes[ key ];
        }
    }
}
