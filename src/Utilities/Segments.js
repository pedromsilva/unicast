export default class Segments {
	constructor ( start = null, end = null ) {
		this.start = start;
		this.end = end;

		this.list = [];
	}

	get gaps () {
		let gaps = [];

		for ( let segment of this.list ) {
			if ( gaps.length === 0 && segment.start < this.start ) {
				this.gaps.push( new Segment( this.start, segment.start - 1 ) );
			} else if ( gaps[ gaps.length - 1 ].end + 1 < segment.start ) {
				gaps.push( new Segment( gaps[ gaps.length - 1 ].end + 1, segment.start - 1 ) );
			}
		}

		if ( gaps.length === 0 ) {
			gaps.push( new Segment( this.start, this.end ) );
		} else if ( gaps[ gaps.length - 1 ].end < this.end ) {
			gaps.push( new Segment( gaps[ gaps.length - 1 ].end + 1, this.end ) );
		}

		return gaps;
	}

	gapsBetween ( start = null, end = null, cut = false ) {
		let gaps = this.gaps.filter( s => s.end >= start && s.start <= end );

		if ( cut ) {
			if ( gaps.length > 0 && gaps[ 0 ].start < start ) {
				gaps[ 0 ].start = start;
			}

			if ( gaps.length > 0 && gaps[ gaps.length - 1 ].end > end ) {
				gaps[ gaps.length - 1 ].end = end;
			}
		}

		return gaps;
	}

	add ( segment ) {
		let pos = this.position( segment );

		this.list.splice( pos, 0, segment );
	}

	position ( segment ) {
		let pos = -1;

		for ( let [ index, each ] of this.list.entries() ) {
			if ( each.start >= segment.start ) {
				pos = index;

				break;
			}
		}

		if ( pos < 0 ) {
			pos = this.list.length - 1;
		}

		return pos;
	}

	getPrevious ( segment ) {
		let pos = this.position( segment );

		if ( pos <= 0 ) {
			return null;
		}

		return this.list[ pos - 1 ];
	}

	getNext ( segment ) {
		let pos = this.position( segment );

		if ( pos + 1 >= this.list.length ) {
			return null;
		}

		return this.list[ pos + 1 ];
	}

	expand ( segment, amount ) {
		let next = this.getNext( segment );

		if ( segment.end + amount >= next.start ) {
			this.remove( next );

			amount = next.end - segment.end;
		}

		segment.end += amount;
	}
}

export class Segment {
	constructor ( start = null, end = null ) {
		this.start = start;
		this.end = end;
	}

	fork () {
		return new Segment( this.start, this.end );
	}

	forkEmpty () {
		let segment = this.fork();

		segment.end = segment.start;

		return segment;
	}
}