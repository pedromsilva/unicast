var install = require( 'source-map-support' ).install;
install();

var Import = require( './bin/Components/Import' ).default;

global.IMPORTER = Import.hijack( '/Unicast', __dirname + '/bin', __dirname );

require( 'babel-polyfill' );

require( '/Unicast/app' );